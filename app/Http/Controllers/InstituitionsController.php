<?php

namespace App\Http\Controllers;

use App\Instituition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class InstituitionsController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = array();
        $data["instituitions"] = Instituition::listBySuperInstituition(Auth::user()->id_instituition);
        return view("instituitions.index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data = array();
        $data["instituitions"][""] = "-- Selecione uma Instituição --";
        foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
            $data["instituitions"][$instituition->id] = $instituition->description;
        }
        return view("instituitions.create", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'description' => 'required',
            'instituition' => 'required',
            'active' => 'required',
        ], [
            'description.required' => 'Você precisa especificar uma descricao válida',
            'instituition.required' => 'Você precisa especificar uma instituição válida',
            'active.required' => 'Você precisa especificar um status válido',
        ]);
        if($validator->fails()) {
            return redirect()->route('instituitions.create')->with('erro', $validator->errors()->all()[0])->withInput();
        } else {
            $data = array();
            $data["description"] = $request->input('description');
            $data["id_instituition"] = $request->input('instituition');
            $data["token"] = Hash::make($request->input('description')) . Hash::make($request->input('instituition')) . Hash::make(date("d-m-Y h:i:s"));
            $data["active"] = ($request->input('active') == "ativo") ? 1 : 0;
            $instituition = Instituition::create($data);
            if($instituition) {
                return redirect()->route('instituitions.index')->with('sucesso', "Dados salvos com sucesso");
            } else {
                return redirect()->route('instituitions.index')->with('erro', "Ocorreu um erro ao tentar salvar os dados");
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if(!$this->isMy($id)) {
            abort(404);
        }
        $data = array();
        foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
            $data["instituitions"][$instituition->id] = $instituition->description;
        }
        $data["instituition"] = Instituition::find($id);
        return view("instituitions.show", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if(!$this->isMy($id)) {
            abort(404);
        }
        $data = array();
        $data["instituitions"][""] = "-- Selecione uma Instituição --";
        foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
            $data["instituitions"][$instituition->id] = $instituition->description;
        }
        $data["instituition"] = Instituition::find($id);
        return view("instituitions.edit", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if(!$this->isMy($id)) {
            abort(404);
        }
        $validator = Validator::make($request->all(), [
            'description' => 'required',
            'instituition' => 'required',
            'active' => 'required',
        ], [
            'description.required' => 'Você precisa especificar uma descrição válida',
            'instituition.required' => 'Você precisa especificar uma instituição válida',
            'active.required' => 'Você precisa especificar um status válido',
        ]);
        if($validator->fails()) {
            return redirect()->route('instituitions.edit', $id)->with('erro', $validator->errors()->all()[0])->withInput();
        } else {
            $data = array();
            $data["description"] = $request->input('description');
            $data["id_instituition"] = $request->input('instituition');
            $data["active"] = ($request->input('active') == "ativo") ? 1 : 0;
            $instituition = Instituition::where('id', $id)->update($data);
            if($instituition) {
                return redirect()->route('instituitions.index')->with('sucesso', "Dados salvos com sucesso");
            } else {
                return redirect()->route('instituitions.index')->with('erro', "Ocorreu um erro ao tentar salvar os dados");
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if(!$this->isMy($id)) {
            abort(404);
        }
        $instituition = Instituition::destroy($id);
        if($instituition) {
            return redirect()->route('instituitions.index')->with('sucesso', "Dados apagados com sucesso.");
        } else {
            return redirect()->route('instituitions.index')->with('erro', "Ocorreu um erro ao tentar apagar os dados.");
        }
    }

    public function token(Request $request) {
        echo date("d-m-Y h:i:s");
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ], [
            'id.required' => 'Você precisa especificar uma instituição válida',
        ]);
        if(!$this->isMy($request->input('id'))) {
            abort(404);
        }
        if($validator->fails()) {
            return redirect()->route('instituitions.edit', $request->input('id'))->with('erro', $validator->errors()->all()[0]);
        } else {
            $instituition = Instituition::find($request->input('id'));
            $instituition->token = Hash::make($instituition->description) . Hash::make($instituition->id_instituition) . Hash::make(date("d-m-Y h:i:s"));
            if($instituition->save()) {
                return redirect()->route('instituitions.edit', $request->input('id'))->with('sucesso', "Dados salvos com sucesso");
            } else {
                return redirect()->route('instituitions.edit', $request->input('id'))->with('erro', "Ocorreu um erro ao tentar salvar os dados");
            }
        }
    }

    private function isMy($id) {
        $instituitionsId = array();
        foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
            $instituitionsId[] = $instituition->id;
        };
        return in_array($id, $instituitionsId);
    }
}
