<?php

namespace App\Http\Controllers;

use App\AdministrativeUnit;
use App\Instituition;
use App\TypeUser;
use App\User;
use App\UserAdministrativeUnit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = array();
        $data["users"] = User::listBySuperInstituition(Auth::user()->id_instituition);
        return view("users.index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data = array();
        $data["instituitions"][""] = "-- Selecione uma Instituição --";
        foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
            $data["instituitions"][$instituition->id] = $instituition->description;
        }
        $data["types"][""] = "-- Selecione um Tipo de Usuário --";
        foreach (TypeUser::get() as $type) {
            $data["types"][$type->id] = $type->description;
        }
        return view("users.create", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users',
            'type' => 'required',
            'password' => 'required',
            'conf_password' => 'required',
            'instituition' => 'required',
            'active' => 'required',
        ], [
            'name.required' => 'Você precisa especificar um nome válido',
            'email.required' => 'Você precisa especificar um email válido',
            'email.unique' => 'Email já existe no sistema',
            'type.required' => 'Você precisa especificar um tipo de usuário válido',
            'password.required' => 'Você precisa especificar uma senha válida',
            'conf_password.required' => 'A confirmação de senha não confere com a senha',
            'instituition.required' => 'Você precisa especificar uma instituição válida',
            'active.required' => 'Você precisa especificar um status válido',
        ]);
        if($validator->fails()) {
            return redirect()->route('users.create')->with('erro', $validator->errors()->all()[0])->withInput();
        } else {
            if($request->input('password') != $request->input('conf_password')) {
                return redirect()->route('users.create')->with('erro', "A confirmação de senha não confere com a senha");
            }
            $data = array();
            $data["name"] = $request->input('name');
            $data["email"] = $request->input('email');
            $data["id_type"] = $request->input('type');
            $data["id_instituition"] = $request->input('instituition');
            $data["password"] = Hash::make($request->input('password'));
            $data["active"] = ($request->input('active') == "ativo") ? 1 : 0;
            $user = User::create($data);
            if($user) {
                return redirect()->route('users.index')->with('sucesso', "Dados salvos com sucesso");
            } else {
                return redirect()->route('users.index')->with('erro', "Ocorreu um erro ao tentar salvar os dados");
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if(!$this->isMy($id)) {
            abort(404);
        }

        foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
            $data["instituitions"][$instituition->id] = $instituition->description;
        }
        foreach (TypeUser::get() as $type) {
            $data["types"][$type->id] = $type->description;
        }
        foreach (AdministrativeUnit::listBySuperInstituition(User::find($id)->id_instituition) as $unit) {
            $data["units"][$unit->id] = $unit->description;
        }
        $data["user"] = User::find($id);
        return view("users.show", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if(!$this->isMy($id)) {
            abort(404);
        }
        $data["instituitions"][""] = "-- Selecione uma Instituição --";
        foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
            $data["instituitions"][$instituition->id] = $instituition->description;
        }
        $data["types"][""] = "-- Selecione um Tipo de Usuário --";
        foreach (TypeUser::get() as $type) {
            $data["types"][$type->id] = $type->description;
        }
        $data["user"] = User::find($id);
        return view("users.edit", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if(!$this->isMy($id)) {
            abort(404);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'type' => 'required',
            'instituition' => 'required',
            'active' => 'required',
        ], [
            'name.required' => 'Você precisa especificar um nome válido',
            'email.required' => 'Você precisa especificar um email válido',
            'email.unique' => 'Email já existe no sistema',
            'type.required' => 'Você precisa especificar um tipo de usuário válido',
            'instituition.required' => 'Você precisa especificar uma instituição válida',
            'active.required' => 'Você precisa especificar um status válido',
        ]);
        if($validator->fails()) {
            return redirect()->route('users.edit', $id)->with('erro', $validator->errors()->all()[0])->withInput();
        } else {
            if($request->input('password') && $request->input('password') != $request->input('conf_password')) {
                return redirect()->route('users.edit', $id)->with('erro', "A confirmação de senha não confere com a senha");
            }
            $data = array();
            $data["name"] = $request->input('name');
            $data["email"] = $request->input('email');
            $data["id_type"] = $request->input('type');
            $data["id_instituition"] = $request->input('instituition');
            if($request->input('password')) $data["password"] = Hash::make($request->input('password'));
            $data["active"] = ($request->input('active') == "ativo") ? 1 : 0;
            $user = User::where('id', $id)->update($data);
            if($user) {
                return redirect()->route('users.index')->with('sucesso', "Dados salvos com sucesso");
            } else {
                return redirect()->route('users.index')->with('erro', "Ocorreu um erro ao tentar salvar os dados");
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if(!$this->isMy($id)) {
            abort(404);
        }
        $user = User::destroy($id);
        if($user) {
            return redirect()->route('users.index')->with('sucesso', "Dados apagados com sucesso.");
        } else {
            return redirect()->route('users.index')->with('erro', "Ocorreu um erro ao tentar apagar os dados.");
        }
    }


    public function addUnit(Request $request) {
        $validator = Validator::make($request->all(), [
            'user' => 'required',
            'unit' => 'required'
        ], [
            'user.required' => 'Você precisa especificar um usuario válido',
            'unit.required' => 'Você precisa especificar uma unidade administrativa válida'
        ]);
        if($validator->fails()) {
            return redirect()->route('users.show', $request->input('user'))->with('erro', $validator->errors()->all()[0]);
        } else {
            if(!$this->isMy($request->input('user'))) {
                abort(404);
            }
            $data = array();
            $data["id_user"] = $request->input('user');
            $data["id_administrative_unit"] = $request->input('unit');
            $userUnit = UserAdministrativeUnit::create($data);
            if($userUnit) {
                return redirect()->route('users.show', $request->input('user'))->with('sucesso', "Dados salvos com sucesso");
            } else {
                return redirect()->route('users.show', $request->input('user'))->with('erro', "Ocorreu um erro ao tentar salvar os dados");
            }
        }
    }
    public function rmUnit(Request $request) {
        $validator = Validator::make($request->all(), [
            'user' => 'required',
            'unit' => 'required'
        ], [
            'user.required' => 'Você precisa especificar um usuario válido',
            'unit.required' => 'Você precisa especificar uma unidade administrativa válida'
        ]);
        if($validator->fails()) {
            return redirect()->route('users.show', $request->input('user'))->with('erro', $validator->errors()->all()[0]);
        } else {
            if(!$this->isMy($request->input('user'))) {
                abort(404);
            }
            $userUnit = UserAdministrativeUnit::where("id_user", "=", $request->input('user'))
                ->where("id_administrative_unit", "=", $request->input('unit'))
                ->delete();
            if($userUnit) {
                return redirect()->route('users.show', $request->input('user'))->with('sucesso', "Dados salvos com sucesso");
            } else {
                return redirect()->route('users.show', $request->input('user'))->with('erro', "Ocorreu um erro ao tentar salvar os dados");
            }
        }
    }
    private function isMy($id) {
        $instituitionsId = array();
        foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
            $instituitionsId[] = $instituition->id;
        };
        return in_array(User::find($id)->id_instituition, $instituitionsId);
    }
}
