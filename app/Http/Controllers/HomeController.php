<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 16/12/16
 * Time: 13:52
 */

namespace App\Http\Controllers;


class HomeController extends Controller {

    /**
     * Esta é a página principal da aplicação.
     *
     * @return \Illuminate\Http\Response
     */

    public function index() {
        return view("home.index");
    }

}