<?php

namespace App\Http\Controllers;

use App\AdministrativeUnit;
use Illuminate\Http\Request;
use App\Instituition;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AdministrativesUnitsController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = array();
        $data["units"] = AdministrativeUnit::listBySuperInstituition(Auth::user()->id_instituition);
        return view("administrativesunits.index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data = array();
        $data["instituitions"][""] = "-- Selecione uma Instituição --";
        foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
            $data["instituitions"][$instituition->id] = $instituition->description;
        }
        return view("administrativesunits.create", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'description' => 'required',
            'instituition' => 'required',
            'active' => 'required',
        ], [
            'description.required' => 'Você precisa especificar uma descricao válida',
            'instituition.required' => 'Você precisa especificar uma instituição válida',
            'active.required' => 'Você precisa especificar um status válido',
        ]);
        if($validator->fails()) {
            return redirect()->route('administrativesunits.create')->with('erro', $validator->errors()->all()[0])->withInput();
        } else {
            $data = array();
            $data["description"] = $request->input('description');
            $data["id_instituition"] = $request->input('instituition');
            if($request->input('code')) $data["code"] = $request->input('code');
            $data["active"] = ($request->input('active') == "ativo") ? 1 : 0;
            $unit = AdministrativeUnit::create($data);
            if($unit) {
                return redirect()->route('administrativesunits.index')->with('sucesso', "Dados salvos com sucesso");
            } else {
                return redirect()->route('administrativesunits.index')->with('erro', "Ocorreu um erro ao tentar salvar os dados");
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if(!$this->isMy($id)) {
            abort(404);
        }
        $data = array();
        foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
            $data["instituitions"][$instituition->id] = $instituition->description;
        }
        $data["unit"] = AdministrativeUnit::find($id);
        return view("administrativesunits.show", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if(!$this->isMy($id)) {
            abort(404);
        }
        $data = array();
        $data["instituitions"][""] = "-- Selecione uma Instituição --";
        foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
            $data["instituitions"][$instituition->id] = $instituition->description;
        }
        $data["unit"] = AdministrativeUnit::find($id);
        return view("administrativesunits.edit", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if(!$this->isMy($id)) {
            abort(404);
        }
        $validator = Validator::make($request->all(), [
            'description' => 'required',
            'instituition' => 'required',
            'active' => 'required',
        ], [
            'description.required' => 'Você precisa especificar uma descrição válida',
            'instituition.required' => 'Você precisa especificar uma instituição válida',
            'active.required' => 'Você precisa especificar um status válido',
        ]);
        if($validator->fails()) {
            return redirect()->route('administrativesunits.edit', $id)->with('erro', $validator->errors()->all()[0])->withInput();
        } else {
            $data = array();
            $data["description"] = $request->input('description');
            $data["id_instituition"] = $request->input('instituition');
            if($request->input('code'))  {
              $data["code"] = $request->input('code');
            } else {
              $data["code"] = NULL;
            }
            $data["active"] = ($request->input('active') == "ativo") ? 1 : 0;
            $unit = AdministrativeUnit::where('id', $id)->update($data);
            if($unit) {
                return redirect()->route('administrativesunits.index')->with('sucesso', "Dados salvos com sucesso");
            } else {
                return redirect()->route('administrativesunits.index')->with('erro', "Ocorreu um erro ao tentar salvar os dados");
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if(!$this->isMy($id)) {
            abort(404);
        }
        $unit = Instituition::destroy($id);
        if($unit) {
            return redirect()->route('administrativesunits.index')->with('sucesso', "Dados apagados com sucesso.");
        } else {
            return redirect()->route('administrativesunits.index')->with('erro', "Ocorreu um erro ao tentar apagar os dados.");
        }
    }

    private function isMy($id) {
        $instituitionsId = array();
        foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
            $instituitionsId[] = $instituition->id;
        };
        return in_array(AdministrativeUnit::find($id)->id_instituition, $instituitionsId);
    }
}
