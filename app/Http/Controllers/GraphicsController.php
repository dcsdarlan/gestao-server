<?php

namespace App\Http\Controllers;

use App\Instituition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class GraphicsController extends Controller {
    public function index() {
        $data = array();
        $data["instituitions"][""] = "-- Selecione uma Instituição --";
        foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
            $data["instituitions"][$instituition->id] = $instituition->description;
        }
        $data["units"] = array();
        $data["units"][""] = "-- Selecione uma Unidade Gestora --";
        $data["types"] = array();
        $data["types"][""] = "-- Selecione um Tipo de Despesa --";
        return view("graphics.index", $data);
    }

    public function show(Request $request) {
        $data = array();
        return view("graphics.show", $data);
    }
}
