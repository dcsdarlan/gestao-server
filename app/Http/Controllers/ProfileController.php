<?php

namespace App\Http\Controllers;

use App\AdministrativeUnit;
use App\Instituition;
use App\TypeUser;
use App\User;
use App\UserAdministrativeUnit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller {
    public function show() {
        $data = array();
        foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
            $data["instituitions"][$instituition->id] = $instituition->description;
        }
        foreach (TypeUser::get() as $type) {
            $data["types"][$type->id] = $type->description;
        }
        foreach (AdministrativeUnit::listBySuperInstituition(Auth::user()->id_instituition) as $unit) {
            $data["units"][$unit->id] = $unit->description;
        }
        $data["user"] = User::find(Auth::user()->id);
        return view("profile.show", $data);
    }
    public function update(Request $request) {
        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'conf_password' => 'required'
        ], [
            'password.required' => 'Você precisa especificar uma senha válida',
            'conf_password.required' => 'A confirmação de senha não confere com a senha'
        ]);
        if($validator->fails()) {
            return redirect()->route('profile.show')->with('erro', $validator->errors()->all()[0]);
        } else {
            if($request->input('password') && $request->input('password') != $request->input('conf_password')) {
                return redirect()->route('profile.show')->with('erro', "A confirmação de senha não confere com a senha");
            }
            $data = array();
            $data["password"] = Hash::make($request->input('password'));
            $user = User::where('id', Auth::user()->id)->update($data);
            if($user) {
                return redirect()->route('profile.show')->with('sucesso', "Dados salvos com sucesso");
            } else {
                return redirect()->route('profile.show')->with('erro', "Ocorreu um erro ao tentar salvar os dados");
            }
        }
    }
}
