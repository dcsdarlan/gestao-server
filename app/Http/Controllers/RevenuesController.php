<?php

namespace App\Http\Controllers;

use App\Revenue;
use App\AdministrativeUnit;
use Illuminate\Http\Request;
use App\Instituition;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Classes\FirebaseDB;

class RevenuesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
      $data = array();
      $data["revenues"] = Revenue::listBySuperInstituition(Auth::user()->id_instituition);
      return view("revenues.index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
      $data = array();
      $data["instituitions"][""] = "-- Selecione uma Instituição --";
      foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
          $data["instituitions"][$instituition->id] = $instituition->description;
      }
      $data["units"] = array();
      $data["units"][""] = "-- Selecione uma Unidade Gestora --";
      if ($request->old('instituition')) {
        $data["units"] = array();
        foreach (AdministrativeUnit::listByInstituition($request->old('instituition')) as $type) {
            $data["units"][$type->id] = $type->description;
        }
      }
      return view("revenues.create", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
      $validator = Validator::make($request->all(), [
          'description' => 'required',
          'instituition' => 'required',
          'unit' => 'required',
          'value' => 'required',
          'date' => 'required',
      ], [
          'description.required' => 'Você precisa especificar uma descricao válida',
          'instituition.required' => 'Você precisa especificar uma instituição válida',
          'unit.required' => 'Você precisa especificar uma unidade administrativa válida',
          'value.required' => 'Você precisa especificar um valor válido',
          'date.required' => 'Você precisa especificar uma data válida',
      ]);
      if($validator->fails()) {
          return redirect()->route('revenues.create')->with('erro', $validator->errors()->all()[0])->withInput();
      } else {
          $data = array();
          $data["description"] = $request->input('description');
          $data["id_administrative_unit"] = $request->input('unit');
          $data["date"] = implode('-', array_reverse(explode('/', $request->input('date'))));
          $data["value"] = (double) str_replace(",", ".", str_replace(".", "", $request->input('value')));
          $revenue = Revenue::create($data);
          if($revenue) {
            $fb = FirebaseDB::init();
            $date = explode('/', $revenue->date);
            $fb->set('/revenues/administrativesunits/' . $revenue->id_administrative_unit . "/" . $date[2] . "/" . $date[1] . "/" . $revenue->id, $revenue);
              return redirect()->route('revenues.index')->with('sucesso', "Dados salvos com sucesso");
          } else {
              return redirect()->route('revenues.index')->with('erro', "Ocorreu um erro ao tentar salvar os dados");
          }
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
      if(!$this->isMy($id)) {
          abort(404);
      }
      $data = array();
      foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
          $data["instituitions"][$instituition->id] = $instituition->description;
      }
      $data["revenue"] = Revenue::find($id);
      return view("revenues.show", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id) {
      if(!$this->isMy($id)) {
          abort(404);
      }
      $data = array();
      $data["instituitions"][""] = "-- Selecione uma Instituição --";
      foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
          $data["instituitions"][$instituition->id] = $instituition->description;
      }
      $data["units"] = array();
      $data["units"][""] = "-- Selecione uma Unidade Gestora --";
      if ($request->old('instituition')) {
        foreach (AdministrativeUnit::listByInstituition($request->old('instituition')) as $type) {
            $data["units"][$type->id] = $type->description;
        }
      } else {
        foreach (AdministrativeUnit::listByInstituition(Revenue::find($id)->administrative_unit->id_instituition) as $unit) {
            $data["units"][$unit->id] = $unit->description;
        }
      }
      $data["revenue"] = Revenue::find($id);
      return view("revenues.edit", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
      if(!$this->isMy($id)) {
          abort(404);
      }
      $validator = Validator::make($request->all(), [
          'description' => 'required',
          'instituition' => 'required',
          'unit' => 'required',
          'value' => 'required',
          'date' => 'required',
      ], [
          'description.required' => 'Você precisa especificar uma descricao válida',
          'instituition.required' => 'Você precisa especificar uma instituição válida',
          'unit.required' => 'Você precisa especificar uma unidade administrativa válida',
          'value.required' => 'Você precisa especificar um valor válido',
          'date.required' => 'Você precisa especificar uma data válida',
      ]);
      if($validator->fails()) {
          return redirect()->route('revenues.create')->with('erro', $validator->errors()->all()[0])->withInput();
      } else {
          $data = array();
          $data["description"] = $request->input('description');
          $data["id_administrative_unit"] = $request->input('unit');
          $data["date"] = implode('-', array_reverse(explode('/', $request->input('date'))));
          $data["value"] = (double) str_replace(",", ".", str_replace(".", "", $request->input('value')));
          $revenue = Revenue::where('id', $id)->update($data);
          if($revenue) {
            $revenue = Revenue::find($id);
            $fb = FirebaseDB::init();
            $date = explode('/', $revenue->date);
            $fb->set('/revenues/administrativesunits/' . $revenue->id_administrative_unit . "/" . $date[2] . "/" . $date[1] . "/" . $revenue->id, $revenue);
              return redirect()->route('revenues.index')->with('sucesso', "Dados salvos com sucesso");
          } else {
              return redirect()->route('revenues.index')->with('erro', "Ocorreu um erro ao tentar salvar os dados");
          }
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
      if(!$this->isMy($id)) {
          abort(404);
      }
      $fb = FirebaseDB::init();
      $date = explode('/', Revenue::find($id)->date);
      $fb->set('/revenues/administrativesunits/' . Revenue::find($id)->id_administrative_unit . "/" . $date[2] . "/" . $date[1] . "/" . Revenue::find($id)->id);
      $revenue = Revenue::destroy($id);
      if($revenue) {
          return redirect()->route('revenues.index')->with('sucesso', "Dados apagados com sucesso.");
      } else {
          return redirect()->route('revenues.index')->with('erro', "Ocorreu um erro ao tentar apagar os dados.");
      }
    }

    private function isMy($id) {
      $unitId = array();
      foreach (AdministrativeUnit::listBySuperInstituition(Auth::user()->id_instituition) as $unit) {
          $unitId[] = $unit->id;
      }
        return in_array(Revenue::find($id)->id_administrative_unit, $unitId);
    }
}
