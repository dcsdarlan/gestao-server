<?php

namespace App\Http\Controllers\Api\V1;

use App\User;
use App\TypeExpense;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use stdClass;
use Tymon\JWTAuth\Facades\JWTAuth;



class TypesExpensesApiController extends ApiController {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $types = TypeExpense::listBySuperInstituition(JWTAuth::toUser(JWTAuth::getToken())->id_instituition);
        if(count($types) > 0) {
            return response()->json($types, 200, [], JSON_UNESCAPED_UNICODE);
        }
        $erro = new stdClass();
        $erro->erro = "não existe dados para a requisição";
        return response()->json($erro, 204, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'description' => 'required',
            'instituition' => 'required',
            'active' => 'required',
            'maximum_limit' => 'max:100',
            'prudential_limit' => 'max:100',
            'alert_limit' => 'max:100',
        ]);
        if($validator->fails()) {
            $erro = new stdClass();
            $erro->erro = $validator->errors()->all();
            return response()->json($erro, 400, [], JSON_UNESCAPED_UNICODE);
        } else {
            $data["description"] =$request->description;
            $data["id_instituition"] = $request->instituition;
            if($request->code) $data["code"] = $request->code;
            if($request->maximum_limit) $data["maximum_limit"] = $request->maximum_limit;
            if($request->prudential_limit) $data["prudential_limit"] = $request->prudential_limit;
            if($request->alert_limit) $data["alert_limit"] = $request->alert_limit;
            $data["active"] = $request->active;
            $type = TypeExpense::create($data);
            if($type) {
                return response()->json($type, 201, [], JSON_UNESCAPED_UNICODE);
            } else {
                $erro = new stdClass();
                $erro->erro = "ocorreu um erro ao tentar salvar os dados";
                return response()->json($erro, 500, [], JSON_UNESCAPED_UNICODE);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if(!$this->isMy($id)) {
            $erro = new stdClass();
            $erro->erro = "pagina não encontrada";
            return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
        }
        $type = TypeExpense::with("instituition")->find($id);
        if(count($type) > 0) {
            return response()->json($type, 200);
        }
        $erro = new stdClass();
        $erro->erro = "não existe dados para a requisição";
        return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if(!$this->isMy($id)) {
            $erro = new stdClass();
            $erro->erro = "pagina não encontrada";
            return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
        }
        if($request->description) $data["description"] = $request->description;
        if($request->code) $data["code"] = $request->code;
        if($request->maximum_limit) $data["maximum_limit"] = $request->maximum_limit;
        if($request->prudential_limit) $data["prudential_limit"] = $request->prudential_limit;
        if($request->alert_limit) $data["alert_limit"] = $request->alert_limit;
        if($request->instituition) $data["id_instituition"] = $request->instituition;
        if($request->password) $data["password"] = Hash::make($request->password);
        if($request->active) $data["active"] = $request->active;
        $type = TypeExpense::where('id', $id)
            ->update($data);
        if($type) {
            return response()->json($type, 204, [], JSON_UNESCAPED_UNICODE);
        } else {
            $erro = new stdClass();
            $erro->erro = "ocorreu um erro ao tentar salvar os dados";
            return response()->json($erro, 304, [], JSON_UNESCAPED_UNICODE);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if(!$this->isMy($id)) {
            $erro = new stdClass();
            $erro->erro = "pagina não encontrada";
            return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
        }
        $type = TypeExpense::destroy($id);
        if($type) {
            return response()->json($type, 204, [], JSON_UNESCAPED_UNICODE);
        } else {
            $erro = new stdClass();
            $erro->erro = "ocorreu um erro ao tentar apagar os dados";
            return response()->json($erro, 500, [], JSON_UNESCAPED_UNICODE);
        }
    }
    private function isMy($id) {
        $instituitionsId = array();
        foreach (Instituition::listBySuperInstituition(JWTAuth::toUser(JWTAuth::getToken())->id_instituition) as $instituition) {
            $instituitionsId[] = $instituition->id;
        };
        return in_array(User::find($id)->id_instituition, $instituitionsId);
    }
    public function listByAdministrativeUnit($id) {
      $types = TypeExpense::listByAdministrativeUnit($id);
      if(count($types) > 0) {
          return response()->json($types, 200, [], JSON_UNESCAPED_UNICODE);
      }
      $erro = new stdClass();
      $erro->erro = "não existe dados para a requisição";
      return response()->json($erro, 204, [], JSON_UNESCAPED_UNICODE);
    }
}
