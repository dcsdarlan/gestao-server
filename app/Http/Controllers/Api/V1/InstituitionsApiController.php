<?php

namespace App\Http\Controllers\Api\V1;

use App\Instituition;
use Illuminate\Http\Request;
use stdClass;
use Tymon\JWTAuth\Facades\JWTAuth;

class InstituitionsApiController extends ApiController {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $instituition = Instituition::listBySuperInstituition(JWTAuth::toUser($request->header('token'))->id_instituition);
        if(count($instituition) > 0) {
            return response()->json($instituition, 200);
        }
        $erro = new stdClass();
        $erro->erro = "não existe dados para a requisição";
        return response()->json($erro, 204, [], JSON_UNESCAPED_UNICODE);
    }

    public function all(Request $request) {
        $instituition = Instituition::get();
        if(count($instituition) > 0) {
            return response()->json($instituition, 200);
        }
        $erro = new stdClass();
        $erro->erro = "não existe dados para a requisição";
        return response()->json($erro, 204, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'descritption' => 'required',
            'active' => 'required',
        ]);
        if($validator->fails()) {
            $erro = new stdClass();
            $erro->erro = $validator->errors()->all();
            return response()->json($erro, 400, [], JSON_UNESCAPED_UNICODE);
        } else {
            if($request->instituition) $data["id_instituition"] = $request->instituition;
            $data["descritption"] = $request->descritption;
            $data["active"] = $request->active;
            $instituition = Instituition::create($data);
            if($instituition) {
                return response()->json($instituition, 201, [], JSON_UNESCAPED_UNICODE);
            } else {
                $erro = new stdClass();
                $erro->erro = "ocorreu um erro ao tentar salvar os dados";
                return response()->json($erro, 500, [], JSON_UNESCAPED_UNICODE);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if(!$this->isMy($id)) {
            $erro = new stdClass();
            $erro->erro = "pagina não encontrada";
            return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
        }
        $instituition = Instituition::find($id);
        if(count($instituition) > 0) {
            return response()->json($instituition, 200);
        }
        $erro = new stdClass();
        $erro->erro = "não existe dados para a requisição";
        return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if(!$this->isMy($id)) {
            $erro = new stdClass();
            $erro->erro = "pagina não encontrada";
            return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
        }
        if($request->instituition) $data["id_instituition"] = $request->instituition;
        if($request->descritption) $data["descritption"] = $request->descritption;
        if($request->active) $data["active"] = $request->active;
        $instituition = Instituition::where('id', $id)
            ->update($data);
        if($instituition) {
            return response()->json($instituition, 200, [], JSON_UNESCAPED_UNICODE);
        } else {
            $erro = new stdClass();
            $erro->erro = "ocorreu um erro ao tentar salvar os dados";
            return response()->json($erro, 304, [], JSON_UNESCAPED_UNICODE);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if(!$this->isMy($id)) {
            $erro = new stdClass();
            $erro->erro = "pagina não encontrada";
            return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
        }
        $instituition = Instituition::destroy($id);
        if($instituition) {
            return response()->json($instituition, 304, [], JSON_UNESCAPED_UNICODE);
        } else {
            $erro = new stdClass();
            $erro->erro = "ocorreu um erro ao tentar apagar os dados";
            return response()->json($erro, 500, [], JSON_UNESCAPED_UNICODE);
        }
    }

    private function isMy($id) {
        $instituitionsId = array();
        foreach (Instituition::listBySuperInstituition(JWTAuth::toUser(JWTAuth::getToken())->id_instituition) as $instituition) {
            $instituitionsId[] = $instituition->id;
        };
        return in_array($id, $instituitionsId);
    }
}
