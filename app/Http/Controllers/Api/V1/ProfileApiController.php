<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 09/01/17
 * Time: 13:11
 */

namespace App\Http\Controllers\Api\V1;

use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use stdClass;

class ProfileApiController extends ApiController {
    public function show(Request $request) {
        $user = User::with("type")->with("instituition")->with("administratives_units")->find(JWTAuth::toUser($request->header('token'))->id);
        if($user) {
            return response()->json($user, 200, [], JSON_UNESCAPED_UNICODE);
        }
        $erro = new stdClass();
        $erro->erro = "não existe dados para a requisição";
        return response()->json($erro, 204, [], JSON_UNESCAPED_UNICODE);
    }

}