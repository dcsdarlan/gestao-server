<?php

namespace App\Http\Controllers\Api\V1;

use App\AdministrativeUnit;
use Illuminate\Http\Request;
use stdClass;

class AdministrativesUnitsApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $adminUnit = AdministrativeUnit::listBySuperInstituition(JWTAuth::toUser(JWTAuth::getToken())->id_instituition);
        if(count($adminUnit) > 0) {
            return response()->json($adminUnit, 200);
        }
        $erro = new stdClass();
        $erro->erro = "não existe dados para a requisição";
        return response()->json($erro, 204, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'instituition' => 'required',
            'descritption' => 'required',
            'active' => 'required',
        ]);
        if($validator->fails()) {
            return response()->json($validator->errors()->all(), 400, [], JSON_UNESCAPED_UNICODE);
        } else {
            $data["id_instituition"] = $request->instituition;
            $data["descritption"] = $request->descritption;
            $data["active"] = $request->active;
            $adminUnit = AdministrativeUnit::create($data);
            if($adminUnit) {
                return response()->json($adminUnit, 201, [], JSON_UNESCAPED_UNICODE);
            } else {
                $erro = new stdClass();
                $erro->erro = "ocorreu um erro ao tentar salvar os dados";
                return response()->json($erro, 500, [], JSON_UNESCAPED_UNICODE);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if(!$this->isMy($id)) {
            $erro = new stdClass();
            $erro->erro = "pagina não encontrada";
            return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
        }
        $adminUnit = AdministrativeUnit::find($id);
        if(count($adminUnit) > 0) {
            return response()->json($adminUnit, 200);
        }
        $erro = new stdClass();
        $erro->erro = "não existe dados para a requisição";
        return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if(!$this->isMy($id)) {
            $erro = new stdClass();
            $erro->erro = "pagina não encontrada";
            return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
        }
        if($request->instituition) $data["id_instituition"] = $request->instituition;
        if($request->descritption) $data["descritption"] = $request->descritption;
        if($request->active) $data["active"] = $request->active;
        $adminUnit = AdministrativeUnit::where('id', $id)
            ->update($data);
        if($adminUnit) {
            return response()->json($adminUnit, 204, [], JSON_UNESCAPED_UNICODE);
        } else {
            $erro = new stdClass();
            $erro->erro = "ocorreu um erro ao tentar salvar os dados";
            return response()->json($erro, 304, [], JSON_UNESCAPED_UNICODE);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if(!$this->isMy($id)) {
            $erro = new stdClass();
            $erro->erro = "pagina não encontrada";
            return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
        }
        $adminUnit = AdministrativeUnit::destroy($id);
        if($adminUnit) {
            return response()->json($adminUnit, 204, [], JSON_UNESCAPED_UNICODE);
        } else {
            $erro = new stdClass();
            $erro->erro = "ocorreu um erro ao tentar apagar os dados";
            return response()->json($erro, 500, [], JSON_UNESCAPED_UNICODE);
        }
    }

    private function isMy($id) {
        $instituitionsId = array();
        foreach (Instituition::listBySuperInstituition(JWTAuth::toUser(JWTAuth::getToken())->id_instituition) as $instituition) {
            $instituitionsId[] = $instituition->id;
        };
        return in_array(AdministrativeUnit::find($id)->id_instituition, $instituitionsId);
    }

    public function listByInstituition($id) {
      $units = AdministrativeUnit::listByInstituition($id);
      if(count($units) > 0) {
          return response()->json($units, 200, [], JSON_UNESCAPED_UNICODE);
      }
      $erro = new stdClass();
      $erro->erro = "não existe dados para a requisição";
      return response()->json($erro, 204, [], JSON_UNESCAPED_UNICODE);
    }
}
