<?php

namespace App\Http\Controllers\Api\V1;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use stdClass;
use Tymon\JWTAuth\Facades\JWTAuth;



class UsersApiController extends ApiController {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $users = User::listBySuperInstituition(JWTAuth::toUser(JWTAuth::getToken())->id_instituition);
        if(count($users) > 0) {
            return response()->json($users, 200, [], JSON_UNESCAPED_UNICODE);
        }
        $erro = new stdClass();
        $erro->erro = "não existe dados para a requisição";
        return response()->json($erro, 204, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'type' => 'required',
            'email' => 'required',
            'instituition' => 'required',
            'password' => 'required',
            'active' => 'required',
        ]);
        if($validator->fails()) {
            $erro = new stdClass();
            $erro->erro = $validator->errors()->all();
            return response()->json($erro, 400, [], JSON_UNESCAPED_UNICODE);
        } else {
            $data["name"] = $request->name;
            $data["id_type"] = $request->type;
            $data["email"] = $request->email;
            $data["id_instituition"] = $request->instituition;
            $data["password"] = Hash::make($request->password);
            $data["active"] = $request->active;
            $user = User::create($data);
            if($user) {
                return response()->json($user, 201, [], JSON_UNESCAPED_UNICODE);
            } else {
                $erro = new stdClass();
                $erro->erro = "ocorreu um erro ao tentar salvar os dados";
                return response()->json($erro, 500, [], JSON_UNESCAPED_UNICODE);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if(!$this->isMy($id)) {
            $erro = new stdClass();
            $erro->erro = "pagina não encontrada";
            return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
        }
        $user = User::with("type")->with("instituition")->with("administratives_units")->find($id);
        if(count($user) > 0) {
            return response()->json($user, 200, [], JSON_UNESCAPED_UNICODE);
        }
        $erro = new stdClass();
        $erro->erro = "não existe dados para a requisição";
        return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if(!$this->isMy($id)) {
            $erro = new stdClass();
            $erro->erro = "pagina não encontrada";
            return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
        }
        if($request->name) $data["name"] = $request->name;
        if($request->type) $data["id_type"] = $request->type;
        if($request->email) $data["email"] = $request->email;
        if($request->instituition) $data["id_instituition"] = $request->instituition;
        if($request->password) $data["password"] = Hash::make($request->password);
        if($request->active) $data["active"] = $request->active;
        $user = User::where('id', $id)
            ->update($data);
        if($user) {
            return response()->json($user, 204, [], JSON_UNESCAPED_UNICODE);
        } else {
            $erro = new stdClass();
            $erro->erro = "ocorreu um erro ao tentar salvar os dados";
            return response()->json($erro, 304, [], JSON_UNESCAPED_UNICODE);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if(!$this->isMy($id)) {
            $erro = new stdClass();
            $erro->erro = "pagina não encontrada";
            return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
        }
        $user = User::destroy($id);
        if($user) {
            return response()->json($user, 204, [], JSON_UNESCAPED_UNICODE);
        } else {
            $erro = new stdClass();
            $erro->erro = "ocorreu um erro ao tentar apagar os dados";
            return response()->json($erro, 500, [], JSON_UNESCAPED_UNICODE);
        }
    }

    private function isMy($id) {
        $instituitionsId = array();
        foreach (Instituition::listBySuperInstituition(JWTAuth::toUser(JWTAuth::getToken())->id_instituition) as $instituition) {
            $instituitionsId[] = $instituition->id;
        };
        return in_array(User::find($id)->id_instituition, $instituitionsId);
    }
}
