<?php

namespace App\Http\Controllers\Api\V1;

use App\User;
use App\Revenue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use stdClass;
use Tymon\JWTAuth\Facades\JWTAuth;

class RevenuesApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
      $revenues = Revenue::listBySuperInstituition(JWTAuth::toUser(JWTAuth::getToken())->id_instituition);
      if(count($revenues) > 0) {
          return response()->json($revenues, 200, [], JSON_UNESCAPED_UNICODE);
      }
      $erro = new stdClass();
      $erro->erro = "não existe dados para a requisição";
      return response()->json($erro, 204, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
      $validator = Validator::make($request->all(), [
          'description' => 'required',
          'instituition' => 'required',
          'date' => 'required',
          'value' => 'required',
      ]);
      if($validator->fails()) {
          $erro = new stdClass();
          $erro->erro = $validator->errors()->all();
          return response()->json($erro, 400, [], JSON_UNESCAPED_UNICODE);
      } else {
          $data["description"] =$request->description;
          $data["value"] = $request->value;
          $data["id_instituition"] = $request->instituition;
          $data["date"] = implode('-', array_reverse(explode('/', $request->date)));
          $revenue = Revenue::create($data);
          if($revenue) {
              return response()->json($revenue, 201, [], JSON_UNESCAPED_UNICODE);
          } else {
              $erro = new stdClass();
              $erro->erro = "ocorreu um erro ao tentar salvar os dados";
              return response()->json($erro, 500, [], JSON_UNESCAPED_UNICODE);
          }
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      if(!$this->isMy($id)) {
          $erro = new stdClass();
          $erro->erro = "pagina não encontrada";
          return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
      }
      $revenue = Revenue::with("instituition")->find($id);
      if(count($revenue) > 0) {
          return response()->json($revenue, 200);
      }
      $erro = new stdClass();
      $erro->erro = "não existe dados para a requisição";
      return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $validator = Validator::make($request->all(), [
          'description' => 'required',
          'instituition' => 'required',
          'date' => 'required',
          'value' => 'required',
      ]);
      if($validator->fails()) {
          $erro = new stdClass();
          $erro->erro = $validator->errors()->all();
          return response()->json($erro, 400, [], JSON_UNESCAPED_UNICODE);
      } else {
          if($request->description) $data["description"] = $request->description;
          if($request->value) $data["value"] = $request->value;
          if($request->instituition) $data["id_instituition"] = $request->instituition;
          if($request->date)$data["date"] = implode('-', array_reverse(explode('/', $request->date)));
          $revenue = Revenue::where('id', $id)
              ->update($data);
          if($revenue) {
              return response()->json($revenue, 201, [], JSON_UNESCAPED_UNICODE);
          } else {
              $erro = new stdClass();
              $erro->erro = "ocorreu um erro ao tentar salvar os dados";
              return response()->json($erro, 500, [], JSON_UNESCAPED_UNICODE);
          }
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
      if(!$this->isMy($id)) {
          $erro = new stdClass();
          $erro->erro = "pagina não encontrada";
          return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
      }
      $revenue = Revenue::destroy($id);
      if($revenue) {
          return response()->json($revenue, 204, [], JSON_UNESCAPED_UNICODE);
      } else {
          $erro = new stdClass();
          $erro->erro = "ocorreu um erro ao tentar apagar os dados";
          return response()->json($erro, 500, [], JSON_UNESCAPED_UNICODE);
      }
    }

    private function isMy($id) {
        $instituitionsId = array();
        foreach (Instituition::listBySuperInstituition(JWTAuth::toUser(JWTAuth::getToken())->id_instituition) as $instituition) {
            $instituitionsId[] = $instituition->id;
        };
        return in_array(Revenue::find($id)->id_instituition, $instituitionsId);
    }
}
