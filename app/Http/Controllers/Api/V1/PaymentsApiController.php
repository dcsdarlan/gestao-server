<?php

namespace App\Http\Controllers\Api\V1;

use App\AdministrativeUnit;
use App\Payment;
use Davibennun\LaravelPushNotification\Facades\PushNotification;
use Illuminate\Http\Request;
use stdClass;
use Illuminate\Support\Facades\Validator;
use App\Instituition;
use Tymon\JWTAuth\Facades\JWTAuth;
use DateTime;

class PaymentsApiController extends ApiController {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $payments = Payment::listByUser(JWTAuth::toUser($request->header('token'))->id);
        if(count($payments) > 0) {
            return response()->json($payments, 200, [], JSON_UNESCAPED_UNICODE);
        }
        $erro = new stdClass();
        $erro->erro = "não existe dados para a requisição";
        return response()->json($erro, 204, [], JSON_UNESCAPED_UNICODE);
    }

    public function filter(Request $request) {
        $date = null;
        if($request->init_date && $request->end_date) {
            $date = str_replace("-", "/", $request->init_date) . " - " . str_replace("-", "/", $request->end_date);
        }
        $payments = Payment::apiListByUser(JWTAuth::toUser(JWTAuth::getToken())->id,
            $date,
            $request->unit,
            $request->creditor,
            $request->type_creditor,
            $request->element,
            $request->source_resource,
            $request->status);
        if(count($payments) > 0) {
            return response()->json($payments, 200, [], JSON_UNESCAPED_UNICODE);
        }
        $erro = new stdClass();
        $erro->erro = "não existe dados para a requisição";
        return response()->json($erro, 200, [], JSON_UNESCAPED_UNICODE);
    }

    public function approve(Request $request) {
        $cont = 0;
        foreach ($request->payment_id as $paymentId) {
            if($this->isMy($paymentId, $request)) {
                $data = array();
                $data["id_user_manager"] = JWTAuth::toUser($request->header('token'))->id;
                $data["operated_at"] = date('Y-m-d H:i:s');
                $data["status"] = "confirmed";
                $payment = Payment::where('id', $paymentId)->update($data);
                if ($payment) $cont++;
            }
        }
        if ($cont == count($request->payment_id)) {
            $success = new stdClass();
            $success->success = "dados salvos com sucesso";
            return response()->json($success, 200, [], JSON_UNESCAPED_UNICODE);
        } else {
            $erro = new stdClass();
            $erro->erro = "um ou mais pagamentos não puderam ser processados";
            return response()->json($erro, 401, [], JSON_UNESCAPED_UNICODE);
        }
    }
    public function deprive(Request $request) {
        $cont = 0;
        foreach ($request->payment_id as $paymentId) {
            if(!$this->isMy($request->payment_id, $request)) {
                $data = array();
                $data["id_user_manager"] = JWTAuth::toUser($request->header('token'))->id;
                $data["operated_at"] = date('Y-m-d H:i:s');
                $data["observation"] = $request->input('obs');
                $data["status"] = "canceled";
                $payment = Payment::where('id', $paymentId)->update($data);
                if ($payment) $cont++;
            }
        }
        if ($cont == count($request->payment_id)) {
            $success = new stdClass();
            $success->success = "dados salvos com sucesso";
            return response()->json($success, 200, [], JSON_UNESCAPED_UNICODE);
        } else {
            $erro = new stdClass();
            $erro->erro = "um ou mais pagamentos não puderam ser processados";
            return response()->json($erro, 401, [], JSON_UNESCAPED_UNICODE);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'code' => 'required',
            'type_payment' => 'required',
            'administrative_unit' => 'required',
            'code_administrative_unit' => 'required',
            'invoice' => 'required',
            'element' => 'required',
            'source_resource' => 'required',
            'type_expense' => 'required',
            'gross_amount' => 'required',
            'discount' => 'required',
            'net_amount' => 'required',
            'bidding' => 'required',
            'type_creditor' => 'required',
            'creditor' => 'required',
            'historic' => 'required',
            'contract' => 'required',
        ]);
        if($validator->fails()) {
            $erro = new stdClass();
            $erro->erro = $validator->errors()->all();
            return response()->json($erro, 400, [], JSON_UNESCAPED_UNICODE);
        } else {
            $instituition = Instituition::findByToken($request->header('token'));
            $unity = AdministrativeUnit::findByInstituitionCode($instituition->id, $request->code_administrative_unit);
            if(!$unity) {
                $unity = new AdministrativeUnit();
                $unity->id_instituition = $instituition->id;
                $unity->code = $request->code_administrative_unit;
                $unity->description = $request->administrative_unit;
                $unity->active = true;
                $unity->save();
                /*
                $erro = new stdClass();
                $erro->erro = "Código da unidade administrativa é inválido";
                return response()->json($erro, 400, [], JSON_UNESCAPED_UNICODE);
                */
            }
            $data["date"] = $request->date;
            $data["code"] = $request->code;
            $data["type_payment"] = $request->type_payment;
            $data["id_administrative_unit"] = $unity->id;
            $data["invoice"] = $request->invoice;
            $data["element"] = $request->element;
            $data["source_resource"] = $request->source_resource;
            $data["type_expense"] = $request->type_expense;
            $data["gross_amount"] = $request->gross_amount;
            $data["discount"] = $request->discount;
            $data["net_amount"] = $request->net_amount;
            $data["bidding"] = $request->bidding;
            $data["type_creditor"] = $request->type_creditor;
            $data["creditor"] = $request->creditor;
            if($request->historic) $data["historic"] = $request->historic;
            if($request->observation) $data["observation"] = $request->observation;
            $data["contract"] = $request->contract;
            $data["status"] = "waiting";

            $payment = Payment::create($data);
            if($payment) {
                $deviceAndroid = array();
                $deviceIOS = array();
                foreach($payment->administrative_unit->users as $user) {
                    foreach($user->devices as $device) {
                        if($device->platform == "android") $deviceAndroid[] = PushNotification::Device($device->token, array());
                        if($device->platform == "ios") $deviceIOS[] = PushNotification::Device($device->token, array());
                    };
                };
                $message = PushNotification::Message('Novo pagamento aguardando aprovação!', array());
                $devicesCAndroid = PushNotification::DeviceCollection($deviceAndroid);
                //$devicesCIOS = PushNotification::DeviceCollection($deviceIOS);
                $collectionAndroid = PushNotification::app('gestaoAndroid')
                    ->to($devicesCAndroid)
                    ->send($message);
                $responseAndroid = null;
                foreach ($collectionAndroid->pushManager as $push) {
                    $responseAndroid = $push->getAdapter()->getResponse();
                }
                return response()->json($payment, 201, [], JSON_UNESCAPED_UNICODE);
            } else {
                $erro = new stdClass();
                $erro->erro = "ocorreu um erro ao tentar salvar os dados";
                return response()->json($erro, 500, [], JSON_UNESCAPED_UNICODE);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id) {
        if(!$this->isMy($id, $request)) {
            $erro = new stdClass();
            $erro->erro = "pagina não encontrada";
            return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
        }
        $payment = Payment::with("administrative_unit")->with("user_manager")->with("administrative_unit.instituition")->find($id);
        if(count($payment) > 0) {
            return response()->json($payment, 200);
        }
        $erro = new stdClass();
        $erro->erro = "não existe dados para a requisição";
        return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id) {
        if(!Payment::find($id)->id_instituition == Instituition::findByToken($request->header('token'))->id) {
            $erro = new stdClass();
            $erro->erro = "pagina não encontrada";
            return response()->json($erro, 404, [], JSON_UNESCAPED_UNICODE);
        }
        $payment = Payment::destroy($id);
        if($payment) {
            return response()->json($payment, 204, [], JSON_UNESCAPED_UNICODE);
        } else {
            $erro = new stdClass();
            $erro->erro = "ocorreu um erro ao tentar apagar os dados";
            return response()->json($erro, 500, [], JSON_UNESCAPED_UNICODE);
        }
    }
    private function isMy($id, $request) {
        $paymentsId = array();
        foreach (Payment::listMyUser(JWTAuth::toUser($request->header('token'))->id) as $payments) {
            $paymentsId[] = $payments->id;
        };
        return in_array($id, $paymentsId);
    }
}
