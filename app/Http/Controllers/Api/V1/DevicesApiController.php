<?php

namespace App\Http\Controllers\Api\V1;

use App\Device;
use Illuminate\Http\Request;
use stdClass;
use Illuminate\Support\Facades\Validator;

class DevicesApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'platform' => 'required',
            'token' => 'required',
            'user' => 'required'
        ]);
        if($validator->fails()) {
            return response()->json($validator->errors()->all(), 400, [], JSON_UNESCAPED_UNICODE);
        } else {
            $data["id_user"] = $request->user;
            $data["token"] = $request->token;
            $data["platform"] = $request->platform;
            $data["uuid"] = $request->uuid;
            $data["active"] = true;
            $device = Device::findByUuid($request->uuid);
            if($device) {
                $device->update($data);
            } else {
                $device = Device::create($data);
            }
            if($device) {
                return response()->json($device, 201, [], JSON_UNESCAPED_UNICODE);
            } else {
                $erro = new stdClass();
                $erro->erro = "ocorreu um erro ao tentar salvar os dados";
                return response()->json($erro, 500, [], JSON_UNESCAPED_UNICODE);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $device = Device::destroy($id);
        if($device) {
            return response()->json($device, 204, [], JSON_UNESCAPED_UNICODE);
        } else {
            $erro = new stdClass();
            $erro->erro = "ocorreu um erro ao tentar apagar os dados";
            return response()->json($erro, 500, [], JSON_UNESCAPED_UNICODE);
        }
    }
}
