<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use stdClass;
use Illuminate\Support\Facades\Validator;
use App\TypeExpense;
use App\Expense;
use App\Revenue;
use App\AdministrativeUnit;

class GraphicsApiController extends Controller {
    public function showMonth(Request $request) {
        $validator = Validator::make($request->all(), [
            'instituition' => 'required',
            'unit' => 'required',
            'type' => 'required',
            'month' => 'required',
            'year' => 'required',
        ]);
        if ($validator->fails()) {
            $erro = new stdClass();
            $erro->erro = $validator->errors()->all();
            return response()->json($erro, 400, [], JSON_UNESCAPED_UNICODE);
        } else {
            $data = new stdClass();
            $unit = AdministrativeUnit::find($request->unit);
            $typeExpense = TypeExpense::find($request->type);
            $data->maximum_limit = $typeExpense->maximum_limit;
            $data->prudential_limit = $typeExpense->prudential_limit;
            $data->alert_limit = $typeExpense->alert_limit;
            $init = $request->year . "-" . $request->month . "-01";
            $end = $request->year . "-" . $request->month . "-31";
            $data->expenses = Expense::listGraphic($typeExpense->id, $init, $end);
            $data->revenues = Revenue::listGraphic($unit->id, $init, $end);

            if (count($data->expenses) < 1 || count($data->revenues) < 1) {
                $erro = new stdClass();
                $erro->erro[] = "Não ha dados para esse filtro";
                return response()->json($erro, 400, [], JSON_UNESCAPED_UNICODE);
            }
            return response()->json($data, 200, [], JSON_UNESCAPED_UNICODE);
        }
    }
    public function showYear(Request $request) {
        $validator = Validator::make($request->all(), [
            'instituition' => 'required',
            'unit' => 'required',
            'type' => 'required',
            'month' => 'required',
            'year' => 'required',
        ]);
        if ($validator->fails()) {
            $erro = new stdClass();
            $erro->erro = $validator->errors()->all();
            return response()->json($erro, 400, [], JSON_UNESCAPED_UNICODE);
        } else {
            $data = new stdClass();
            $unit = AdministrativeUnit::find($request->unit);
            $typeExpense = TypeExpense::find($request->type);
            $data->maximum_limit = $typeExpense->maximum_limit;
            $data->prudential_limit = $typeExpense->prudential_limit;
            $data->alert_limit = $typeExpense->alert_limit;
            $init = $request->year . "-" . $request->month . "-01";
            if($request->month < 10) {
                $mes = $request->month + 3;
                $ano = $request->year - 1;
                $init = $ano . "-" . $mes . "-01";
            } else {
                $mes = $request->month -10;
                $ano = $request->year;
                $init = $ano . "-" . $mes . "-01";
            }
            $end = $request->year . "-" . $request->month . "-31";
            $data->expenses = Expense::listGraphic($typeExpense->id, $init, $end);
            $data->revenues = Revenue::listGraphic($unit->id, $init, $end);

            if (count($data->expenses) < 1 || count($data->revenues) < 1) {
                $erro = new stdClass();
                $erro->erro[] = "Não ha dados para esse filtro";
                return response()->json($erro, 400, [], JSON_UNESCAPED_UNICODE);
            }
            return response()->json($data, 200, [], JSON_UNESCAPED_UNICODE);
        }
    }
}
