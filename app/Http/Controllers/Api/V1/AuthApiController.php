<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 13/12/16
 * Time: 11:16
 */

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use stdClass;


class AuthApiController extends ApiController {
    public function logon(Request $request) {
        $credentials = $request->only('email', 'password');
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ], [
            'email.required' => 'Você precisa especificar um email válido',
            'password.required' => 'Você precisa especificar uma senha válida'
        ]);
        if($validator->fails()) {
            $erro = new stdClass();
            $erro->erro = $validator->errors()->all();
            return response()->json($erro, 400, [], JSON_UNESCAPED_UNICODE);
        } else {
            try {
                if (!$token = JWTAuth::attempt($credentials)) {
                    $erro = new stdClass();
                    $erro->erro = "email e/ou senha inválido(s)";
                    return response()->json($erro, 401, [], JSON_UNESCAPED_UNICODE);
                }
            } catch (JWTException $e) {
                $erro = new stdClass();
                $erro->erro = "erro ao gerar token";
                return response()->json($erro, 500, [], JSON_UNESCAPED_UNICODE);
            }

            $token = compact('token');
            $token["user"] = JWTAuth::setToken($token['token'])->toUser();
            return response()->json($token);
        }
    }
}