<?php

namespace App\Http\Controllers;

use App\Expense;
use App\TypeExpense;
use App\AdministrativeUnit;
use Illuminate\Http\Request;
use App\Instituition;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Classes\FirebaseDB;

class ExpensesController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {
    $data = array();
    $data["expenses"] = Expense::listBySuperInstituition(Auth::user()->id_instituition);
    return view("expenses.index", $data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create(Request $request) {
    $data = array();
    $data["instituitions"][""] = "-- Selecione uma Instituição --";
    foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
        $data["instituitions"][$instituition->id] = $instituition->description;
    }
    $data["units"] = array();
    $data["units"][""] = "-- Selecione uma Unidade Gestora --";
    if ($request->old('instituition')) {
      foreach (AdministrativeUnit::listByInstituition($request->old('instituition')) as $type) {
          $data["units"][$type->id] = $type->description;
      }
    }
    $data["types"] = array();
    $data["types"][""] = "-- Selecione um Tipo de Despesa --";
    if ($request->old('units')) {
      foreach (TypeExpense::listByInstituition($request->old('units')) as $type) {
          $data["types"][$type->id] = $type->description;
      }
    }
    return view("expenses.create", $data);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request) {
    $validator = Validator::make($request->all(), [
        'description' => 'required',
        'instituition' => 'required',
        'unit' => 'required',
        'type' => 'required',
        'value' => 'required',
        'date' => 'required',
    ], [
        'description.required' => 'Você precisa especificar uma descricao válida',
        'instituition.required' => 'Você precisa especificar uma instituição válida',
        'unit.required' => 'Você precisa especificar uma unidade administrativa válida',
        'type.required' => 'Você precisa especificar um tipo de despesa válido',
        'value.required' => 'Você precisa especificar um valor válido',
        'date.required' => 'Você precisa especificar uma data válida',
    ]);
    if($validator->fails()) {
        return redirect()->route('expenses.create')->with('erro', $validator->errors()->all()[0])->withInput();
    } else {
        $data = array();
        $data["description"] = $request->input('description');
        $data["id_type"] = $request->input('type');
        $data["date"] = implode('-', array_reverse(explode('/', $request->input('date'))));
        $data["value"] = (double) str_replace(",", ".", str_replace(".", "", $request->input('value')));
        $expense = Expense::create($data);
        if($expense) {
          $fb = FirebaseDB::init();
          $date = explode('/', $expense->date);
          $fb->set('/expenses/types/' . $expense->id_type . "/" . $date[2] . "/" . $date[1] . "/" . $expense->id, $expense);
            return redirect()->route('expenses.index')->with('sucesso', "Dados salvos com sucesso");
        } else {
            return redirect()->route('expenses.index')->with('erro', "Ocorreu um erro ao tentar salvar os dados");
        }
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id) {
    if(!$this->isMy($id)) {
        abort(404);
    }
    $data = array();
    foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
        $data["instituitions"][$instituition->id] = $instituition->description;
    }
    $data["expense"] = Expense::find($id);
    return view("expenses.show", $data);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit(Request $request, $id) {
    if(!$this->isMy($id)) {
        abort(404);
    }
    $data = array();
    $data["instituitions"][""] = "-- Selecione uma Instituição --";
    foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
        $data["instituitions"][$instituition->id] = $instituition->description;
    }
    $data["units"] = array();
    $data["units"][""] = "-- Selecione uma Unidade Gestora --";
    if ($request->old('instituition')) {
      foreach (AdministrativeUnit::listByInstituition($request->old('instituition')) as $type) {
          $data["units"][$type->id] = $type->description;
      }
    } else {
      foreach (AdministrativeUnit::listByInstituition(Expense::find($id)->type->administrative_unit->id_instituition) as $type) {
          $data["units"][$type->id] = $type->description;
      }
    }
    $data["types"] = array();
    $data["types"][""] = "-- Selecione um Tipo de Despesa --";
    if ($request->old('units')) {
      foreach (TypeExpense::listByInstituition($request->old('units')) as $type) {
          $data["types"][$type->id] = $type->description;
      }
    } else {
      foreach (TypeExpense::listByAdministrativeUnit(Expense::find($id)->type->id_administrative_unit) as $type) {
          $data["types"][$type->id] = $type->description;
      }
    }

    $data["expense"] = Expense::find($id);
    return view("expenses.edit", $data);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id) {
    if(!$this->isMy($id)) {
        abort(404);
    }
    $validator = Validator::make($request->all(), [
        'description' => 'required',
        'instituition' => 'required',
        'unit' => 'required',
        'type' => 'required',
        'value' => 'required',
        'date' => 'required',
    ], [
        'description.required' => 'Você precisa especificar uma descricao válida',
        'instituition.required' => 'Você precisa especificar uma instituição válida',
        'unit.required' => 'Você precisa especificar uma unidade administrativa válida',
        'type.required' => 'Você precisa especificar um tipo de despesa válido',
        'value.required' => 'Você precisa especificar um valor válido',
        'date.required' => 'Você precisa especificar uma data válida',
    ]);
    if($validator->fails()) {
        return redirect()->route('expenses.create')->with('erro', $validator->errors()->all()[0])->withInput();
    } else {
        $data = array();
        $data["description"] = $request->input('description');
        $data["id_type"] = $request->input('type');
        $data["date"] = implode('-', array_reverse(explode('/', $request->input('date'))));
        $data["value"] = (double) str_replace(",", ".", str_replace(".", "", $request->input('value')));
        $expense = Expense::where('id', $id)->update($data);
        if($expense) {
          $expense = Expense::find($id);
          $fb = FirebaseDB::init();
          $date = explode('/', $expense->date);
          $fb->set('/expenses/types/' . $expense->id_type . "/" . $date[2] . "/" . $date[1] . "/" . $expense->id, $expense);
            return redirect()->route('expenses.index')->with('sucesso', "Dados salvos com sucesso");
        } else {
            return redirect()->route('expenses.index')->with('erro', "Ocorreu um erro ao tentar salvar os dados");
        }
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id){
    if(!$this->isMy($id)) {
        abort(404);
    }
    $fb = FirebaseDB::init();
    $date = explode('/', Expense::find($id)->date);
    $fb->set('/expenses/types/' . Expense::find($id)->id_type . "/" . $date[2] . "/" . $date[1] . "/" . Expense::find($id)->id);
    $expense = Expense::destroy($id);
    if($expense) {
        return redirect()->route('expenses.index')->with('sucesso', "Dados apagados com sucesso.");
    } else {
        return redirect()->route('expenses.index')->with('erro', "Ocorreu um erro ao tentar apagar os dados.");
    }
  }

  private function isMy($id) {
      $typesId = array();
      foreach (TypeExpense::listBySuperInstituition(Auth::user()->id_instituition) as $type) {
          $typesId[] = $type->id;
      };
      return in_array(Expense::find($id)->id_type, $typesId);
  }
}
