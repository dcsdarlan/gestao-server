<?php
/**
 * Created by PhpStorm.
 * User: desenvolvimento
 * Date: 13/12/16
 * Time: 11:16
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;


class AuthController extends Controller {
    public function login(Request $request) {
        return view('auth.login');
    }
    public function logon(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ], [
            'email.required' => 'Você precisa especificar um email válido',
            'password.required' => 'Você precisa especificar uma senha válida'
        ]);
        if($validator->fails()) {
            return redirect()->route('auth.login')->with('erro', $validator->errors()->all()[0]);
        } else {
            $remember = ($request->input('remenber')) ? 1 : 0;
            if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password'), 'active' => 1], $remember)) {
                return redirect()->route('home.index');
            }
            return redirect()->route('auth.login')->with('erro', "Email e/ou Senha inválido(s)");
        }
    }
    public function logoff(Request $request) {
        Auth::logout();
        return redirect()->route('auth.login')->with('success', "Saida do sistema efetuada com sucesso");
    }
}