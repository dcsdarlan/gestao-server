<?php

namespace App\Http\Controllers;

use App\TypeExpense;
use App\AdministrativeUnit;
use Illuminate\Http\Request;
use App\Instituition;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Classes\FirebaseDB;

class TypesExpensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
      $data = array();
      $data["types"] = TypeExpense::listBySuperInstituition(Auth::user()->id_instituition);
      return view("typesexpenses.index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
      $data = array();
      $data["instituitions"][""] = "-- Selecione uma Instituição --";
      foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
          $data["instituitions"][$instituition->id] = $instituition->description;
      }
      $data["units"] = array();
      $data["units"][""] = "-- Selecione uma Unidade Gestora --";
      if ($request->old("instituition")) {
        foreach (AdministrativeUnit::listByInstituition($request->old("instituition")) as $type) {
            $data["units"][$type->id] = $type->description;
        }
      }
      return view("typesexpenses.create", $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
      $validator = Validator::make($request->all(), [
          'description' => 'required',
          'instituition' => 'required',
          'unit' => 'required',
          'active' => 'required',
          'maximum_limit' => 'max:100',
          'prudential_limit' => 'max:100',
          'alert_limit' => 'max:100',
      ], [
          'description.required' => 'Você precisa especificar uma descricao válida',
          'instituition.required' => 'Você precisa especificar uma instituição válida',
          'unit.required' => 'Você precisa especificar uma unidade administrativa válida',
          'active.required' => 'Você precisa especificar um status válido',
          'maximum_limit.max' => 'Você precisa especificar uma porcentagem válida',
          'prudential_limit.max' => 'Você precisa especificar uma porcentagem válida',
          'prudential_limit.max' => 'Você precisa especificar uma porcentagem válida',
      ]);
      if($validator->fails()) {
          return redirect()->route('typesexpenses.create')->with('erro', $validator->errors()->all()[0])->withInput();
      } else {
          $data = array();
          $data["description"] = $request->input('description');
          $data["id_administrative_unit"] = $request->input('unit');
          if($request->input('code')) $data["code"] = $request->input('code');
          if($request->input('maximum_limit')) $data["maximum_limit"] = $request->input('maximum_limit');
          if($request->input('prudential_limit')) $data["prudential_limit"] = $request->input('prudential_limit');
          if($request->input('alert_limit')) $data["alert_limit"] = $request->input('alert_limit');
          $data["active"] = ($request->input('active') == "ativo") ? 1 : 0;
          $type = TypeExpense::create($data);
          if($type) {
              $fb = FirebaseDB::init();
              $fb->set('/typesexpenses/administrativesunits/' . $type->id_administrative_unit . "/" . $type->id, $type);
              return redirect()->route('typesexpenses.index')->with('sucesso', "Dados salvos com sucesso");
          } else {
              return redirect()->route('typesexpenses.index')->with('erro', "Ocorreu um erro ao tentar salvar os dados");
          }
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id) {
      if(!$this->isMy($id)) {
          abort(404);
      }
      $data = array();
      foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
          $data["instituitions"][$instituition->id] = $instituition->description;
      }
      if ($request->session()->has('unit')) {
        $data["units"] = array();
        foreach (AdministrativeUnit::listByInstituition($request->session()->get('unit')) as $type) {
            $data["units"][$type->id] = $type->description;
        }
      } else {
        $data["units"] = array();
      }
      $data["type"] = TypeExpense::find($id);
      return view("typesexpenses.show", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id) {
      if(!$this->isMy($id)) {
          abort(404);
      }
      $data = array();
      $data["instituitions"][""] = "-- Selecione uma Instituição --";
      foreach (Instituition::listBySuperInstituition(Auth::user()->id_instituition) as $instituition) {
          $data["instituitions"][$instituition->id] = $instituition->description;
      }
      $data["units"] = array();
      $data["units"][""] = "-- Selecione uma Unidade Gestora --";
      if ($request->old('instituition')) {
        foreach (AdministrativeUnit::listByInstituition($request->old('instituition')) as $type) {
            $data["units"][$type->id] = $type->description;
        }
      } else {
        foreach (AdministrativeUnit::listByInstituition(TypeExpense::find($id)->administrative_unit->id_instituition) as $unit) {
            $data["units"][$unit->id] = $unit->description;
        }
      }
      $data["type"] = TypeExpense::find($id);
      return view("typesexpenses.edit", $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
      if(!$this->isMy($id)) {
          abort(404);
      }
      $validator = Validator::make($request->all(), [
          'description' => 'required',
          'instituition' => 'required',
          'unit' => 'required',
          'active' => 'required',
          'maximum_limit' => 'max:100',
          'prudential_limit' => 'max:100',
          'alert_limit' => 'max:100',
      ], [
          'description.required' => 'Você precisa especificar uma descrição válida',
          'instituition.required' => 'Você precisa especificar uma instituição válida',
          'unit.required' => 'Você precisa especificar uma unidade administrativa válida',
          'active.required' => 'Você precisa especificar um status válido',
          'maximum_limit.max' => 'Você precisa especificar uma porcentagem válida',
          'prudential_limit.max' => 'Você precisa especificar uma porcentagem válida',
          'prudential_limit.max' => 'Você precisa especificar uma porcentagem válida',
      ]);
      if($validator->fails()) {
          return redirect()->route('typesexpenses.edit', $id)->with('erro', $validator->errors()->all()[0])->withInput();
      } else {
          $data = array();
          $data["description"] = $request->input('description');
          $data["id_administrative_unit"] = $request->input('unit');
          if($request->input('code')) {
            $data["code"] = $request->input('code');
          } else {
            $data["code"] = NULL;
          }
          if($request->input('maximum_limit')) {
            $data["maximum_limit"] = $request->input('maximum_limit');
          } else {
            $data["maximum_limit"] = NULL;
          }
          if($request->input('prudential_limit')) {
            $data["prudential_limit"] = $request->input('prudential_limit');
          } else {
            $data["maximum_limit"] = NULL;
          }
          if($request->input('alert_limit')) {
            $data["alert_limit"] = $request->input('alert_limit');
          } else {
            $data["alert_limit"] = NULL;
          }
          $data["active"] = ($request->input('active') == "ativo") ? 1 : 0;
          $type = TypeExpense::where('id', $id)->update($data);
          if($type) {
              $type = TypeExpense::find($id);
              $fb = FirebaseDB::init();
              $fb->set('/typesexpenses/administrativesunits/' . $type->id_administrative_unit . "/" . $type->id, $type);
              return redirect()->route('typesexpenses.index')->with('sucesso', "Dados salvos com sucesso");
          } else {
              return redirect()->route('typesexpenses.index')->with('erro', "Ocorreu um erro ao tentar salvar os dados");
          }
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
      if(!$this->isMy($id)) {
          abort(404);
      }
      $fb = FirebaseDB::init();
      $fb->delete('/typesexpenses/administrativesunits/' . TypeExpense::find($id)->id_administrative_unit . "/" . TypeExpense::find($id)->id);
      $type = TypeExpense::destroy($id);
      if($type) {
          return redirect()->route('typesexpenses.index')->with('sucesso', "Dados apagados com sucesso.");
      } else {
          return redirect()->route('typesexpenses.index')->with('erro', "Ocorreu um erro ao tentar apagar os dados.");
      }
    }

    private function isMy($id) {
        $unitId = array();
        foreach (AdministrativeUnit::listBySuperInstituition(Auth::user()->id_instituition) as $unit) {
            $unitId[] = $unit->id;
        };
        return in_array(TypeExpense::find($id)->id_administrative_unit, $unitId);
    }
}
