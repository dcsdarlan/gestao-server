<?php

namespace App\Http\Controllers;

use App\AdministrativeUnit;
use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class PaymentsController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = array();
        $data["payments"] = Payment::listByUser(Auth::user()->id);
        foreach (AdministrativeUnit::listBySuperInstituition(Auth::user()->id_instituition) as $unit) {
            $data["units"][$unit->id] = $unit->description;
        }
        return view("payments.index", $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if(!$this->isMy($id)) {
            abort(404);
        }
        $data["payment"] = Payment::find($id);
        return view("payments.show", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function approve(Request $request) {
        $request->input('payment_id');
        $validator = Validator::make($request->all(), [
            'payment_id' => 'required',
        ], [
            'payment_id.required' => 'Você precisa especificar um nome válido',
        ]);
        if($validator->fails()) {
            return redirect()->route('payments.index')->with('erro', "Você precisa selecionar pagamentos válidos");
        } else {
            $cont = 0;
            foreach ($request->input('payment_id') as $paymentId) {
                if($this->isMy($paymentId)) {
                    $data = array();
                    $data["id_user_manager"] = Auth::user()->id;
                    $data["operated_at"] = date('Y-m-d H:i:s');
                    $data["status"] = "confirmed";
                    $payment = Payment::where('id', $paymentId)->update($data);
                    if ($payment) $cont++;
                }
            }
            if ($cont == count($request->input('payment_id'))) {
                return redirect()->back()->with('sucesso', "Dados salvos com sucesso");
            } else {
                return redirect()->back()->with('erro', "Um ou mais pagamentos não puderam ser processados");
            }
        }
    }
    public function deprive(Request $request) {
        $validator = Validator::make($request->all(), [
            'payment_id' => 'required',
        ], [
            'payment_id.required' => 'Você precisa especificar um nome válido',
        ]);
        if($validator->fails()) {
            return redirect()->route('payments.index')->with('erro', "Você precisa selecionar pagamentos válidos");
        } else {
            $cont = 0;
            foreach ($request->input('payment_id') as $paymentId) {
                if($this->isMy($paymentId)) {
                    $data = array();
                    $data["id_user_manager"] = Auth::user()->id;
                    $data["operated_at"] = date('Y-m-d H:i:s');
                    $data["observation"] = $request->input('obs');
                    $data["status"] = "canceled";
                    $payment = Payment::where('id', $paymentId)->update($data);
                    if ($payment) $cont++;
                }
            }
            if ($cont == count($request->input('payment_id'))) {
                return redirect()->back()->with('sucesso', "Dados salvos com sucesso");
            } else {
                return redirect()->back()->with('erro', "Um ou mais pagamentos não puderam ser processados");
            }
        }
    }

    public function filter(Request $request) {
        if($request->input('filter_payment_date')) {
            $request->session()->put('filter_payment_date', $request->input('filter_payment_date'));
        } else {
            $request->session()->forget('filter_payment_date');
        }
        if($request->input('filter_payment_creditor')) {
            $request->session()->put('filter_payment_creditor', $request->input('filter_payment_creditor'));
        } else {
            $request->session()->forget('filter_payment_creditor');
        }
        if($request->input('filter_payment_element')) {
            $request->session()->put('filter_payment_element', $request->input('filter_payment_element'));
        } else {
            $request->session()->forget('filter_payment_element');
        }
        if($request->input('filter_payment_source_resource')) {
            $request->session()->put('filter_payment_source_resource', $request->input('filter_payment_source_resource'));
        } else {
            $request->session()->forget('filter_payment_source_resource');
        }
        if($request->input('filter_payment_status')) {
            $request->session()->put('filter_payment_status', $request->input('filter_payment_status'));
        } else {
            $request->session()->forget('filter_payment_status');
        }
        if($request->input('filter_payment_type_creditor')) {
            $request->session()->put('filter_payment_type_creditor', $request->input('filter_payment_type_creditor'));
        } else {
            $request->session()->forget('filter_payment_type_creditor');
        }
        if($request->input('filter_payment_unit')) {
            $request->session()->put('filter_payment_unit', $request->input('filter_payment_unit'));
        } else {
            $request->session()->forget('filter_payment_unit');
        }

        print_r($request->input('filter_payment_unit'));
        return redirect()->route('payments.index');
    }
    public function clear(Request $request) {
        $request->session()->forget('filter_payment_date');
        $request->session()->forget('filter_payment_date');
        $request->session()->forget('filter_payment_element');
        $request->session()->forget('filter_payment_source_resource');
        $request->session()->forget('filter_payment_status');
        $request->session()->forget('filter_payment_unit');
        return redirect()->route('payments.index');
    }

    private function isMy($id) {
        $paymentsId = array();
        foreach (Payment::listMyUser(Auth::user()->id) as $payments) {
            $paymentsId[] = $payments->id;
        };
        return in_array($id, $paymentsId);
    }
}
