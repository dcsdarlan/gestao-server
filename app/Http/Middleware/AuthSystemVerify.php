<?php
/**
 * Created by PhpStorm.
 * User: darlan
 * Date: 21/12/16
 * Time: 08:35
 */
namespace App\Http\Middleware;

use App\Instituition;
use Closure;
use Illuminate\Support\Facades\Auth;
use stdClass;

class AuthsystemVerify {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if(!Instituition::findByToken($request->header('token'))) {
            $erro = new stdClass();
            $erro->erro = "Token inválido";
            return response()->json($erro, 401, [], JSON_UNESCAPED_UNICODE);
        }
        return $next($request);
    }
}