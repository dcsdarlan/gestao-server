<?php
/**
 * Created by PhpStorm.
 * User: darlan
 * Date: 21/12/16
 * Time: 08:39
 */
namespace App\Http\Middleware\TypeUser;

use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;
use Exception;
use stdClass;

class GestorApiVerify {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next) {
        try {
            if(JWTAuth::setToken($request->header('token'))->toUser()->type->description != "Administrador" &&
                JWTAuth::setToken($request->header('token'))->toUser()->type->description != "Gestor") {
                $erro = new stdClass();
                $erro->erro = "Token inválido";
                return response()->json($erro, 401, [], JSON_UNESCAPED_UNICODE);
            }
        } catch (Exception $e) {
            $erro = new stdClass();
            $erro->erro = "acesso não autorizado";
            return response()->json($erro, 401, [], JSON_UNESCAPED_UNICODE);
        }
        return $next($request);
    }
}