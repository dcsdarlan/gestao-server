<?php
/**
 * Created by PhpStorm.
 * User: darlan
 * Date: 21/12/16
 * Time: 08:38
 */
namespace App\Http\Middleware\TypeUser;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminVerify {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if(Auth::user()->type->description != "Administrador") {
            abort(404);
        }
        return $next($request);
    }
}