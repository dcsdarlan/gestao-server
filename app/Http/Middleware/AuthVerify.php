<?php
/**
 * Created by PhpStorm.
 * User: darlan
 * Date: 21/12/16
 * Time: 08:35
 */
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthVerify {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if(!Auth::check()) {
            Auth::logout();
            return redirect()->route('auth.login')->with('erro', "Email e/ou Senha inválido(s)");
        }
        return $next($request);
    }
}