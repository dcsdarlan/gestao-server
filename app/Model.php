<?php
/**
 * Created by PhpStorm.
 * User: darlan
 * Date: 07/12/16
 * Time: 21:43
 */

namespace App;

use Illuminate\Database\Eloquent\Model as LaravelModel;
use Carbon\Carbon;


class Model extends LaravelModel {
    protected $dFormat = 'd/m/Y';
    protected $dtFormat = 'd/m/Y H:i:s';
    protected $dates = ['created_at', 'updated_at'];
    protected $casts = [
        'active' => 'boolean'
    ];
    protected function formatNullFromString($attr) {
        return ($attr) ? $attr : "";
    }
    protected function formatDateFromString($attr) {
        return Carbon::parse($attr)->format($this->dFormat);
    }
    protected function formatDateTimeFromString($attr) {
        return Carbon::parse($attr)->format($this->dtFormat);
    }
    public function getUpdatedAtAttribute($attr) {
        return $this->formatDateTimeFromString($attr);
    }
    public function getCreatedAtAttribute($attr) {
        return $this->formatDateTimeFromString($attr);
    }
    public function getActiveAttribute() {
        return ($this->attributes['active']) ? true: false;
    }
}