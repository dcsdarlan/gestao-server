<?php

namespace App;

/**
 * Class Instituition
 */
class Instituition extends Model {
    protected $table = 'instituitions';
    public $timestamps = true;
    protected $fillable = [
        'id_instituition',
        'token',
        'description',
        'active'
    ];
    protected $hidden = [
        'token',
    ];
    private static $instituitionsList;
    public function getIdInstituitionAttribute($attr) {
        return $this->formatNullFromString($attr);
    }
    public function instituition() {
        return $this->hasOne('App\Instituition', 'id', 'id_instituition');
    }
    public static function findByToken($token) {
        return Instituition::where("token", "=", $token)->first();
    }
    public static function listBySuperInstituition($id) {
        Instituition::$instituitionsList = array();
        Instituition::$instituitionsList[] = Instituition::find($id);
        Instituition::listInstituitions($id);
        return Instituition::$instituitionsList;
    }
    private static function isSuper($id) {
        return count(Instituition::where("id_instituition", "=", $id)->get()) > 0;
    }
    private static function listInstituitions($id) {
        foreach (Instituition::where("id_instituition", "=", $id)->get() as $instituition) {
            Instituition::$instituitionsList[] = $instituition;
            if(Instituition::isSuper($instituition->id)) {
                Instituition::listInstituitions($instituition->id);
            }
        }
    }
}