<?php

namespace App;

use DateTime;

class Expense extends Model {
  protected $table = 'expenses';
  public $timestamps = true;
  protected $fillable = [
      'id_type',
      'description',
      'value',
      'date'
  ];
  public function getValueAttribute($attr) {
      setlocale(LC_MONETARY, 'pt_BR.UTF-8', 'Portuguese_Brazil.1252');
      return money_format('%n', $attr);
  }
  public function getDateAttribute($attr) {
      return $this->formatDateFromString($attr);
  }
  public function type() {
      return $this->hasOne('App\TypeExpense', 'id', 'id_type');
  }
  public static function listBySuperInstituition($id) {
      $typesId = array();
      foreach (TypeExpense::listBySuperInstituition($id) as $type) {
          $typesId[] = $type->id;
      }
      return Expense::whereIn("id_type", $typesId)->get();
  }
  public static function listGraphic($id, $init, $end) {
      return Expense::where("id_type", "=" , $id)
              ->whereBetween('date', array($init, $end))
              ->get();
  }
}
