<?php

namespace App;

use Illuminate\Support\Facades\Session;

/**
 * Class Payment
 */
class Payment extends Model {
    protected $table = 'payments';
    public $timestamps = true;
    protected $fillable = [
        'date',
        'code',
        'type_payment',
        'id_administrative_unit',
        'type_expense',
        'invoice',
        'element',
        'source_resource',
        'gross_amount',
        'discount',
        'net_amount',
        'bidding',
        'contract',
        'creditor',
        'type_creditor',
        'historic',
        'id_user_manager',
        'operated_at',
        'status',
        'observation'
    ];
    public function getObservationAttribute($attr) {
        return $this->formatNullFromString($attr);
    }
    public function getHistoricAttribute($attr) {
        return $this->formatNullFromString($attr);
    }
    public function getDateAttribute($attr) {
        return $this->formatDateFromString($attr);
    }
    public function getOperatedAtAttribute($attr) {
        return $attr ? $this->formatDateTimeFromString($attr) : $this->formatNullFromString($attr);
    }
    public function getNetAmountAttribute($attr) {
        setlocale(LC_MONETARY, 'pt_BR.UTF-8', 'Portuguese_Brazil.1252');
        return money_format('%n', $attr);
    }
    public function getGrossAmountAttribute($attr) {
        setlocale(LC_MONETARY, 'pt_BR.UTF-8', 'Portuguese_Brazil.1252');
        return money_format('%n', $attr);
    }
    public function getDiscountAttribute($attr) {
        setlocale(LC_MONETARY, 'pt_BR.UTF-8', 'Portuguese_Brazil.1252');
        return money_format('%n', $attr);
    }
    public function administrative_unit() {
        return $this->hasOne('App\AdministrativeUnit', 'id', 'id_administrative_unit');
    }
    public function user_manager() {
        return $this->hasOne('App\User', 'id', 'id_user_manager');
    }
    public static function listByUser($id) {
        $unitsId = array();
        foreach (User::find($id)->administratives_units as $unit) {
            $unitsId[] = $unit->id;
        }
        $payments = Payment::whereIn("id_administrative_unit", $unitsId);
        if(Session::has('filter_payment_date')) {
            $dates = explode(" - ", Session::get("filter_payment_date"));
            $dateInit = implode('-', array_reverse(explode('/', $dates[0])));
            $dateEnd = implode('-', array_reverse(explode('/', $dates[1])));
            $payments->whereBetween('date', array($dateInit, $dateEnd));
        } else {
            $payments->where('date', "=", date('Y-m-d'));
        }
        if(Session::has('filter_payment_creditor')) {
            $payments->where('creditor', "like", "%".Session::get('filter_payment_creditor')."%");
        }
        if(Session::has('filter_payment_type_creditor')) {
            $payments->where('type_creditor', "like", "%".Session::get('filter_payment_type_creditor')."%");
        }
        if(Session::has('filter_payment_element')) {
            $payments->where('element', "like", "%".Session::get('filter_payment_element')."%");
        }
        if(Session::has('filter_payment_source_resource')) {
            $payments->where('source_resource', "like", "%".Session::get('filter_payment_source_resource')."%");
        }
        if(Session::has('filter_payment_unit')) {
            $payments->whereIn('id_administrative_unit', Session::get('filter_payment_unit'));
        }
        if(!Session::has('filter_payment_status')) {
            $payments->where('status', "=", "waiting");
        } elseif(Session::get('filter_payment_status') != "all") {
            $payments->where('status', "=", Session::get('filter_payment_status'));
        }
        return $payments->with("administrative_unit")->with("user_manager")->with("administrative_unit.instituition")->get();
    }

    public static function listMyUser($id) {
        $unitsId = array();
        foreach (User::find($id)->administratives_units as $unit) {
            $unitsId[] = $unit->id;
        }
        $payments = Payment::whereIn("id_administrative_unit", $unitsId);
        return $payments->with("administrative_unit")->with("user_manager")->with("administrative_unit.instituition")->get();
    }

    public static function apiListByUser($id,
                                         $filterPaymentDate = null,
                                         $filterPaymentUnit = null,
                                         $filterPaymentCreditor = null,
                                         $filterPaymentTypeCreditor = null,
                                         $filterPaymentElement = null,
                                         $filterPaymentSourceResource = null,
                                         $filterPaymentStatus = null) {
        $unitsId = array();
        foreach (User::find($id)->administratives_units as $unit) {
            $unitsId[] = $unit->id;
        }
        $payments = Payment::whereIn("id_administrative_unit", $unitsId);
        if($filterPaymentDate) {
            $dates = explode(" - ", $filterPaymentDate);
            $dateInit = implode('-', explode('/', $dates[0]));
            $dateEnd = implode('-', explode('/', $dates[1]));
            $payments->whereBetween('date', array($dateInit, $dateEnd));
        } else {
            $payments->where('date', "=", date('Y-m-d'));
        }
        if($filterPaymentCreditor) {
            $payments->where('creditor', "like", "%".$filterPaymentCreditor."%");
        }
        if($filterPaymentTypeCreditor) {
            $payments->where('type_creditor', "like", "%".$filterPaymentTypeCreditor."%");
        }
        if($filterPaymentElement) {
            $payments->where('element', "like", "%".$filterPaymentElement."%");
        }
        if($filterPaymentSourceResource) {
            $payments->where('source_resource', "like", "%".$filterPaymentSourceResource."%");
        }
        if($filterPaymentUnit) {
            $payments->whereIn('id_administrative_unit', $filterPaymentUnit);
        }
        if(!$filterPaymentStatus) {
            $payments->where('status', "=", "waiting");
        } elseif($filterPaymentStatus != "all") {
            $payments->where('status', "=", $filterPaymentStatus);
        }
        return $payments->with("administrative_unit")->with("user_manager")->with("administrative_unit.instituition")->get();
    }
}