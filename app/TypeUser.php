<?php

namespace App;

/**
 * Class TypesUser
 */
class TypeUser extends Model {
    protected $table = 'types_users';
    public $timestamps = true;
    protected $fillable = [
        'description',
        'active'
    ];
}