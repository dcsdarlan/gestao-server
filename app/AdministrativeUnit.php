<?php

namespace App;

/**
 * Class AdministrativesUnit
 */
class AdministrativeUnit extends Model {
    protected $table = 'administratives_units';
    public $timestamps = true;
    protected $fillable = [
        'id_instituition',
        'code',
        'description',
        'active'
    ];
    public static function findByInstituitionCode($instituition, $code) {
        return AdministrativeUnit::where("id_instituition", "=", $instituition)->where("code", "=", $code)->first();
    }
    public function getCodeAttribute($attr) {
        return $this->formatNullFromString($attr);
    }
    public function instituition() {
        return $this->hasOne('App\Instituition', 'id', 'id_instituition');
    }
    public function payments() {
        return $this->hasMany('App\Payment', 'id_administrative_unit', 'id');
    }
    public function users() {
        return $this->belongsToMany('App\User', 'users_administratives_units', 'id_administrative_unit', 'id_user');
    }
    public static function listBySuperInstituition($id) {
        $instituitionsId = array();
        foreach (Instituition::listBySuperInstituition($id) as $instituition) {
            $instituitionsId[] = $instituition->id;
        }
        return AdministrativeUnit::whereIn("id_instituition", $instituitionsId)->get();
    }
    public static function listByInstituition($id) {
        return AdministrativeUnit::where("id_instituition", "=", $id)->get();
    }
}
