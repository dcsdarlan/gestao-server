<?php

namespace App;

/**
 * Class UsersAdministrativesUnit
 */
class Device extends Model {
    protected $table = 'devices';
    public $timestamps = true;
    protected $fillable = [
        'id_user',
        'token',
        'uuid',
        'platform',
        'active'
    ];
    public function user() {
        return $this->hasOne('App\User', 'id', 'id_user');
    }
    public static function findByUuid($id) {
        return Device::where("uuid", "=" , $id)
            ->first();
    }
}