<?php

namespace App;

class Revenue extends Model {
  protected $table = 'revenues';
  public $timestamps = true;
  protected $fillable = [
      'id_administrative_unit',
      'description',
      'value',
      'date'
  ];
  public function getValueAttribute($attr) {
      setlocale(LC_MONETARY, 'pt_BR.UTF-8', 'Portuguese_Brazil.1252');
      return money_format('%n', $attr);
  }
  public function getDateAttribute($attr) {
      return $this->formatDateFromString($attr);
  }
  public function administrative_unit() {
      return $this->hasOne('App\AdministrativeUnit', 'id', 'id_administrative_unit');
  }
  public static function listBySuperInstituition($id) {
      $unitId = array();
      foreach (AdministrativeUnit::listBySuperInstituition($id) as $unit) {
          $unitId[] = $unit->id;
      }
      return Revenue::whereIn("id_administrative_unit", $unitId)->get();
  }
  public static function listGraphic($id, $init, $end) {
      return Revenue::where("id_administrative_unit", "=" , $id)
              ->whereBetween('date', array($init, $end))
              ->get();
  }
}
