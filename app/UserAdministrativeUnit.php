<?php

namespace App;

/**
 * Class UsersAdministrativesUnit
 */
class UserAdministrativeUnit extends Model {
    protected $table = 'users_administratives_units';
    public $timestamps = false;
    protected $fillable = [
        'id_user',
        'id_administrative_unit'
    ];
    public function administrative_unit() {
        return $this->hasOne('App\AdministrativeUnit', 'id', 'id_administrative_unit');
    }
    public function user() {
        return $this->hasOne('App\User', 'id', 'id_user');
    }
}