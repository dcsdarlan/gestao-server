<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;

/**
 * Class User
 */
class User extends Authenticatable {
    use Notifiable;

    protected $table = 'users';
    public $timestamps = true;
    protected $fillable = [
        'name',
        'password',
        'id_type',
        'id_instituition',
        'email',
        'active'
    ];
    protected $hidden = [
        'password',
        'remember_token'
    ];
    protected $dFormat = 'd/m/Y';
    protected $dtFormat = 'd/m/Y H:i:s';
    protected $dates = ['created_at', 'updated_at'];
    protected $casts = [
        'active' => 'boolean'
    ];
    protected function formatNullFromString($attr) {
        return ($attr) ? $attr : "";
    }
    protected function formatDateFromString($attr) {
        return Carbon::parse($attr)->format($this->dFormat);
    }
    protected function formatDateTimeFromString($attr) {
        return Carbon::parse($attr)->format($this->dtFormat);
    }
    public function getUpdatedAtAttribute($attr) {
        return $this->formatDateTimeFromString($attr);
    }
    public function getCreatedAtAttribute($attr) {
        return $this->formatDateTimeFromString($attr);
    }
    public function getActiveAttribute() {
        return ($this->attributes['active']) ? true: false;
    }
    public function type() {
        return $this->hasOne('App\TypeUser', 'id', 'id_type');
    }
    public function devices() {
        return $this->hasMany('App\Device', 'id_user', 'id');
    }
    public function instituition() {
        return $this->hasOne('App\Instituition', 'id', 'id_instituition');
    }
    public function administratives_units() {
        return $this->belongsToMany('App\AdministrativeUnit', 'users_administratives_units', 'id_user', 'id_administrative_unit');
    }
    public static function listBySuperInstituition($id) {
        $instituitionsId = array();
        foreach (Instituition::listBySuperInstituition($id) as $instituition) {
            $instituitionsId[] = $instituition->id;
        }
        return User::whereIn("id_instituition", $instituitionsId)->with("type")->with("instituition")->with("administratives_units")->get();
    }
}