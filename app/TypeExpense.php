<?php

namespace App;

/**
 * Class TypeExpense
 */
class TypeExpense extends Model {
  protected $table = 'types_expenses';
  public $timestamps = true;
  protected $fillable = [
      'id_administrative_unit',
      'code',
      'description',
      'maximum_limit',
      'prudential_limit',
      'alert_limit',
      'active'
  ];
  public function administrative_unit() {
      return $this->hasOne('App\AdministrativeUnit', 'id', 'id_administrative_unit');
  }
  public static function listBySuperInstituition($id) {
      $unitsId = array();
      foreach (AdministrativeUnit::listBySuperInstituition($id) as $unit) {
          $unitsId[] = $unit->id;
      }
      return TypeExpense::whereIn("id_administrative_unit", $unitsId)->get();
  }
  public static function listByAdministrativeUnit($id) {
      return TypeExpense::where("id_administrative_unit", "=", $id)->get();
  }
}
