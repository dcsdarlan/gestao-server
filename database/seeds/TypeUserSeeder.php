<?php

use Illuminate\Database\Seeder;

class TypeUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('types_users')->insert([
          'description' => "Administrador",
          'active' => true
      ]);
      DB::table('types_users')->insert([
          'description' => "Gestor",
          'active' => true
      ]);
        DB::table('types_users')->insert([
            'description' => "Funcionário",
            'active' => true
        ]);
    }
}
