<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TypeUserSeeder::class);
        $this->call(InstituitionSeeder::class);
        $this->call(AdministrativeUnitSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(UserAdministrativeUnitSeeder::class);
    }
}
