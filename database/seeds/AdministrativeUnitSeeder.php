<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdministrativeUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('administratives_units')->insert([
          'description' => "Sercretaria",
          'id_instituition' => 1,
          'active' => true,
          'code' => "S1"
      ]);
    }
}
