<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
          'name' => "Darlan",
          'id_type' => 1,
          'id_instituition' => 1,
          'email' => "darlan@tudomunicipal.com.br",
          'password' => Hash::make(12345),
          'active' => true
      ]);
    }
}
