<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserAdministrativeUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users_administratives_units')->insert([
          'id_user' => 1,
          'id_administrative_unit' => 1
      ]);
    }
}
