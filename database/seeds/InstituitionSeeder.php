<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class InstituitionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('instituitions')->insert([
          'description' => "Prefeitura Municipal",
          'active' => true,
          'token' => Hash::make("Prefeitura Municipal") . Hash::make(0) . Hash::make(date("d-m-Y h:i:s"))
      ]);
    }
}
