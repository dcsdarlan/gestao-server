<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->date('date');
			$table->string('code');
			$table->string('type_payment');
			$table->integer('id_administrative_unit');
			$table->string('type_expense');
			$table->string('invoice');
			$table->string('element');
			$table->string('source_resource');
			$table->double('gross_amount');
			$table->double('discount');
			$table->double('net_amount');
			$table->string('bidding');
			$table->string('contract');
			$table->string('creditor');
			$table->string('type_creditor');
			$table->text('historic')->nullable();
			$table->integer('id_user_manager')->nullable();
			$table->dateTime('operated_at')->nullable();
            $table->enum('status', array('waiting','confirmed','canceled'))->default('waiting');
            $table->string('observation', 500)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payments');
	}

}
