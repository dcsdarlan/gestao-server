<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypesExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('types_expenses', function(Blueprint $table)
      {
        $table->increments('id');
        $table->string('code')->nullable();
        $table->integer('id_administrative_unit');
        $table->string('description');
        $table->integer('maximum_limit')->nullable();
        $table->integer('prudential_limit')->nullable();
        $table->integer('alert_limit')->nullable();
        $table->boolean('active');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('types_expenses');
    }
}
