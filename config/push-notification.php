<?php

return array(

    'gestaoIOS'     => array(
        'environment' =>'development',
        'certificate' =>'/path/to/certificate.pem',
        'passPhrase'  =>'password',
        'service'     =>'apns'
    ),
    'gestaoAndroid' => array(
        'environment' =>'production',
        'apiKey'      =>'AIzaSyCVHaQ2Jvf9nW_REgBChIDJ2MAozrnlys0',
        'service'     =>'gcm'
    )

);