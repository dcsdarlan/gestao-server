<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => 'cors'], function(){
  Route::group(['prefix' => 'v1'], function(){
      Route::post('/auth', [
          'as' => 'api.auth.logon',
          'uses' => 'Api\V1\AuthApiController@logon'
      ]);
      Route::post('/graphics/month', [
          'as' => 'grafics.show.month',
          'uses' => 'Api\V1\GraphicsApiController@showMonth'
      ]);

      Route::post('/graphics/year', [
          'as' => 'grafics.show.year',
          'uses' => 'Api\V1\GraphicsApiController@showYear'
      ]);

      Route::post('/devices', [
          'as' => 'devices.store',
          'uses' => 'Api\V1\DevicesApiController@store'
      ]);

      Route::resource('revenues', 'Api\V1\RevenuesApiController');
      Route::resource('expenses', 'Api\V1\ExpensesApiController');
      Route::resource('typesexpenses', 'Api\V1\TypeExpensesApiController');

      Route::group(['prefix' => 'instituitions'], function(){
          Route::get('/all', [
              'as' => 'instituitions.all',
              'uses' => 'Api\V1\InstituitionsApiController@all'
          ]);
      });
      Route::group(['prefix' => 'administrativesunits'], function(){
          Route::get('instituitions/{instituition}', [
              'as' => 'administrativesunits.instituition.list',
              'uses' => 'Api\V1\AdministrativesUnitsApiController@listByInstituition'
          ]);
      });
      Route::group(['prefix' => 'typesexpenses'], function(){
          Route::get('administrativesunits/{instituition}', [
              'as' => 'typesexpenses.administrativesunits.list',
              'uses' => 'Api\V1\TypesExpensesApiController@listByAdministrativeUnit'
          ]);
      });
      Route::get('/profile', [
          'as' => 'profile.show',
          'uses' => 'Api\V1\ProfileApiController@show'
      ])->middleware("authApiGestor");

      Route::group(['middleware' => 'authApi'], function() {
          Route::group(['middleware' => 'authApiAdmin'], function() {
              Route::resource('users', 'Api\V1\UsersApiController');
              Route::resource('administrativesunits', 'Api\V1\AdministrativesUnitsApiController');
              Route::resource('instituitions', 'Api\V1\InstituitionsApiController');
          });
      });

      Route::group(['prefix' => 'payments'], function(){
          Route::post('/filters', [
              'as' => 'payments.filter',
              'uses' => 'Api\V1\PaymentsApiController@filter'
          ])->middleware("authApiGestor");
          Route::post('/approve', [
              'as' => 'payments.approve',
              'uses' => 'Api\V1\PaymentsApiController@approve'
          ])->middleware("authApiGestor");
          Route::post('/deprive', [
              'as' => 'payments.deprive',
              'uses' => 'Api\V1\PaymentsApiController@deprive'
          ])->middleware("authApiGestor");
          Route::get('{payment}', [
              'as' => 'payments.show',
              'uses' => 'Api\V1\PaymentsApiController@show'
          ])->middleware("authApiGestor");
          Route::get('/', [
              'as' => 'payments.index',
              'uses' => 'Api\V1\PaymentsApiController@index'
          ])->middleware("authApiAdmin");
          Route::delete('{payment}', [
              'as' => 'payments.destroy',
              'uses' => 'Api\V1\PaymentsApiController@destroy'
          ])->middleware("authSystem");
          Route::post('/', [
              'as' => 'payments.store',
              'uses' => 'Api\V1\PaymentsApiController@store'
          ])->middleware("authSystem");
      });
  });
});
