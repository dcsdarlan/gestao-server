<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::get('/login', [
    'as' => 'auth.login',
    'uses' => 'AuthController@login'
]);
Route::post('/logon', [
    'as' => 'auth.logon',
    'uses' => 'AuthController@logon'
]);
Route::get('/logoff', [
    'as' => 'auth.logoff',
    'uses' => 'AuthController@logoff'
]);
Route::group(['middleware' => 'auth'], function(){
    Route::get('/', [
        'as' => 'home.index',
        'uses' => 'HomeController@index'
    ]);
    Route::get('/profile', [
        'as' => 'profile.show',
        'uses' => 'ProfileController@show'
    ]);
    Route::post('/profile', [
        'as' => 'profile.update',
        'uses' => 'ProfileController@update'
    ]);
    Route::get('/graphics', [
        'as' => 'graphics.index',
        'uses' => 'GraphicsController@index'
    ]);
    Route::group(['middleware' => 'authFuncionario'], function() {
        Route::resource('typesexpenses', 'TypesExpensesController');
        Route::resource('expenses', 'ExpensesController');
        Route::resource('revenues', 'RevenuesController');
    });

    Route::group(['middleware' => 'authAdmin'], function() {
        Route::resource('instituitions', 'InstituitionsController');
        Route::group(['prefix' => 'instituitions'], function(){
            Route::post('/token', [
                'as' => 'instituitions.token',
                'uses' => 'InstituitionsController@token'
            ]);
        });
        Route::resource('administrativesunits', 'AdministrativesUnitsController');
        Route::resource('users', 'UsersController');
        Route::group(['prefix' => 'users/administrativesunits'], function(){
            Route::post('/add', [
                'as' => 'users.administrativesunits.add',
                'uses' => 'UsersController@addUnit'
            ]);
            Route::post('/rm', [
                'as' => 'users.administrativesunits.rm',
                'uses' => 'UsersController@rmUnit'
            ]);
        });
    });
    Route::group(['middleware' => 'authGestor'], function() {
        Route::resource('payments', 'PaymentsController');
        Route::group(['prefix' => 'payments'], function(){
            Route::post('/approve', [
                'as' => 'payments.approve',
                'uses' => 'PaymentsController@approve'
            ]);
            Route::post('/deprive', [
                'as' => 'payments.deprive',
                'uses' => 'PaymentsController@deprive'
            ]);
            Route::post('/filter', [
                'as' => 'payments.filter',
                'uses' => 'PaymentsController@filter'
            ]);
            Route::get('/filter/clear', [
                'as' => 'payments.filter.clear',
                'uses' => 'PaymentsController@clear'
            ]);
        });
    });
});
