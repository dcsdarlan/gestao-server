# Gestão Api V1
---

Versão Estável: 1.0.0

- [Usuários](#markdown-header-api-usuarios)
    - [Autenticação](#markdown-header-autenticacao)
    - [Listar](#markdown-header-usuarios-listar)
    - [Detalhar](#markdown-header-usuarios-detalhar)
    - [Adicionar](#markdown-header-usuarios-adicionar)
    - [Alteração](#markdown-header-usuarios-alterar)
    - [Apagar](#markdown-header-usuarios-apagar)
- [Instituições(PREFEITURAS)](#markdown-header-api-instituicoes)
    - [Listar](#markdown-header-instituicoes-listar)
    - [Detalhar](#markdown-header-instituicoes-detalhar)
    - [Adicionar](#markdown-header-instituicoes-adicionar)
    - [Alteração](#markdown-header-instituicoes-alterar)
    - [Apagar](#markdown-header-instituicoes-apagar)
- [Uniades Administrativas(SECRETARIAS)](#markdown-header-api-unidades)
    - [Listar](#markdown-header-unidades-listar)
    - [Detalhar](#markdown-header-unidades-detalhar)
    - [Adicionar](#markdown-header-unidades-adicionar)
    - [Alteração](#markdown-header-unidades-alterar)
    - [Apagar](#markdown-header-unidades-apagar)
- [Pagamentos](#markdown-header-api-pagamentos)
    - [Adicionar](#markdown-header-pagamentos-adicionar)
    - [Apagar](#markdown-header-pagamentos-apagar)
    - [Listar](#markdown-header-pagamentos-listar)
    - [Detalhar](#markdown-header-pagamentos-detalhar)    
    - [Filtrar](#markdown-header-pagamentos-filtrar)
    - [Aprovar](#markdown-header-pagamentos-aprovar)
    - [Cancelar](#markdown-header-pagamentos-cancelar)

---

## Usuários

### Autenticação

> Requisita os dados de acesso do cliente.

* url: http://*url_do_projeto*/api/v1/auth
* HTTP Method: POST

#### Requisição :
{
	"email": "darlan@tudomunicipal.com.br",
	"password": "12345"
}

#### Resposta :

##### Sucesso :
(http status 200)
```
{  
   "token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDAwXC9hcGlcL3YxXC9hdXRoIiwiaWF0IjoxNDgyMzQ5NzQxLCJleHAiOjE0ODIzNTMzNDEsIm5iZiI6MTQ4MjM0OTc0MSwianRpIjoiZTBjMGQ5MTM2NDhhODE4YjgwOTg5NWVjZjg0Y2MzYTMifQ.qU-MqntEEkhcTobMUSDXONWTA8x7tW_qbwJCtftQ5e4"
}
```
OBS: Essas informações devem ser enviadas no cabeçalho de **algumas** das urls utilizadas para operações realizadas pelo cliente.

##### Erro:
(http status 401)
```
{
  "erro":"email e/ou senha inválido(s)"
}
```

(http status 500)
```
{
  "erro": "erro ao gerar token."
}
```

### Listar

> Requisita a lista de usuarios cadastrados.

> Obs: Enviar token de usuario no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/users
* HTTP Method: GET

#### Resposta :

##### Sucesso :
(http status 200)
```
[{
    id: 8,
    name: "Darlan",
    id_type: "1",
    id_instituition: "1",
    email: "darlan@tudomunicipal.com.br",
    image: "",
    active: true,
    created_at: "19/12/2016 17:33:57",
    updated_at: "19/12/2016 17:33:57",
    type: {
        id: 1,
        description: "Administrador",
        active: true,
        created_at: "19/12/2016 18:15:18",
        updated_at: "19/12/2016 18:15:18"
    },
    instituition: {
        id: 1,
        id_instituition: "",
        description: "Tudo Municipal",
        active: true,
        created_at: "19/12/2016 18:15:18",
        updated_at: "19/12/2016 18:15:18"
    },
    administratives_units: [{
        id: 3,
        id_instituition: 1,
        description: "Desenvolvimento",
        active: true,
        created_at: "30/11/2016 11:36:27",
        updated_at: "30/11/2016 11:36:27",
        pivot: {
            id_user: 8,
            id_administrative_unit: 3
        }
    }]
}]
```

##### Erro:
(http status 204)
```
{
  "erro": "não existe dados para a requisição"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.

### Detalhar

> Detalhar um usuário cadastrado.

> Obs: Enviar token de usuario no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/users/*id_usuario*
* HTTP Method: GET

#### Resposta :

##### Sucesso :
(http status 200)
```
{
    id: 8,
    name: "Darlan",
    id_type: "1",
    id_instituition: "1",
    email: "darlan@tudomunicipal.com.br",
    active: true,
    created_at: "19/12/2016 17:33:57",
    updated_at: "19/12/2016 17:33:57",
    type: {
        id: 1,
        description: "Administrador",
        active: true,
        created_at: "19/12/2016 18:15:18",
        updated_at: "19/12/2016 18:15:18"
    },
    instituition: {
        id: 1,
        id_instituition: "",
        description: "Tudo Municipal",
        active: true,
        created_at: "19/12/2016 18:15:18",
        updated_at: "19/12/2016 18:15:18"
    },
    administratives_units: [{
        id: 3,
        id_instituition: 1,
        description: "Desenvolvimento",
        active: true,
        created_at: "30/11/2016 11:36:27",
        updated_at: "30/11/2016 11:36:27",
        pivot: {
            id_user: 8,
            id_administrative_unit: 3
        }
    }]
}
```

##### Erro:
(http status 404)
```
{
  "erro": "não existe dados para a requisição"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.

### Adicionar

> Adiciona um usuário.

> Obs: Enviar token de usuario no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/users
* HTTP Method: POST

#### Requisição :
```
{
    name: "Darlan",
    id_type: "1",
    id_instituition: "1",
    password: "12345",
    email: "darlan@tudomunicipal.com.br",
    active: true
}
```

#### Resposta :

##### Sucesso :
(http status 201)
```
{
    id: 8,
    name: "Darlan",
    id_type: "1",
    id_instituition: "1",
    email: "darlan@tudomunicipal.com.br",
    active: true,
    created_at: "19/12/2016 17:33:57",
    updated_at: "19/12/2016 17:33:57",    
}
```

##### Erro:
(http status 400)
```
{
  "erro": "mensagem de validação"
}
```

(http status 500)
```
{
  "erro": "Acorreu um erro ao tentar salvar os dados"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.


### Alterar

> Altera um usuário.

> Obs: Enviar token de usuario no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/users/*id_usuario*
* HTTP Method: PUT

#### Requisição :
```
{
    name: "Darlan",
    id_type: "1",
    id_instituition: "1",
    password: "12345",
    email: "darlan@tudomunicipal.com.br",
    active: true
}
```

#### Resposta :

##### Sucesso :
(http status 204)
```
{
    id: 8,
    name: "Darlan",
    id_type: "1",
    id_instituition: "1",
    email: "darlan@tudomunicipal.com.br",
    active: true    
}
```

##### Erro:
(http status 304)
```
{
  "erro": "ocorreu um erro ao tentar salvar os dados"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.

### Apagar

> Apaga um usuário.

> Obs: Enviar token de usuario no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/users/*id_usuario*
* HTTP Method: DELETE

#### Resposta :

##### Sucesso :
(http status 304)
```
{
    id: 8,
    name: "Darlan",
    id_type: "1",
    id_instituition: "1",
    email: "darlan@tudomunicipal.com.br",
    active: true    
}
```

##### Erro:
(http status 500)
```
{
  "erro": "ocorreu um erro ao tentar salvar os dados"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.

---

## Instituições(PREFEITURAS)

### Listar

> Requisita a lista de instituições(PREFEITURAS) cadastradas.

> Obs: Enviar token de usuario no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/instituitions
* HTTP Method: GET

#### Resposta :

##### Sucesso :
(http status 200)
```
[{
    id: 1,
    id_instituition: "",
    description: "Tudo Municipal",
    active: true,
    created_at: "19/12/2016 19:14:51",
    updated_at: "19/12/2016 19:14:51"
}, {
    id: 2,
    id_instituition: 1,
    description: "Prefeitura de Fortaleza",
    active: true,
    created_at: "29/11/2016 16:11:43",
    updated_at: "29/11/2016 16:11:43"
}]
```

##### Erro:
(http status 204)
```
{
  "erro": "não existe dados para a requisição"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.

### Detalhar

> Detalhar uma instituição(PREFEITURA) cadastrada.

> Obs: Enviar token de usuario no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/instituitions/*id_instituicao*
* HTTP Method: GET

#### Resposta :

##### Sucesso :
(http status 200)
```
{
    id: 1,
    id_instituition: "",
    description: "Tudo Municipal",
    active: true,
    created_at: "19/12/2016 19:14:51",
    updated_at: "19/12/2016 19:14:51"
}
```

##### Erro:
(http status 404)
```
{
  "erro": "não existe dados para a requisição"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.

### Adicionar

> Adiciona uma instituição(PREFEITURA).

> Obs: Enviar token de usuario no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/instituitions
* HTTP Method: POST

#### Requisição :
```
{
    id_instituition: "1",
    description: "Tudo Municipal",
    active: true
}
```

#### Resposta :

##### Sucesso :
(http status 201)
```
{
    id: 1,
    id_instituition: "",
    description: "Tudo Municipal",
    active: true,
    created_at: "19/12/2016 19:14:51",
    updated_at: "19/12/2016 19:14:51"
}
```

##### Erro:
(http status 400)
```
{
  "erro": "mensagem de validação"
}
```

(http status 500)
```
{
  "erro": "ocorreu um erro ao tentar salvar os dados"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.


### Alterar

> Altera uma instituição(PREFEITURA).

> Obs: Enviar token de usuario no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/instituitions/*id_usuario*
* HTTP Method: PUT

#### Requisição :
```
{
    id_instituition: "1",
    description: "Tudo Municipal",
    active: true
}
```

#### Resposta :

##### Sucesso :
(http status 204)
```
{
    id: 1,
    id_instituition: "",
    description: "Tudo Municipal",
    active: true,
    created_at: "19/12/2016 19:14:51",
    updated_at: "19/12/2016 19:14:51"
}
```

##### Erro:
(http status 304)
```
{
  "erro": "ocorreu um erro ao tentar salvar os dados"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.

### Apagar

> Apaga uma instituição(PREFEITURA).

> Obs: Enviar token de usuario no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/instituitions/*id_instituicao*
* HTTP Method: DELETE

#### Resposta :

##### Sucesso :
(http status 304)
```
{
    id: 1,
    id_instituition: "",
    description: "Tudo Municipal",
    active: true,
    created_at: "19/12/2016 19:14:51",
    updated_at: "19/12/2016 19:14:51"
}
```

##### Erro:
(http status 500)
```
{
  "erro": "ocorreu um erro ao tentar salvar os dados"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.

---

## Unidades Adiministrativas(SECRETARIAS)

### Listar

> Requisita a lista de Unidades Gestoras(SECRETARIAS) cadastradas.

> Obs: Enviar token de usuario no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/administrativesunits
* HTTP Method: GET

#### Resposta :

##### Sucesso :
(http status 200)
```
[{
    id: 1,
    id_instituition: 2,
    description: "Sercretaria de Saúde Pública",
    active: true,
    created_at: "29/11/2016 16:11:55",
    updated_at: "29/11/2016 16:34:47"
}, {
    id: 2,
    id_instituition: 2,
    description: "Secretaria de Segurança Pública",
    active: true,
    created_at: "29/11/2016 16:14:34",
    updated_at: "29/11/2016 16:35:36"
}]
```

##### Erro:
(http status 204)
```
{
  "erro": "não existe dados para a requisição"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.

### Detalhar

> Detalhar uma unidade administrativa(SECRETARIA) cadastrada.

> Obs: Enviar token de usuario no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/adminsitrativesunits/*id_unidade*
* HTTP Method: GET

#### Resposta :

##### Sucesso :
(http status 200)
```
{
    id: 2,
    id_instituition: 2,
    description: "Secretaria de Segurança Pública",
    active: true,
    created_at: "29/11/2016 16:14:34",
    updated_at: "29/11/2016 16:35:36"
}
```

##### Erro:
(http status 404)
```
{
  "erro": "não existe dados para a requisição"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.

### Adicionar

> Adiciona uma unidade administrativa(SECRETARIA).

> Obs: Enviar token de usuario no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/administrativesunits
* HTTP Method: POST

#### Requisição :
```
{
    id_instituition: 2,
    description: "Secretaria de Segurança Pública",
    active: true
}
```

#### Resposta :

##### Sucesso :
(http status 201)
```
{
    id: 2,
    id_instituition: 2,
    description: "Secretaria de Segurança Pública",
    active: true,
    created_at: "29/11/2016 16:14:34",
    updated_at: "29/11/2016 16:35:36"
}
```

##### Erro:
(http status 400)
```
{
  "erro": ["mensagens de validação"]
}
```

(http status 500)
```
{
  "erro": "ocorreu um erro ao tentar salvar os dados"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.


### Alterar

> Altera uma unidade administrativa(SECRETARIA).

> Obs: Enviar token de usuario no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/administrativesunits/*id_unidade*
* HTTP Method: PUT

#### Requisição :
```
{
    id_instituition: 2,
    description: "Secretaria de Segurança Pública",
    active: true
}
```

#### Resposta :

##### Sucesso :
(http status 204)
```
{
    id: 2,
    id_instituition: 2,
    description: "Secretaria de Segurança Pública",
    active: true,
    created_at: "29/11/2016 16:14:34",
    updated_at: "29/11/2016 16:35:36"
}
```

##### Erro:
(http status 304)
```
{
  "erro": "ocorreu um erro ao tentar salvar os dados"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.

### Apagar

> Apaga uma unidade administativa(SECRETARIA).

> Obs: Enviar token de usuario no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/administrativeunits/*id_instituicao*
* HTTP Method: DELETE

#### Resposta :

##### Sucesso :
(http status 304)
```
{
    id: 2,
    id_instituition: 2,
    description: "Secretaria de Segurança Pública",
    active: true,
    created_at: "29/11/2016 16:14:34",
    updated_at: "29/11/2016 16:35:36"
}
```

##### Erro:
(http status 500)
```
{
  "erro": "ocorreu um erro ao tentar salvar os dados"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.

---

## Pagamentos

### Adicionar

> Adiciona um pagamento.

> Obs: Enviar token de instituição no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/payments
* HTTP Method: POST

#### Requisição :
```
{
    date: "2017-01-01",
    code: "7894123",
    type_payment: "AGUA",
    administrative_unit: "Nome da Secretaria",
    code_administrative_unit: "S1",
    type_expense: "MENSAL",
    invoice: "BR 23456-9",
    element: "AGUA E ESGOTO",
    source_resource: "CONTABILIDADE",
    gross_amount: 10000,
    discount: 25.5,
    net_amount: 74.5,
    bidding: "1345678976456-198-adv",
    contract: "798789-ABS465465",
    creditor: "CAGECE",
    type_creditor: "PUBLICO",
    historic: "não existe historico",
    observation: ""
}
```

#### Resposta :

##### Sucesso :
(http status 201)
```
{
    id: 21,
    date: "05/12/2016",
    code: "7894123",
    type_payment: "AGUA",
    id_administrative_unit: 3,
    type_expense: "MENSAL",
    invoice: "BR 23456-9",
    element: "AGUA E ESGOTO",
    source_resource: "CONTABILIDADE",
    gross_amount: 10000,
    discount: 25.5,
    net_amount: 74.5,
    bidding: "1345678976456-198-adv",
    contract: "798789-ABS465465",
    creditor: "CAGECE",
    type_creditor: "PUBLICO",
    historic: "não existe historico",
    id_user_manager: "",
    operated_at: "",
    created_at: "01/12/2016 09:44:36",
    updated_at: "05/12/2016 16:54:27",
    observation: ""
}
```

##### Erro:
(http status 400)
```
{
  "erro": "mensagem de validação"
}
```

(http status 500)
```
{
  "erro": "ocorreu um erro ao tentar salvar os dados"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.

### Apagar

> Apaga um pagamento.

> Obs: Enviar token de instituição no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/payments/*id_pagamento*
* HTTP Method: DELETE

#### Resposta :

##### Sucesso :
(http status 304)
```
{
    id: 21,
    date: "05/12/2016",
    code: "7894123",
    type_payment: "AGUA",
    id_administrative_unit: 3,
    type_expense: "MENSAL",
    invoice: "BR 23456-9",
    element: "AGUA E ESGOTO",
    source_resource: "CONTABILIDADE",
    gross_amount: 10000,
    discount: 25.5,
    net_amount: 74.5,
    bidding: "1345678976456-198-adv",
    contract: "798789-ABS465465",
    creditor: "CAGECE",
    type_creditor: "PUBLICO",
    historic: "não existe historico",
    id_user_manager: "",
    operated_at: "",
    created_at: "01/12/2016 09:44:36",
    updated_at: "05/12/2016 16:54:27",
    observation: ""
}
```

##### Erro:
(http status 500)
```
{
  "erro": "ocorreu um erro ao tentar salvar os dados"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.

### Listar

> Requisita a lista de pagamentos cadastrados.

* url: http://*url_do_projeto*/api/v1/payments
* HTTP Method: GET

#### Resposta :

##### Sucesso :
(http status 200)
```
[{
    id: 21,
    date: "05/12/2016",
    code: "7894123",
    type_payment: "AGUA",
    id_administrative_unit: 3,
    type_expense: "MENSAL",
    invoice: "BR 23456-9",
    element: "AGUA E ESGOTO",
    source_resource: "CONTABILIDADE",
    gross_amount: 10000,
    discount: 25.5,
    net_amount: 74.5,
    bidding: "1345678976456-198-adv",
    contract: "798789-ABS465465",
    creditor: "CAGECE",
    type_creditor: "PUBLICO",
    historic: "não existe historico",
    id_user_manager: 8,
    operated_at: "05/12/2016 16:54:27",
    created_at: "01/12/2016 09:44:36",
    updated_at: "05/12/2016 16:54:27",
    status: "waiting",
    observation: "",
    administrative_unit: {
        id: 3,
        id_instituition: 1,
        description: "Desenvolvimento",
        active: true,
        created_at: "30/11/2016 11:36:27",
        updated_at: "30/11/2016 11:36:27"
    },
    user_manager: {
        id: 8,
        name: "Darlan",
        id_type: "1",
        id_instituition: "1",
        email: "darlan@tudomunicipal.com.br",
        active: true,
        created_at: "19/12/2016 17:33:57",
        updated_at: "19/12/2016 17:33:57"
    }
}, {
    id: 22,
    date: "05/12/2016",
    code: "7894123",
    type_payment: "AGUA",
    id_administrative_unit: 3,
    type_expense: "MENSAL",
    invoice: "BR 23456-9",
    element: "AGUA E ESGOTO",
    source_resource: "CONTABILIDADE",
    gross_amount: 100,
    discount: 25.5,
    net_amount: 74.5,
    bidding: "1345678976456-198-adv",
    contract: "798789-ABS465465",
    creditor: "CAGECE",
    type_creditor: "PRIVADO",
    historic: "não existe historico",
    id_user_manager: "",
    operated_at: "",
    created_at: "02/12/2016 15:05:45",
    updated_at: "06/12/2016 09:17:47",
    status: "confirmed",
    observation: "",
    administrative_unit: {
        id: 3,
        id_instituition: 1,
        description: "Desenvolvimento",
        active: true,
        created_at: "30/11/2016 11:36:27",
        updated_at: "30/11/2016 11:36:27"
    }    
}]
```

##### Erro:
(http status 204)
```
{
  "erro": "não existe dados para a requisição"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.

### Detalhar

> Detalhar um pagamento cadastrado.

> Obs: Enviar token de usuario no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/payments/*id_pagamento*
* HTTP Method: GET

#### Resposta :

##### Sucesso :
(http status 200)
```
{
    id: 21,
    date: "05/12/2016",
    code: "7894123",
    type_payment: "AGUA",
    id_administrative_unit: 3,
    type_expense: "MENSAL",
    invoice: "BR 23456-9",
    element: "AGUA E ESGOTO",
    source_resource: "CONTABILIDADE",
    gross_amount: 10000,
    discount: 25.5,
    net_amount: 74.5,
    bidding: "1345678976456-198-adv",
    contract: "798789-ABS465465",
    creditor: "CAGECE",
    type_creditor: "PUBLICO",
    historic: "não existe historico",
    id_user_manager: 8,
    operated_at: "05/12/2016 16:54:27",
    created_at: "01/12/2016 09:44:36",
    updated_at: "05/12/2016 16:54:27",
    status: "waiting",
    observation: "",
    administrative_unit: {
        id: 3,
        id_instituition: 1,
        description: "Desenvolvimento",
        active: true,
        created_at: "30/11/2016 11:36:27",
        updated_at: "30/11/2016 11:36:27"
    },
    user_manager: {
        id: 8,
        name: "Darlan",
        id_type: "1",
        id_instituition: "1",
        email: "darlan@tudomunicipal.com.br",
        active: true,
        created_at: "19/12/2016 17:33:57",
        updated_at: "19/12/2016 17:33:57"
    }
}
```

##### Erro:
(http status 404)
```
{
  "erro": "não existe dados para a requisição"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.

### Alterar

> Altera um pagamento.

> Obs: Enviar token de usuario no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/payments/*id_pagamento*
* HTTP Method: PUT

#### Requisição :
```
{
    date: "05/12/2016",
    code: "7894123",
    type_payment: "AGUA",
    id_administrative_unit: 3,
    type_expense: "MENSAL",
    invoice: "BR 23456-9",
    element: "AGUA E ESGOTO",
    source_resource: "CONTABILIDADE",
    gross_amount: 10000,
    discount: 25.5,
    net_amount: 74.5,
    bidding: "1345678976456-198-adv",
    contract: "798789-ABS465465",
    creditor: "CAGECE",
    type_creditor: "PUBLICO",
    historic: "não existe historico",
    id_user_manager: "",
    operated_at: "",
    created_at: "01/12/2016 09:44:36",
    updated_at: "05/12/2016 16:54:27",
    observation: ""
}
```

#### Resposta :

##### Sucesso :
(http status 204)
```
{
    id: 21,
    date: "05/12/2016",
    code: "7894123",
    type_payment: "AGUA",
    id_administrative_unit: 3,
    type_expense: "MENSAL",
    invoice: "BR 23456-9",
    element: "AGUA E ESGOTO",
    source_resource: "CONTABILIDADE",
    gross_amount: 10000,
    discount: 25.5,
    net_amount: 74.5,
    bidding: "1345678976456-198-adv",
    contract: "798789-ABS465465",
    creditor: "CAGECE",
    type_creditor: "PUBLICO",
    historic: "não existe historico",
    id_user_manager: "",
    operated_at: "",
    created_at: "01/12/2016 09:44:36",
    updated_at: "05/12/2016 16:54:27",
    observation: ""
}
```

##### Erro:
(http status 304)
```
{
  "erro": "ocorreu um erro ao tentar salvar os dados"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.

### Filtrar

> Filtra a lista de pagamentos cadastrados de acordo com os paramentros fornecidos.

> Obs: Enviar token de usuario no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/payments/filter
* HTTP Method: POST

#### Requisição :
```
{
    date: [
        "initial" : 05/12/2016"
        "final" : 05/12/2016"
        "exactly" : 05/12/2016"
    ],
    type_payment: "AGUA",
    id_administrative_unit: 3",
    status: "waiting | confirmed | canceled",
}
```
#### Resposta :

##### Sucesso :
(http status 200)
```
[{
    id: 21,
    date: "05/12/2016",
    code: "7894123",
    type_payment: "AGUA",
    id_administrative_unit: 3,
    type_expense: "MENSAL",
    invoice: "BR 23456-9",
    element: "AGUA E ESGOTO",
    source_resource: "CONTABILIDADE",
    gross_amount: 10000,
    discount: 25.5,
    net_amount: 74.5,
    bidding: "1345678976456-198-adv",
    contract: "798789-ABS465465",
    creditor: "CAGECE",
    type_creditor: "PUBLICO",
    historic: "não existe historico",
    id_user_manager: 8,
    operated_at: "05/12/2016 16:54:27",
    created_at: "01/12/2016 09:44:36",
    updated_at: "05/12/2016 16:54:27",
    status: "waiting",
    observation: "",
    administrative_unit: {
        id: 3,
        id_instituition: 1,
        description: "Desenvolvimento",
        active: true,
        created_at: "30/11/2016 11:36:27",
        updated_at: "30/11/2016 11:36:27"
    },
    user_manager: {
        id: 8,
        name: "Darlan",
        id_type: "1",
        id_instituition: "1",
        email: "darlan@tudomunicipal.com.br",
        active: true,
        created_at: "19/12/2016 17:33:57",
        updated_at: "19/12/2016 17:33:57"
    }
}, {
    id: 22,
    date: "05/12/2016",
    code: "7894123",
    type_payment: "AGUA",
    id_administrative_unit: 3,
    type_expense: "MENSAL",
    invoice: "BR 23456-9",
    element: "AGUA E ESGOTO",
    source_resource: "CONTABILIDADE",
    gross_amount: 100,
    discount: 25.5,
    net_amount: 74.5,
    bidding: "1345678976456-198-adv",
    contract: "798789-ABS465465",
    creditor: "CAGECE",
    type_creditor: "PRIVADO",
    historic: "não existe historico",
    id_user_manager: "",
    operated_at: "",
    created_at: "02/12/2016 15:05:45",
    updated_at: "06/12/2016 09:17:47",
    status: "confirmed",
    observation: "",
    administrative_unit: {
        id: 3,
        id_instituition: 1,
        description: "Desenvolvimento",
        active: true,
        created_at: "30/11/2016 11:36:27",
        updated_at: "30/11/2016 11:36:27"
    }    
}]
```

##### Erro:
(http status 204)
```
{
  "erro": "não existe dados para a requisição"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.

### Aprovar

> Aprova o(s) pagamento(s) selecionados de acordo com os paramentros fornecidos.

> Obs: Enviar token de usuario no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/payments/approve
* HTTP Method: POST

#### Requisição :
```
{
    id: [ 1, 2, 3]
}
```
#### Resposta :

##### Sucesso :
(http status 200)
```
[{
    id: 1,
    date: "05/12/2016",
    code: "7894123",
    type_payment: "AGUA",
    id_administrative_unit: 3,
    type_expense: "MENSAL",
    invoice: "BR 23456-9",
    element: "AGUA E ESGOTO",
    source_resource: "CONTABILIDADE",
    gross_amount: 10000,
    discount: 25.5,
    net_amount: 74.5,
    bidding: "1345678976456-198-adv",
    contract: "798789-ABS465465",
    creditor: "CAGECE",
    type_creditor: "PUBLICO",
    historic: "não existe historico",
    id_user_manager: 8,
    operated_at: "05/12/2016 16:54:27",
    created_at: "01/12/2016 09:44:36",
    updated_at: "05/12/2016 16:54:27",
    status: "waiting",
    observation: "",
    administrative_unit: {
        id: 3,
        id_instituition: 1,
        description: "Desenvolvimento",
        active: true,
        created_at: "30/11/2016 11:36:27",
        updated_at: "30/11/2016 11:36:27"
    },
    user_manager: {
        id: 8,
        name: "Darlan",
        id_type: "1",
        id_instituition: "1",
        email: "darlan@tudomunicipal.com.br",
        active: true,
        created_at: "19/12/2016 17:33:57",
        updated_at: "19/12/2016 17:33:57"
    }
}, {
    id: 22,
    date: "05/12/2016",
    code: "7894123",
    type_payment: "AGUA",
    id_administrative_unit: 3,
    type_expense: "MENSAL",
    invoice: "BR 23456-9",
    element: "AGUA E ESGOTO",
    source_resource: "CONTABILIDADE",
    gross_amount: 100,
    discount: 25.5,
    net_amount: 74.5,
    bidding: "1345678976456-198-adv",
    contract: "798789-ABS465465",
    creditor: "CAGECE",
    type_creditor: "PRIVADO",
    historic: "não existe historico",
    id_user_manager: "",
    operated_at: "",
    created_at: "02/12/2016 15:05:45",
    updated_at: "06/12/2016 09:17:47",
    status: "confirmed",
    observation: "",
    administrative_unit: {
        id: 3,
        id_instituition: 1,
        description: "Desenvolvimento",
        active: true,
        created_at: "30/11/2016 11:36:27",
        updated_at: "30/11/2016 11:36:27"
    }    
}]
```

##### Erro:
(http status 204)
```
{
  "erro": "não existe dados para a requisição"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.

### Cancelar

> Cancela o(s) pagamento(s) selecionados de acordo com os paramentros fornecidos.

> Obs: Enviar token de usuario no cabeçalho da Requisição
```
{
  "token": "gwsdegasdghsadhasfhsdfhsd"
}
```

* url: http://*url_do_projeto*/api/v1/payments/deprive
* HTTP Method: POST

#### Requisição :
```
{
    id: [ 1, 2, 3]
}
```
#### Resposta :

##### Sucesso :
(http status 200)
```
[{
    id: 1,
    date: "05/12/2016",
    code: "7894123",
    type_payment: "AGUA",
    id_administrative_unit: 3,
    type_expense: "MENSAL",
    invoice: "BR 23456-9",
    element: "AGUA E ESGOTO",
    source_resource: "CONTABILIDADE",
    gross_amount: 10000,
    discount: 25.5,
    net_amount: 74.5,
    bidding: "1345678976456-198-adv",
    contract: "798789-ABS465465",
    creditor: "CAGECE",
    type_creditor: "PUBLICO",
    historic: "não existe historico",
    id_user_manager: 8,
    operated_at: "05/12/2016 16:54:27",
    created_at: "01/12/2016 09:44:36",
    updated_at: "05/12/2016 16:54:27",
    status: "waiting",
    observation: "",
    administrative_unit: {
        id: 3,
        id_instituition: 1,
        description: "Desenvolvimento",
        active: true,
        created_at: "30/11/2016 11:36:27",
        updated_at: "30/11/2016 11:36:27"
    },
    user_manager: {
        id: 8,
        name: "Darlan",
        id_type: "1",
        id_instituition: "1",
        email: "darlan@tudomunicipal.com.br",
        active: true,
        created_at: "19/12/2016 17:33:57",
        updated_at: "19/12/2016 17:33:57"
    }
}, {
    id: 22,
    date: "05/12/2016",
    code: "7894123",
    type_payment: "AGUA",
    id_administrative_unit: 3,
    type_expense: "MENSAL",
    invoice: "BR 23456-9",
    element: "AGUA E ESGOTO",
    source_resource: "CONTABILIDADE",
    gross_amount: 100,
    discount: 25.5,
    net_amount: 74.5,
    bidding: "1345678976456-198-adv",
    contract: "798789-ABS465465",
    creditor: "CAGECE",
    type_creditor: "PRIVADO",
    historic: "não existe historico",
    id_user_manager: "",
    operated_at: "",
    created_at: "02/12/2016 15:05:45",
    updated_at: "06/12/2016 09:17:47",
    status: "confirmed",
    observation: "",
    administrative_unit: {
        id: 3,
        id_instituition: 1,
        description: "Desenvolvimento",
        active: true,
        created_at: "30/11/2016 11:36:27",
        updated_at: "30/11/2016 11:36:27"
    }    
}]
```

##### Erro:
(http status 204)
```
{
  "erro": "não existe dados para a requisição"
}
```

(http status 401)
```
{
  "erro": "acesso não autorizado"
}
```
OBS: Esse erro é exibido no caso de falha nas credenciais de acesso enviadas no cabeçalho.
