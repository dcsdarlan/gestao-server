@extends('template')
@section("tittle", "Detalhar Despesas")
@section('content')
    <div id="main">
        <div class="container-fluid">
            <div class="page-header">
                <div class="pull-left">
                    <h1>@yield("tittle")</h1>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="breadcrumbs">
                        <ul>
                            <li>
                                <a href="{{route("typesexpenses.index")}}">Listar Despesas</a>
                                <i class="icon-angle-right"></i>
                            </li>
                            <li>
                                <a>@yield("tittle")</a>
                            </li>
                        </ul>
                        <div class="close-bread">
                            <a><i class="icon-remove"></i></a>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-title">
                            <h3>
                                <i class="icon-signout"></i>
                                Despesa
                            </h3>
                        </div>
                        <div class="box-content">
                            {!! Form::open(array('class' => 'form-horizontal')) !!}
                            @if(Session::has('erro'))
                                <div class="alert alert-danger alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('erro')}}
                                </div>
                            @endif
                            @if(Session::has('sucesso'))
                                <div class="alert alert-success alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('sucesso')}}
                                </div>
                            @endif
                            <div class="control-group">
                                {!!  Form::label('description', 'Descrição', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('description', $expense->description, ["readonly"=>true, "name"=>'description', "placeholder"=>"Descrição", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('insitituition', 'Instituição', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('insitituition', $expense->type->administrative_unit->instituition->description, ["readonly"=>true, "name"=>'insitituition', "placeholder"=>"Instituição", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('unit_expense', 'Unidade Gestora', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('unit_expense', $expense->type->administrative_unit->description, ["readonly"=>true, "name"=>'unit_expense', "placeholder"=>"Tipo de Despesa", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('type_expense', 'Tipo de Despesa', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('type_expense', $expense->type->description, ["readonly"=>true, "name"=>'type_expense', "placeholder"=>"Tipo de Despesa", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('value', 'Valor', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                  <div class="input-append">
                                    {!! Form::text('value', str_replace("R$ ", "", $expense->value), ["readonly"=>true, "name"=>'value', "placeholder"=>"Valor", "class"=>'input-block-level value', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                    <span class="add-on">R$</span>
          												</div>
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('date', 'Data', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                  {!! Form::text('date', $expense->date, ["readonly"=>true, "name"=>'date', "placeholder"=>"Data", "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="breadcrumbs">
                                <div class="form-actions">
                                    <div class="submit">
                                        <span class="pull-right">
                                            <a href="{{route("expenses.index")}}" class='btn btn-default'>Cancelar</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
