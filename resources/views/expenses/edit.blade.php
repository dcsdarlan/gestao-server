@extends('template')
@section("tittle", "Alterar Depesas")
@section('content')
    <div id="main">
        <div class="container-fluid">
            <div class="page-header">
                <div class="pull-left">
                    <h1>@yield("tittle")</h1>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="breadcrumbs">
                        <ul>
                            <li>
                                <a href="{{route("expenses.index")}}">Listar Depesas</a>
                                <i class="icon-angle-right"></i>
                            </li>
                            <li>
                                <a>@yield("tittle")</a>
                            </li>
                        </ul>
                        <div class="close-bread">
                            <a><i class="icon-remove"></i></a>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-title">
                            <h3>
                                <i class="icon-signout"></i>
                                Depesa
                            </h3>
                        </div>
                        <div class="box-content">
                            {!! Form::open(array('route' => array('expenses.update', $expense->id), 'method' => 'put', 'class' => 'form-horizontal')) !!}
                            @if(Session::has('erro'))
                                <div class="alert alert-danger alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('erro')}}
                                </div>
                            @endif
                            @if(Session::has('sucesso'))
                                <div class="alert alert-success alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('sucesso')}}
                                </div>
                            @endif
                            <div class="control-group">
                                {!!  Form::label('description', 'Descrição', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('description', old("description", $expense->description), ["name"=>'description', "placeholder"=>"Descrição", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>

                            <div class="control-group">
                                {!!  Form::label('instituition', 'Instituição', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::select('instituition', $instituitions, old("instituition", $expense->type->administrative_unit->id_instituition), ["name"=>'instituition', "data-rule-required"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('unit', 'Unidade Gestora', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::select('unit', $units, old("unit", $expense->type->id_administrative_unit), ["name"=>'unit', "data-placeholder" => "-- Selecione uma Instituição --", "data-rule-required"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('type', 'Tipo de Despesa', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::select('type', $types, Session::has('type') ? Session::get('type') : $expense->id_type, ["name"=>'type', "data-placeholder" => "-- Selecione uma Instituição --",  "class"=>'input-block-level', "data-rule-required"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('value', 'Valor', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                  <div class="input-append">
                                    {!! Form::text('value', old("value", str_replace("R$ ", "", $expense->value)), ["name"=>'value', "placeholder"=>"Valor", "class"=>'input-block-level value', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                    <span class="add-on">R$</span>
                                  </div>
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('date', 'Data', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                  {!! Form::text('date', old("date", $expense->date), ["name"=>'date', "placeholder"=>"Data", "class"=>'datepick', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="breadcrumbs">
                                <div class="form-actions">
                                    <div class="submit">
                                    <span class="pull-right">
                                        <a href="{{route("expenses.index")}}" class='btn btn-default'>Cancelar</a>
                                        {!! Form::submit('Salvar', ["class"=>'btn btn-primary']) !!}
                                    </span>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
