
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <title>{{config('app.name')}} - Entrar</title>
    {{ Html::style('flat/html/css/bootstrap.min.css') }}
    {{ Html::style('flat/html/css/bootstrap-responsive.min.css') }}
    {{ Html::style('flat/html/css/plugins/icheck/all.css') }}
    {{ Html::style('flat/html/css/style.css') }}
    {{ Html::style('flat/html/css/themes.css') }}
    {{ Html::style('css/styles.min.css') }}
    <link rel="shortcut icon" href="img/favicon.ico" />
    <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-precomposed.png" />
</head>

<body class='login'>
    <div class="wrapper">

        <div class="login-body">
            <h1><a>{{ Html::image('img/' . config('app.logo'), 'Adm', array('class' => 'image-login')) }}</a></h1>
            <h2 class="tittle-login">{{config('app.name')}}</h2>
            {!! Form::open(array('route' => 'auth.logon', 'method' => 'post')) !!}
            @if(Session::has('erro'))
                <div class="alert alert-danger alert-dismissible animate1 fadeIn">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{Session::get('erro')}}
                </div>
            @endif
            @if(Session::has('sucesso'))
                <div class="alert alert-success alert-dismissible animate1 fadeIn">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{Session::get('sucesso')}}
                </div>
            @endif
                <div class="control-group">
                    <div class="email controls">
                        {!! Form::text('email', null, ["name"=>'email', "placeholder"=>"Email", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                    </div>
                </div>
                <div class="control-group">
                    <div class="pw controls">
                        {!! Form::password('password', ["name"=>'password', "placeholder"=>"Senha", "class"=>'input-block-level', "data-rule-required"=>"true"]) !!}
                    </div>
                </div>
                <div class="submit">
                    <div class="remember">
                        {!! Form::checkbox('remember', 'remember', true, ["class"=>'icheck-me', "data-skin"=>"square", "data-color"=>"blue", "id"=>"remember"]) !!}
                        {!!  Form::label('remember', 'Lembrar-me') !!}
                    </div>
                    {!! Form::submit('Entrar', ["class"=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
            <div class="forget">
                <a><span>Esqueceu a Senha?</span></a>
            </div>
        </div>
    </div>
</body>
{{ Html::script('flat/html/js/jquery.min.js') }}
    {{ Html::script('flat/html/js/plugins/nicescroll/jquery.nicescroll.min.js') }}
    {{ Html::script('flat/html/js/plugins/validation/jquery.validate.min.js') }}
    {{ Html::script('flat/html/js/plugins/validation/additional-methods.min.js') }}
    {{ Html::script('flat/html/js/plugins/icheck/jquery.icheck.min.js') }}
    {{ Html::script('flat/html/js/bootstrap.min.js') }}
    {{ Html::script('flat/html/js/eakroko.js') }}

    <!--[if lte IE 9]>
{{ Html::script('flat/html/js/plugins/placeholder/jquery.placeholder.min.js') }}
<script>
    $(document).ready(function() {
        $('input, textarea').placeholder();
    });
</script>
<![endif]-->
</html>
