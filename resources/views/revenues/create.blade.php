@extends('template')
@section("tittle", "Adicionar Receitas")
@section('content')
<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left">
                <h1>@yield("tittle")</h1>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="breadcrumbs">
                    <ul>
                        <li>
                            <a href="{{route("revenues.index")}}">Listar Receitas</a>
                            <i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a>@yield("tittle")</a>
                        </li>
                    </ul>
                    <div class="close-bread">
                        <a><i class="icon-remove"></i></a>
                    </div>
                </div>
                <div class="box">
                    <div class="box-title">
                        <h3>
                            <i class="icon-signin"></i>
                            Receita
                        </h3>
                    </div>
                    <div class="box-content">
                        {!! Form::open(array('route' => 'revenues.store', 'method' => 'post', 'class' => 'form-horizontal')) !!}
                        @if(Session::has('erro'))
                            <div class="alert alert-danger alert-dismissible animate1 fadeIn">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{Session::get('erro')}}
                            </div>
                        @endif
                        @if(Session::has('sucesso'))
                            <div class="alert alert-success alert-dismissible animate1 fadeIn">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{Session::get('sucesso')}}
                            </div>
                        @endif
                        <div class="control-group">
                            {!!  Form::label('description', 'Descrição', ["class"=>"control-label"]) !!}
                            <div class="controls">
                                {!! Form::text('description', old("description", null), ["name"=>'description', "placeholder"=>"Descrição", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                            </div>
                        </div>
                        <div class="control-group">
                            {!!  Form::label('instituition', 'Instituição', ["class"=>"control-label"]) !!}
                            <div class="controls">
                                {!! Form::select('instituition', $instituitions, old("instituition", null), ["name"=>'instituition', "data-rule-required"=>"true"]) !!}
                            </div>
                        </div>
                        <div class="control-group">
                            {!!  Form::label('unit', 'Unidade Gestora', ["class"=>"control-label"]) !!}
                            <div class="controls">
                                {!! Form::select('unit', $units, old("unit", null), ["name"=>'unit', "data-placeholder" => "-- Selecione uma Instituição --",  "data-rule-required"=>"true"]) !!}
                            </div>
                        </div>
                        <div class="control-group">
                            {!!  Form::label('value', 'Valor', ["class"=>"control-label"]) !!}
                            <div class="controls">
                              <div class="input-append">
                                {!! Form::text('value', old("value", null), ["name"=>'value', "placeholder"=>"Valor", "class"=>'input-block-level value', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                <span class="add-on">R$</span>
                              </div>
                            </div>
                        </div>
                        <div class="control-group">
                            {!!  Form::label('date', 'Data', ["class"=>"control-label"]) !!}
                            <div class="controls">
                              {!! Form::text('date', old("date", null), ["name"=>'date', "placeholder"=>"Data", "class"=>'datepick', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                            </div>
                        </div>
                        <div class="breadcrumbs">
                            <div class="form-actions">
                                <div class="submit">
                                    <span class="pull-right">
                                        <a href="{{route("revenues.index")}}" class='btn btn-defaut'>Cancelar</a>
                                        {!! Form::submit('Salvar', ["class"=>'btn btn-primary']) !!}
                                    </span>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
