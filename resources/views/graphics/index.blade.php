@extends('template')
@section("tittle", "Graficos")
@section('content')
    <div id="main">
        <div class="container-fluid">
          <div class="box">
              <div class="box-title">
                  <h3>
                      <i class="icon-filter"></i>
                      Filtro
                  </h3>
              </div>
              <div class="box-content">
                  {!! Form::open(array("onsubmit" => "return false", 'class' => 'form-horizontal')) !!}
                  <div class="control-group">
                      {!!  Form::label('instituition', 'Instituição', ["class"=>"control-label"]) !!}
                      <div class="controls">
                          {!! Form::select('instituition', $instituitions, null, ["name"=>'instituition', "data-rule-required"=>"true", "class"=>'input-block-level']) !!}
                      </div>
                  </div>
                  <div class="control-group">
                      {!!  Form::label('unit', 'Unidade Gestora', ["class"=>"control-label"]) !!}
                      <div class="controls">
                          {!! Form::select('unit', $units, null, ["name"=>'unit', "data-placeholder" => "-- Selecione uma Instituição --",  "data-rule-required"=>"true", "class"=>'input-block-level']) !!}
                      </div>
                  </div>
                  <div class="control-group">
                      {!!  Form::label('type', 'Tipo de Despesa', ["class"=>"control-label"]) !!}
                      <div class="controls">
                          {!! Form::select('type', $types, null, ["name"=>'type', "data-placeholder" => "-- Selecione uma Unidade administrativa --", "data-rule-required"=>"true", "class"=>'input-block-level']) !!}
                      </div>
                  </div>
                  <div class="control-group">
                      <label class="control-label">Mês</label>
                      <div class="controls">
                        <select class="input-block-level" id="month" name="month">
                          <option>-- Selecione o Mês</option>
                          <option value="01">JAN</option>
                          <option value="02">FEV</option>
                          <option value="03">MAR</option>
                          <option value="04">ABR</option>
                          <option value="05">MAI</option>
                          <option value="06">JUN</option>
                          <option value="07">JUL</option>
                          <option value="08">AGO</option>
                          <option value="09">SET</option>
                          <option value="10">OUT</option>
                          <option value="11">NOV</option>
                          <option value="12">DEZ</option>
                        </select>
                      </div>
                  </div>
                  <div class="control-group">
                      <label class="control-label">Ano</label>
                      <div class="controls">
                        <select class="input-block-level" id="year" name="year">
                          <option>-- Selecione o Ano</option>
                          <option value="2017">2017</option>
                          <option value="2016">2016</option>
                          <option value="2015">2015</option>
                        </select>
                      </div>
                  </div>
                  <div class="breadcrumbs">
                      <div class="form-actions" style="margin-bottom: 0px; !important">
                          <div class="submit">
                              <span class="pull-right">
                                  {!! Form::submit('Gerar Grafico', ["id"=>"gerar_grafico","class"=>'btn btn-primary']) !!}
                              </span>
                          </div>
                      </div>
                  </div>
                  {!! Form::close() !!}
              </div>
          </div>

          <div class="modal fade" id="grafic" tabindex="-1" role="dialog_grafic" aria-labelledby="delete" aria-hidden="true">
              <div class="modal-dialog" role="document">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">
                                  &times;
                              </span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel">
                              Gráfico
                          </h4>
                      </div>
                      <div class="modal-body">
                        <div class="alert alert-danger alert-dismissible animate1 fadeIn" id="alert-erro-graphic">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <span id="erro-graphic"></span>
                        </div>
                        <div class="graphic-container">
                            <canvas id="c-graphic" width="300" height="400"></canvas>
                        </div>
                        <div id="graphic-limit">
                          <div><b>Limite Prudencial: </b><span id="limit-prudential"></span></div>
                          <div><b>Limite de Alerta: </b><span id="limit-alert"></span></div>
                          <div><b>Limte Máximo: </b><span id="limit-max"></span></div>
                        </div>
                          <div id="graphic-limit">
                              <button type="button" id="ateDataBt" class="btn pull-right">
                                  Até a Data
                              </button>
                              <button type="button" id="dataBt" class="btn pull-right">
                                  Data
                              </button>
                          </div>
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">
                              Fechar
                          </button>
                      </div>
                  </div>
              </div>
          </div>

        </div>
    </div>
@stop
