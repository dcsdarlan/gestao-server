@extends('template')
@section("tittle", "Adicionar Usuários")
@section('content')
    <div id="main">
        <div class="container-fluid">
            <div class="page-header">
                <div class="pull-left">
                    <h1>@yield("tittle")</h1>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="breadcrumbs">
                        <ul>
                            <li>
                                <a href="{{route("users.index")}}">Listar Usuários</a>
                                <i class="icon-angle-right"></i>
                            </li>
                            <li>
                                <a>@yield("tittle")</a>
                            </li>
                        </ul>
                        <div class="close-bread">
                            <a><i class="icon-remove"></i></a>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-title">
                            <h3>
                                <i class="icon-user"></i>
                                Usuário
                            </h3>
                        </div>
                        <div class="box-content">
                            {!! Form::open(array('route' => 'users.store', 'method' => 'post', 'class' => 'form-horizontal')) !!}
                            @if(Session::has('erro'))
                                <div class="alert alert-danger alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('erro')}}
                                </div>
                            @endif
                            @if(Session::has('sucesso'))
                                <div class="alert alert-success alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('sucesso')}}
                                </div>
                            @endif
                            <div class="control-group">
                                {!!  Form::label('name', 'Nome', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('name', old("name", null), ["name"=>'name', "placeholder"=>"Nome", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('email', 'Email', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('email', old("email", null), ["name"=>'email', "placeholder"=>"Email", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('password', 'Senha', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::password('password', ["name"=>'password', "placeholder"=>"Senha", "class"=>'input-block-level', "data-rule-required"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('conf_password', 'Confirmação de Senha', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::password('conf_password', ["name"=>'conf_password', "placeholder"=>"Confirmação de Senha", "class"=>'input-block-level', "data-rule-required"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('type_user', 'Tipo de Usuário', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::select('type_user', $types, old("type", null), ["name"=>'type', "data-rule-required"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('instituition', 'Instituição', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::select('instituition', $instituitions, old("instituition", null), ["name"=>'instituition', "data-rule-required"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('active', 'Ativo', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::select('active', ["ativo" => 'Ativo', "inativo" => 'Inativo'], old("active", null), ["name"=>'active', "data-rule-required"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="breadcrumbs">
                                <div class="form-actions">
                                    <div class="submit">
                                        <span class="pull-right">
                                            <a href="{{route("users.index")}}" class='btn btn-default'>Cancelar</a>
                                            {!! Form::submit('Salvar', ["class"=>'btn btn-primary']) !!}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
