@extends('template')
@section("tittle", "Detalhar Usuários")
@section('content')
    <div id="main">
        <div class="container-fluid">
            <div class="page-header">
                <div class="pull-left">
                    <h1>@yield("tittle")</h1>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="breadcrumbs">
                        <ul>
                            <li>
                                <a href="{{route("users.index")}}">Listar Usuários</a>
                                <i class="icon-angle-right"></i>
                            </li>
                            <li>
                                <a>@yield("tittle")</a>
                            </li>
                        </ul>
                        <div class="close-bread">
                            <a><i class="icon-remove"></i></a>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-title">
                            <h3>
                                <i class="icon-user"></i>
                                Usuário
                            </h3>
                        </div>
                        <div class="box-content">
                            {!! Form::open(array('class' => 'form-horizontal')) !!}
                            @if(Session::has('erro'))
                                <div class="alert alert-danger alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('erro')}}
                                </div>
                            @endif
                            @if(Session::has('sucesso'))
                                <div class="alert alert-success alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('sucesso')}}
                                </div>
                            @endif
                            <div class="control-group">
                                {!!  Form::label('name', 'Nome', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('name', $user->name, ["readonly"=>true, "name"=>'name', "placeholder"=>"Nome", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('email', 'Email', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('email', $user->email, ["readonly"=>true, "name"=>'email', "placeholder"=>"Email", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('type', 'Tipo de Usuário', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('type', $user->type->description, ["readonly"=>true, "name"=>'type', "placeholder"=>"Tipo", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('instituition', 'Instituição', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('instituition', $user->instituition->description, ["readonly"=>true, "name"=>'instituition', "placeholder"=>"Instituição", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('status', 'Status', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('status', $user->active ? "Ativo": "Inativo", ["readonly"=>true, "name"=>'status', "placeholder"=>"Status", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="breadcrumbs">
                                <div class="form-actions">
                                    <div class="submit">
                                        <span class="pull-right">
                                            <a href="{{route("users.index")}}" class='btn btn-default'>Cancelar</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-title">
                            <h3>
                                <i class="icon-th"></i>
                                Unidades Gestoras
                            </h3>
                            <button data-toggle="modal" data-target="#add" class="pull-right btn btn-blue"><i class="icon-plus"></i>Adicionar ao Usuário</button>
                            <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="add" aria-hidden="true">
                                {!! Form::open(array('route' => 'users.administrativesunits.add', 'method' => 'post', 'class' => 'form-horizontal')) !!}
                                {{ Form::hidden("user", $user->id) }}
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">
                                                                &times;
                                                            </span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">
                                                Adicionar Unidade Gestora ao Usuário
                                            </h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="control-group">
                                                {!!  Form::label('unit_select', 'Unidade Gestora', ["class"=>"control-label"]) !!}
                                                <div class="controls">
                                                    {!! Form::select('unit_select', $units, null, ["name"=>'unit', "class"=>'input-block-level', "data-rule-required"=>"true"]) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                Fechar
                                            </button>
                                            <button type="submit" class="btn btn-primary">
                                                Adicionar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <div class="box-content">
                            <table class="table dataTable">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Instituição</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($user->administratives_units as $unit)
                                    <tr>
                                        <td>{{$unit->description}}</td>
                                        <td>{{$unit->instituition->description}}</td>
                                        <td>@if($unit->active)<span class="status alert alert-success">Ativo</span>@else<span class="status alert alert-danger">Inativo</span>@endif</td>
                                        <td>
                                            <span class="pull-right">
                                                <butom class="btn btn-red" data-toggle="modal" data-target="#delete_{{$unit->id}}"><i class="icon-trash"></i>Remover do Usuário</butom>
                                                <div class="modal fade" id="delete_{{$unit->id}}" tabindex="-1" role="dialog_{{$unit->id}}" aria-labelledby="delete" aria-hidden="true">
                                                    {!! Form::open(array('route' => 'users.administrativesunits.rm', 'method' => 'post', 'class' => 'form-horizontal')) !!}
                                                    {{ Form::hidden("user", $user->id) }}
                                                    {{ Form::hidden("unit", $unit->id) }}
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">
                                                                        &times;
                                                                    </span>
                                                                </button>
                                                                <h4 class="modal-title" id="myModalLabel">
                                                                    Remover Unidade Gestora do Usuário
                                                                </h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                Deseja realmente remover esse registro?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                                    Fechar
                                                                </button>
                                                                <button type="submit" class="btn btn-red">
                                                                    Remover
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
