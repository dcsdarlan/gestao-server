@extends('template')
@section("tittle", "Alterar Tipos de Despesas")
@section('content')
    <div id="main">
        <div class="container-fluid">
            <div class="page-header">
                <div class="pull-left">
                    <h1>@yield("tittle")</h1>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="breadcrumbs">
                        <ul>
                            <li>
                                <a href="{{route("typesexpenses.index")}}">Listar Tipos de Despesas</a>
                                <i class="icon-angle-right"></i>
                            </li>
                            <li>
                                <a>@yield("tittle")</a>
                            </li>
                        </ul>
                        <div class="close-bread">
                            <a><i class="icon-remove"></i></a>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-title">
                            <h3>
                                <i class="icon-tags"></i>
                                Tipo de Despesa
                            </h3>
                        </div>
                        <div class="box-content">
                            {!! Form::open(array('route' => array('typesexpenses.update', $type->id), 'method' => 'put', 'class' => 'form-horizontal')) !!}
                            @if(Session::has('erro'))
                                <div class="alert alert-danger alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('erro')}}
                                </div>
                            @endif
                            @if(Session::has('sucesso'))
                                <div class="alert alert-success alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('sucesso')}}
                                </div>
                            @endif
                            <div class="control-group">
                                {!!  Form::label('description', 'Descrição', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('description', old("description", $type->description), ["name"=>'description', "placeholder"=>"Descrição", "data-rule-required"=>"true", "class"=>'input-block-level', "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('instituition', 'Instituição', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::select('instituition', $instituitions, old("instituition", $type->administrative_unit->id_instituition), ["name"=>'instituition', "data-rule-required"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('unit', 'Unidade Gestora', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::select('unit', $units, old("unit", $type->id_administrative_unit), ["name"=>'unit', "data-placeholder" => "-- Selecione uma Instituição --", "data-rule-required"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('code', 'Código', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('code', old("code", $type->code), ["name"=>'code', "placeholder"=>"Código", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('maximum_limit', 'Limite Máximo (%)', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                  <div class="input-append">
                                    {!! Form::number('maximum_limit', old("maximum_limit", ($type->maximum_limit ? $type->maximum_limit : null)), ["max"=>"100", "name"=>'maximum_limit', "placeholder"=>"Limite Máximo", "data-rule-required"=>"true"]) !!}
                                    <span class="add-on">%</span>
                                  </div>
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('prudential_limit', 'Limite Prudencial (%)', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                  <div class="input-append">
                                    {!! Form::number('prudential_limit',old("prudential_limit", ($type->prudential_limit ? $type->prudential_limit : null)), ["max"=>"100", "name"=>'prudential_limit', "placeholder"=>"Limite Prudencial", "data-rule-required"=>"true"]) !!}
                                    <span class="add-on">%</span>
                                  </div>
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('alert_limit', 'Limite de Alerta (%)', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                  <div class="input-append">
                                    {!! Form::number('alert_limit', old("prudential_limit", ($type->alert_limit ? $type->alert_limit : null)), ["max"=>"100", "name"=>'alert_limit', "placeholder"=>"Limite de Alerta", "data-rule-required"=>"true"]) !!}
                                    <span class="add-on">%</span>
                                  </div>
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('active', 'Ativo', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::select('active', ["ativo" => 'Ativo', "inativo" => 'Inativo'], old("active", ($type->active ? "ativo" : "inativo")), ["name"=>'active', "data-rule-required"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="breadcrumbs">
                                <div class="form-actions">
                                    <div class="submit">
                                    <span class="pull-right">
                                        <a href="{{route("typesexpenses.index")}}" class='btn btn-default'>Cancelar</a>
                                        {!! Form::submit('Salvar', ["class"=>'btn btn-primary']) !!}
                                    </span>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
