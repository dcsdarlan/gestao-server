@extends('template')
@section("tittle", "Detalhar Tipos de Despesas")
@section('content')
    <div id="main">
        <div class="container-fluid">
            <div class="page-header">
                <div class="pull-left">
                    <h1>@yield("tittle")</h1>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="breadcrumbs">
                        <ul>
                            <li>
                                <a href="{{route("typesexpenses.index")}}">Listar Tipos de Despesas</a>
                                <i class="icon-angle-right"></i>
                            </li>
                            <li>
                                <a>@yield("tittle")</a>
                            </li>
                        </ul>
                        <div class="close-bread">
                            <a><i class="icon-remove"></i></a>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-title">
                            <h3>
                                <i class="icon-tags"></i>
                                Tipo de Despesa
                            </h3>
                        </div>
                        <div class="box-content">
                            {!! Form::open(array('class' => 'form-horizontal')) !!}
                            @if(Session::has('erro'))
                                <div class="alert alert-danger alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('erro')}}
                                </div>
                            @endif
                            @if(Session::has('sucesso'))
                                <div class="alert alert-success alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('sucesso')}}
                                </div>
                            @endif
                            <div class="control-group">
                                {!!  Form::label('description', 'Descrição', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('description', $type->description, ["readonly"=>true, "name"=>'description', "placeholder"=>"Descrição", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('unit_detail', 'Unidade AdministrativeUnit', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('unit_detail', $type->administrative_unit->description, ["readonly"=>true, "name"=>'unit', "placeholder"=>"Unidade Gestora", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('insitituition', 'Instituição', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('insitituition', $type->administrative_unit->instituition->description, ["readonly"=>true, "name"=>'insitituition', "placeholder"=>"Instituição", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('code', 'Código', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('code', $type->code, ["readonly"=>true, "name"=>'code', "placeholder"=>"Código", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('maximum_limit', 'Limite Máximo (%)', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                  <div class="input-append">
                                    {!! Form::number('maximum_limit', $type->maximum_limit ? $type->maximum_limit : null, ["readonly"=>true, "name"=>'maximum_limit', "placeholder"=>"Limite Prudencial", "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                    <span class="add-on">%</span>
                                  </div>
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('prudential_limit', 'Limite Prudencial (%)', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                  <div class="input-append">
                                    {!! Form::number('prudential_limit', $type->prudential_limit ? $type->prudential_limit : null, ["readonly"=>true, "name"=>'prudential_limit', "placeholder"=>"Limite Prudencial", "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                    <span class="add-on">%</span>
                                  </div>
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('alert_limit', 'Limite de Alerta (%)', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                  <div class="input-append">
                                    {!! Form::number('alert_limit', $type->alert_limit ? $type->alert_limit : null, ["readonly"=>true, "name"=>'alert_limit', "placeholder"=>"Limite de Alerta", "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                    <span class="add-on">%</span>
                                  </div>
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('status', 'Status', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('status', $type->active ? "Ativo": "Inativo", ["readonly"=>true, "name"=>'status', "placeholder"=>"Status", "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="breadcrumbs">
                                <div class="form-actions">
                                    <div class="submit">
                                        <span class="pull-right">
                                            <a href="{{route("typesexpenses.index")}}" class='btn btn-default'>Cancelar</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
