@extends('template')
@section("tittle", "Listar Tipos de Despesas")
@section('content')
<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left">
                <h1>@yield("tittle")</h1>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="breadcrumbs">
                    <ul>
                        <li>
                            <a>@yield("tittle")</a>
                        </li>
                    </ul>
                    <div class="close-bread">
                        <a><i class="icon-remove"></i></a>
                    </div>
                </div>
                <div class="box">
                    <div class="box-title">
                        <h3>
                            <i class="icon-tags"></i>
                            Tipos de Despesas
                        </h3>
                        <a href="{{route('typesexpenses.create')}}" class="pull-right btn btn-blue"><i class="icon-plus"></i>Adicionar</a>
                    </div>
                    <div class="box-content">
                        @if(Session::has('erro'))
                            <div class="alert alert-danger alert-dismissible animate1 fadeIn">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{Session::get('erro')}}
                            </div>
                        @endif
                        @if(Session::has('sucesso'))
                            <div class="alert alert-success alert-dismissible animate1 fadeIn">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{Session::get('sucesso')}}
                            </div>
                        @endif
                        <table class="table dataTable">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Unidade Gestora</th>
                                    <th>Instituição</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($types as $type)
                                <tr>
                                    <td>{{$type->description}}</td>
                                    <td>{{$type->administrative_unit->description}}</td>
                                    <td>{{$type->administrative_unit->instituition->description}}</td>
                                    <td>@if($type->active)<span class="status alert alert-success">Ativo</span>@else<span class="status alert alert-danger">Inativo</span>@endif</td>
                                    <td>
                                        <span class="pull-right">
                                            <a href="{{route('typesexpenses.show', $type->id)}}" class="btn btn-green"><i class="icon-search"></i>Detalhar</a>
                                            <a href="{{route('typesexpenses.edit', $type->id)}}" class="btn btn-orange"><i class="icon-pencil"></i>Alterar</a>
                                            <butom class="btn btn-red" data-toggle="modal" data-target="#delete_{{$type->id}}"><i class="icon-trash"></i>Apagar</butom>
                                            <div class="modal fade" id="delete_{{$type->id}}" tabindex="-1" role="dialog_{{$type->id}}" aria-labelledby="delete" aria-hidden="true">
                                                {!! Form::open(array('route' => array('typesexpenses.destroy', $type->id), 'method' => 'delete', 'class' => 'form-horizontal')) !!}
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">
                                                                    &times;
                                                                </span>
                                                            </button>
                                                            <h4 class="modal-title" id="myModalLabel">
                                                                Apagar Tipo de Despesa
                                                            </h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            Deseja realmente apagar esse registro?
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                                Fechar
                                                            </button>
                                                            <button type="submit" class="btn btn-red">
                                                                Apagar
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </span>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
