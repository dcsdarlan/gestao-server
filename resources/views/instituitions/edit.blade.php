@extends('template')
@section("tittle", "Alterar Instituições")
@section('content')
    <div id="main">
        <div class="container-fluid">
            <div class="page-header">
                <div class="pull-left">
                    <h1>@yield("tittle")</h1>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="breadcrumbs">
                        <ul>
                            <li>
                                <a href="{{route("instituitions.index")}}">Listar Instituições</a>
                                <i class="icon-angle-right"></i>
                            </li>
                            <li>
                                <a>@yield("tittle")</a>
                            </li>
                        </ul>
                        <div class="close-bread">
                            <a><i class="icon-remove"></i></a>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-title">
                            <h3>
                                <i class="icon-th-large"></i>
                                Instituição
                            </h3>
                        </div>
                        <div class="box-content">
                            {!! Form::open(array('route' => array('instituitions.update', $instituition->id), 'method' => 'put', 'class' => 'form-horizontal')) !!}
                            @if(Session::has('erro'))
                                <div class="alert alert-danger alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('erro')}}
                                </div>
                            @endif
                            @if(Session::has('sucesso'))
                                <div class="alert alert-success alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('sucesso')}}
                                </div>
                            @endif
                            <div class="control-group">
                                {!!  Form::label('description', 'Descrição', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('description', old("description", $instituition->descriptio), ["name"=>'description', "placeholder"=>"Descrição", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            @if($instituition->id_instituition)
                            <div class="control-group">
                                {!!  Form::label('instituition', 'Instituição', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::select('instituition', $instituitions, old("instituition", $instituition->id_instituition), ["name"=>'instituition', "data-rule-required"=>"true"]) !!}
                                </div>
                            </div>
                            @endif
                            <div class="control-group">
                                {!!  Form::label('token', 'Token', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::textarea('token', $instituition->token, ["readonly"=>true, "name"=>'token', "placeholder"=>"Token", "rows"=>"2", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>

                            </div>
                            <div class="control-group">
                                {!!  Form::label('active', 'Ativo', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::select('active', ["ativo" => 'Ativo', "inativo" => 'Inativo'], old("active", ($instituition->active ? "ativo" : "inativo")), ["name"=>'active', "data-rule-required"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="breadcrumbs">
                                <div class="form-actions">
                                    <div class="submit">
                                    <span class="pull-right">
                                        <a href="{{route("instituitions.index")}}" class='btn btn-default'>Cancelar</a>
                                        <button  data-toggle="modal" data-target="#dialog_token" class='btn btn-danger'>Gerar Novo Token</button>
                                        {!! Form::submit('Salvar', ["class"=>'btn btn-primary']) !!}
                                    </span>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                            <div class="modal fade" id="dialog_token" tabindex="-1" role="dialog_token" aria-labelledby="delete" aria-hidden="true">
                                {!! Form::open(array('route' => 'instituitions.token', 'method' => 'post', 'class' => 'form-horizontal')) !!}
                                {{ Form::hidden("id", $instituition->id) }}
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">
                                                                    &times;
                                                                </span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">
                                                Gerar Novo Token
                                            </h4>
                                        </div>
                                        <div class="modal-body">
                                            Todas as aplicações que utilizarem o token anterior serão impedidas de acessar o sistema.
                                            Deseja realmente atualizar esse registro?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                Fechar
                                            </button>
                                            <button type="submit" class="btn btn-red">
                                                Gerar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
