@extends('template')
@section("tittle", "Detalhar Instituições")
@section('content')
    <div id="main">
        <div class="container-fluid">
            <div class="page-header">
                <div class="pull-left">
                    <h1>@yield("tittle")</h1>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="breadcrumbs">
                        <ul>
                            <li>
                                <a href="{{route("instituitions.index")}}">Listar Instituições</a>
                                <i class="icon-angle-right"></i>
                            </li>
                            <li>
                                <a>@yield("tittle")</a>
                            </li>
                        </ul>
                        <div class="close-bread">
                            <a><i class="icon-remove"></i></a>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-title">
                            <h3>
                                <i class="icon-th-large"></i>
                                Instituição
                            </h3>
                        </div>
                        <div class="box-content">
                            {!! Form::open(array('class' => 'form-horizontal')) !!}
                            @if(Session::has('erro'))
                                <div class="alert alert-danger alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('erro')}}
                                </div>
                            @endif
                            @if(Session::has('sucesso'))
                                <div class="alert alert-success alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('sucesso')}}
                                </div>
                            @endif
                            <div class="control-group">
                                {!!  Form::label('description', 'Descrição', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('description', $instituition->description, ["readonly"=>true, "name"=>'description', "placeholder"=>"Descrição", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            @if($instituition->id_instituition)
                            <div class="control-group">
                                {!!  Form::label('insitituition', 'Instituição', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('insitituition', $instituition->instituition->description, ["readonly"=>true, "name"=>'insitituition', "placeholder"=>"Instituição", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            @endif
                            <div class="control-group">
                                {!!  Form::label('token', 'Token', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::textarea('token', $instituition->token, ["readonly"=>true, "name"=>'token', "placeholder"=>"Token", "rows"=>"2", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('status', 'Status', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('status', $instituition->active ? "Ativo": "Inativo", ["readonly"=>true, "name"=>'status', "placeholder"=>"Status", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="breadcrumbs">
                                <div class="form-actions">
                                    <div class="submit">
                                        <span class="pull-right">
                                            <a href="{{route("instituitions.index")}}" class='btn btn-default'>Cancelar</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop