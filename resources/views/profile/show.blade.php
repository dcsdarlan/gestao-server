@extends('template')
@section("tittle", "Perfil dos Usuários")
@section('content')
    <div id="main">
        <div class="container-fluid">
            <div class="page-header">
                <div class="pull-left">
                    <h1>@yield("tittle")</h1>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="breadcrumbs">
                        <ul>
                            <li>
                                <a>@yield("tittle")</a>
                            </li>
                        </ul>
                        <div class="close-bread">
                            <a><i class="icon-remove"></i></a>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-title">
                            <h3>
                                <i class="icon-reorder"></i>
                                Perfil do Usuário
                            </h3>
                        </div>
                        <div class="box-content">
                            {!! Form::open(array('route' => 'profile.update', 'method' => 'post', 'class' => 'form-horizontal')) !!}
                            @if(Session::has('erro'))
                                <div class="alert alert-danger alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('erro')}}
                                </div>
                            @endif
                            @if(Session::has('sucesso'))
                                <div class="alert alert-success alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('sucesso')}}
                                </div>
                            @endif
                            <div class="control-group">
                                {!!  Form::label('name', 'Nome', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('name', $user->name, ["readonly"=>true, "name"=>'name', "placeholder"=>"Nome", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('email', 'Email', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('email', $user->email, ["readonly"=>true, "name"=>'email', "placeholder"=>"Email", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('type', 'Tipo de Usuário', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('type', $user->type->description, ["readonly"=>true, "name"=>'type', "placeholder"=>"Tipo", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('instituition', 'Instituição', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('instituition', $user->instituition->description, ["readonly"=>true, "name"=>'instituition', "placeholder"=>"Instituição", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('status', 'Status', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('status', $user->active ? "Ativo": "Inativo", ["readonly"=>true, "name"=>'status', "placeholder"=>"Status", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('password', 'Senha', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::password('password', ["name"=>'password', "placeholder"=>"Senha", "class"=>'input-block-level', "data-rule-required"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('conf_password', 'Confirmação de Senha', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::password('conf_password', ["name"=>'conf_password', "placeholder"=>"Confirmação de Senha", "class"=>'input-block-level', "data-rule-required"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="breadcrumbs">
                                <div class="form-actions">
                                    <div class="submit">
                                        <span class="pull-right">
                                            {!! Form::submit('Salvar', ["class"=>'btn btn-primary']) !!}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    @if(count($user->administratives_units) > 0)
                    <div class="box">
                        <div class="box-title">
                            <h3>
                                <i class="icon-th"></i>
                                Unidades Gestoras
                            </h3>
                        </div>
                        <div class="box-content">
                            <table class="table dataTable">
                                <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Instituição</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($user->administratives_units as $unit)
                                    <tr>
                                        <td>{{$unit->description}}</td>
                                        <td>{{$unit->instituition->description}}</td>
                                        <td>@if($unit->active)<span class="status alert alert-success">Ativo</span>@else<span class="status alert alert-danger">Inativo</span>@endif</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop
