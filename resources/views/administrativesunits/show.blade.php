@extends('template')
@section("tittle", "Detalhar Unidades Gestoras")
@section('content')
    <div id="main">
        <div class="container-fluid">
            <div class="page-header">
                <div class="pull-left">
                    <h1>@yield("tittle")</h1>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="breadcrumbs">
                        <ul>
                            <li>
                                <a href="{{route("instituitions.index")}}">Listar Unidades Gestoras</a>
                                <i class="icon-angle-right"></i>
                            </li>
                            <li>
                                <a>@yield("tittle")</a>
                            </li>
                        </ul>
                        <div class="close-bread">
                            <a><i class="icon-remove"></i></a>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-title">
                            <h3>
                                <i class="icon-th"></i>
                                Unidade Gestora
                            </h3>
                        </div>
                        <div class="box-content">
                            {!! Form::open(array('class' => 'form-horizontal')) !!}
                            @if(Session::has('erro'))
                                <div class="alert alert-danger alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('erro')}}
                                </div>
                            @endif
                            @if(Session::has('sucesso'))
                                <div class="alert alert-success alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('sucesso')}}
                                </div>
                            @endif
                            <div class="control-group">
                                {!!  Form::label('description', 'Descrição', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('description', $unit->description, ["readonly"=>true, "name"=>'description', "placeholder"=>"Descrição", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('insitituition', 'Instituição', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('insitituition', $unit->instituition->description, ["readonly"=>true, "name"=>'insitituition', "placeholder"=>"Instituição", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('code', 'Código', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('code', $unit->code, ["readonly"=>true, "name"=>'code', "placeholder"=>"Código", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('status', 'Status', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('status', $unit->active ? "Ativo": "Inativo", ["readonly"=>true, "name"=>'status', "placeholder"=>"Status", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="breadcrumbs">
                                <div class="form-actions">
                                    <div class="submit">
                                        <span class="pull-right">
                                            <a href="{{route("administrativesunits.index")}}" class='btn btn-default'>Cancelar</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop