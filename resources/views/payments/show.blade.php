@extends('template')
@section("tittle", "Detalhar Pagamentos")
@section('content')
    <div id="main">
        <div class="container-fluid">
            <div class="page-header">
                <div class="pull-left">
                    <h1>@yield("tittle")</h1>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="breadcrumbs">
                        <ul>
                            <li>
                                <a href="{{route("payments.index")}}">Listar Pagamentos</a>
                                <i class="icon-angle-right"></i>
                            </li>
                            <li>
                                <a>@yield("tittle")</a>
                            </li>
                        </ul>
                        <div class="close-bread">
                            <a><i class="icon-remove"></i></a>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-title">
                            <h3>
                                <i class="icon-th-list"></i>
                                Pagamento
                            </h3>
                            <span class="pull-right">
                                @if($payment->status == "confirmed")
                                    <span class="status alert alert-success">Confirmado</span>
                                @elseif($payment->status == "canceled")
                                    <span class="status alert alert-danger">Cancelado</span>
                                @else
                                    <span class="status alert alert-warning">Aguardando</span>
                                @endif
                            </span>
                        </div>
                        <div class="box-content">
                            {!! Form::open(array('class' => 'form-horizontal')) !!}
                            @if(Session::has('erro'))
                                <div class="alert alert-danger alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('erro')}}
                                </div>
                            @endif
                            @if(Session::has('sucesso'))
                                <div class="alert alert-success alert-dismissible animate1 fadeIn">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{Session::get('sucesso')}}
                                </div>
                            @endif
                            <div class="control-group">
                                {!!  Form::label('code', 'Código', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('code', $payment->code, ["readonly"=>true, "name"=>'code', "placeholder"=>"Código", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('date', 'Data', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('date', $payment->date, ["readonly"=>true, "name"=>'date', "placeholder"=>"Data", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('type', 'Tipo de Pagamento', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('type', $payment->type_payment, ["readonly"=>true, "name"=>'type', "placeholder"=>"Tipo de Pagamento", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('unit', 'Unidade Gestora', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('unit', $payment->administrative_unit->description, ["readonly"=>true, "name"=>'unit', "placeholder"=>"Unidade Gestora", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('instituition', 'Instituicao', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('instituition', $payment->administrative_unit->instituition->description, ["readonly"=>true, "name"=>'instituition', "placeholder"=>"Instituição", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('type', 'Tipo de Gasto', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('type', $payment->type_expense, ["readonly"=>true, "name"=>'type', "placeholder"=>"Tipo de Gasto", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('invoice', 'Fatura', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('invoice', $payment->invoice, ["readonly"=>true, "name"=>'invoice', "placeholder"=>"Fatura", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('element', 'Elemento', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('element', $payment->element, ["readonly"=>true, "name"=>'element', "placeholder"=>"Elemento", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('source_resource', 'Fonte de Recurso', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('source_resource', $payment->source_resource, ["readonly"=>true, "name"=>'source_resource', "placeholder"=>"Fonte de Recurso", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('gross_amount', 'Valor Bruto', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('gross_amount', $payment->gross_amount, ["readonly"=>true, "name"=>'source_resource', "placeholder"=>"Valor Bruto", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('net_amount', 'Valor Líquido', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('net_amount', $payment->net_amount, ["readonly"=>true, "name"=>'net_amount', "placeholder"=>"Valor Líquido", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('discount', 'Desconto', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('discount', $payment->discount, ["readonly"=>true, "name"=>'discount', "placeholder"=>"Desconto", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('bidding', 'Licitação', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('bidding', $payment->bidding, ["readonly"=>true, "name"=>'bidding', "placeholder"=>"Licitação", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('contract', 'Contrato', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('contract', $payment->contract, ["readonly"=>true, "name"=>'contract', "placeholder"=>"Contrato", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('creditor', 'Credor', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('creditor', $payment->creditor, ["readonly"=>true, "name"=>'creditor', "placeholder"=>"Credor", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('type', 'Tipo de Credor', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::text('type', $payment->type_creditor, ["readonly"=>true, "name"=>'type', "placeholder"=>"Tipo de Credor", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('historic', 'Histórico', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::textarea('historic', $payment->historic, ["readonly"=>true, "name"=>'historic', "placeholder"=>"Histórico", "rows"=>"2", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            <div class="control-group">
                                {!!  Form::label('observation', 'Observação', ["class"=>"control-label"]) !!}
                                <div class="controls">
                                    {!! Form::textarea('observation', $payment->observation, ["readonly"=>true, "name"=>'observation', "placeholder"=>"Observação", "rows"=>"2", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                </div>
                            </div>
                            @if($payment->operated_at)
                                @if($payment->status == "confirmed")
                                    <div class="status alert alert-success">
                                        <div class="control-group">
                                            {!!  Form::label('user', 'Aprovado Por', ["class"=>"control-label"]) !!}
                                            <div class="controls">
                                                {!! Form::text('user', $payment->user_manager->name, ["readonly"=>true, "name"=>'user', "placeholder"=>"Usuario", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            {!!  Form::label('date', 'Aprovado Em', ["class"=>"control-label"]) !!}
                                            <div class="controls">
                                                {!! Form::text('date', $payment->operated_at, ["readonly"=>true, "name"=>'date', "placeholder"=>"Data", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                            </div>
                                        </div>
                                    </div>
                                @elseif($payment->status == "canceled")
                                    <div class="status alert alert-danger">
                                        <div class="control-group">
                                            {!!  Form::label('user', 'Cancelado Por', ["class"=>"control-label"]) !!}
                                            <div class="controls">
                                                {!! Form::text('user', $payment->user_manager->name, ["readonly"=>true, "name"=>'user', "placeholder"=>"Usuario", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            {!!  Form::label('date', 'Cancelado Em', ["class"=>"control-label"]) !!}
                                            <div class="controls">
                                                {!! Form::text('date', $payment->operated_at, ["readonly"=>true, "name"=>'date', "placeholder"=>"Data", "class"=>'input-block-level', "data-rule-required"=>"true",  "data-rule-email"=>"true"]) !!}
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="status alert alert-warning">Aguardando</div>
                                @endif
                            @endif
                            <div class="breadcrumbs">
                                <div class="form-actions">
                                    <div class="submit">
                                        <span class="pull-right">
                                            @if($payment->status == "waiting")
                                              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_approve"><i class="icon-money"></i>   Aprovar</button>
                                              <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal_deprive"><i class="icon-trash"></i>   Rejeitar</button>
                                            @endif
                                            <a href="{{route("payments.index")}}" class='btn btn-default'>Cancelar</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}

                            @if($payment->status == "waiting")
                            <div class="modal fade" id="modal_approve" tabindex="-1" role="dialog" aria-labelledby="approve">
                                {!! Form::open(array('route' => 'payments.approve', 'method' => 'post', 'class' => 'form-horizontal')) !!}
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Aviso</h4>
                                            </div>
                                            <div class="modal-body">
                                                <h5>Tem certeza que deseja aprovar esses registro?</h5>
                                                <br>
                                                <table class="table" id="modal_table_approve">
                                                    <thead>
                                                    <tr>
                                                        <th>Código</th>
                                                        <th>Valor</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                      <tr>
                                                        <td>{{$payment->code}}</td>
                                                        <td>{{$payment->net_amount}}<input type='hidden' name='payment_id[]' value='{{$payment->id}}'></td>
                                                      </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <span id="total_approve" class="pull-left btn btn-danger">{{$payment->net_amount}}</span>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                                {!! Form::submit('Aprovar', ["class"=>'btn btn-primary']) !!}
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>

                            <div class="modal fade" id="modal_deprive" tabindex="-1" role="dialog" aria-labelledby="deprive">
                                {!! Form::open(array('route' => 'payments.deprive', 'method' => 'post', 'class' => 'form-horizontal')) !!}<div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Aviso</h4>
                                            </div>
                                            <div class="modal-body">
                                                <h5>Tem certeza que deseja cancelar esses registro?</h5>
                                                <br>
                                                <table class="table" id="modal_table_deprive">
                                                    <thead>
                                                    <tr>
                                                        <th>Código</th>
                                                        <th>Valor</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                      <tr>
                                                        <td>{{$payment->code}}</td>
                                                        <td>{{$payment->net_amount}}<input type='hidden' name='payment_id[]' value='{{$payment->id}}'></td>
                                                      </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="control-group">
                                                    <div class="span12">
                                                        <textarea name="obs" placeholder="Observação" class="input-block-level"></textarea>
                                                    </div>
                                                </div>
                                                <span id="total_deprive" class="pull-left btn btn-danger">{{$payment->net_amount}}</span>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                                {!! Form::submit('Rejeitar', ["class"=>'btn btn-danger']) !!}
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
