@extends('template')
@section("tittle", "Listar Pagamentos")
@section('content')
<div id="main">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-left">
                <h1>Listar Pagamentos</h1>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="breadcrumbs">
                    <ul>
                        <li>
                            <a>Listar Pagamentos</a>
                        </li>
                    </ul>
                    <div class="close-bread">
                        <a><i class="icon-remove"></i></a>
                    </div>
                </div>
                <div class="box">
                    <div class="box-title">
                        <h3>
                            <i class="icon-th-list"></i>
                            Pagamentos
                        </h3>
                    </div>
                    <div class="box-content">
                        @if(Session::has('erro'))
                            <div class="alert alert-danger alert-dismissible animate1 fadeIn">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{Session::get('erro')}}
                            </div>
                        @endif
                        @if(Session::has('sucesso'))
                            <div class="alert alert-success alert-dismissible animate1 fadeIn">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{Session::get('sucesso')}}
                            </div>
                        @endif
                        <div class="box">
                            <div class="box-content">
                                <div class="box">
                                    <div class="box-title"><h3>Filtro</h3></div>
                                    {!! Form::open(array('route' => 'payments.filter', 'method' => 'post')) !!}
                                        <div class="box-content">
                                            <div>
                                                <div class="span3">
                                                    <div>
                                                        {!!  Form::label('filter_payment_date', 'Data', ["class"=>"control-label"]) !!}
                                                        <div class="controls">
                                                            {!! Form::text('filter_payment_date', Session::has('filter_payment_date') ? Session::get('filter_payment_date') : date('d/m/Y - d/m/Y'), ["name"=>'filter_payment_date', "placeholder"=>"Data", "class"=>'input-block-level daterangepick']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span3">
                                                    <div>
                                                        {!!  Form::label('filter_payment_unit', 'Unidade Gestora', ["class"=>"control-label"]) !!}
                                                        <div class="controls">
                                                            {!! Form::select('filter_payment_unit', $units, Session::has('filter_payment_unit') ? Session::get('filter_payment_unit') : null, ["name"=>'filter_payment_unit[]', "data-placeholder" => "Unidade Gestora",  "data-rule-required"=>"true", "class"=>'chosen-select input-block-level', "multiple"=>"multiple"]) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span3">
                                                    <div>
                                                        {!!  Form::label('filter_payment_creditor', 'Credor', ["class"=>"control-label"]) !!}
                                                        <div class="controls">
                                                            {!! Form::text('filter_payment_creditor', Session::has('filter_payment_creditor') ? Session::get('filter_payment_creditor') : "", ["name"=>'filter_payment_creditor', "placeholder"=>"Credor", "class"=>'input-block-level']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span3">
                                                    <div>
                                                        {!!  Form::label('filter_payment_type_creditor', 'Tipo de Credor', ["class"=>"control-label"]) !!}
                                                        <div class="controls">
                                                            {!! Form::text('filter_payment_type_creditor', Session::has('filter_payment_type_creditor') ? Session::get('filter_payment_type_creditor') : "", ["name"=>'filter_payment_type_creditor', "placeholder"=>"Tipo de Credor", "class"=>'input-block-level']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="span3">
                                                    <div>
                                                        {!!  Form::label('filter_payment_element', 'Elemento', ["class"=>"control-label"]) !!}
                                                        <div class="controls">
                                                            {!! Form::text('filter_payment_element', Session::has('filter_payment_element') ? Session::get('filter_payment_element') : "", ["name"=>'filter_payment_element', "placeholder"=>"Elemento", "class"=>'input-block-level']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span3">
                                                    <div>
                                                        {!!  Form::label('filter_payment_source_resource', 'Fonte de Recurso', ["class"=>"control-label"]) !!}
                                                        <div class="controls">
                                                            {!! Form::text('filter_payment_source_resource', Session::has('filter_payment_source_resource') ? Session::get('filter_payment_source_resource') : "", ["name"=>'filter_payment_source_resource', "placeholder"=>"Fonte de Recurso", "class"=>'input-block-level']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="span3">
                                                    <div>
                                                        <label for="textfield" class="control-label">Status</label>
                                                        <div class="controls">
                                                            <select class="input-block-level" name="filter_payment_status">
                                                                <option value="all" @if(Session::get('filter_payment_status') == "all") selected @endif>Todos</option>
                                                                <option value="waiting" @if(!Session::has('filter_payment_status') || Session::get('filter_payment_status') == "waiting") selected @endif>Aguardando</option>
                                                                <option value="confirmed" @if(Session::get('filter_payment_status') == "confirmed") selected @endif>Aprovado</option>
                                                                <option value="canceled" @if(Session::get('filter_payment_status') == "canceled") selected @endif>Cancelado</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="span3">
                                                    <br>
                                                    <button type="submit" class="btn btn-blue span5" value="Filtrar"><i class="icon-search"></i>  Filtrar</button>
                                                    <a href="{{route("payments.filter.clear")}}" class="btn btn-default span5"><i class="icon-trash"></i>  Limpar</a>
                                                </div>
                                            </div>

                                        </div>
                                    {!! Form::close() !!}
                                </div>
                                <div class="modal fade" id="modal_approve" tabindex="-1" role="dialog" aria-labelledby="approve">
                                    {!! Form::open(array('route' => 'payments.approve', 'method' => 'post', 'class' => 'form-horizontal')) !!}
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Aviso</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <h5>Tem certeza que deseja aprovar esses registro?</h5>
                                                    <br>
                                                    <table class="table" id="modal_table_approve">
                                                        <thead>
                                                        <tr>
                                                            <th>Código</th>
                                                            <th>Valor</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                    <span id="total_approve" class="pull-left btn btn-danger"></span>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                                    {!! Form::submit('Aprovar', ["class"=>'btn btn-primary']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </div>

                                <div class="modal fade" id="modal_deprive" tabindex="-1" role="dialog" aria-labelledby="deprive">
                                    {!! Form::open(array('route' => 'payments.deprive', 'method' => 'post', 'class' => 'form-horizontal')) !!}<div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Aviso</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <h5>Tem certeza que deseja cancelar esses registro?</h5>
                                                    <br>
                                                    <table class="table" id="modal_table_deprive">
                                                        <thead>
                                                        <tr>
                                                            <th>Código</th>
                                                            <th>Valor</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="control-group">
                                                        <div class="span12">
                                                            <textarea name="obs" placeholder="Observação" class="input-block-level"></textarea>
                                                        </div>
                                                    </div>
                                                    <span id="total_deprive" class="pull-left btn btn-danger"></span>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                                    {!! Form::submit('Rejeitar', ["class"=>'btn btn-danger']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </div>

                                <div class="modal fade" id="modal_aviso" tabindex="-1" role="dialog" aria-labelledby="aviso">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Aviso</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="alert alert-danger alert-dismissible animate1 fadeIn" id="alert-erro-graphic">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    <span>Você precisa selecionar pelo menos um registro.</span>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <table  class="table dataTable">
                                     <thead>
                                        <tr role="row">
                                            <th><input type="checkbox" class="todos" ></th>
                                            <th>Código</th>
                                            <th>Conta</th>
                                            <th>Unidade Gestora</th>
                                            <th>Valor Líquido</th>
                                            <th>Data</th>
                                            <th>Status</th>
                                            <th>
                                                <span class="pull-right">
                                                    <button type="button" class="btn btn-primary" id="approve"><i class="icon-money"></i>   Aprovar</button>
                                                    <button type="button" class="btn btn-danger" id="deprive"><i class="icon-trash"></i>   Rejeitar</button>
                                                </span>
                                            </th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach ($payments as $payment)
                                        <tr>
                                            <td>
                                                @if($payment->status == "waiting")
                                                    <input type="checkbox" payment-id="{{$payment->id}}" payment-value="{{$payment->net_amount}}" payment-code="{{$payment->code}}" payment-status="{{$payment->status}}" class="payment">
                                                @endif
                                            </td>
                                            <td>{{$payment->code}}</td>
                                            <td>{{$payment->invoice}}</td>
                                            <td>{{$payment->administrative_unit->description}}</td>
                                            <td>{{$payment->net_amount}}</td>
                                            <td>{{$payment->date}}</td>
                                            <td>
                                                @if($payment->status == "confirmed")
                                                    <span class="status alert alert-success">Confirmado</span>
                                                @elseif($payment->status == "canceled")
                                                    <span class="status alert alert-danger">Cancelado</span>
                                                @else
                                                    <span class="status alert alert-warning">Aguardando</span>
                                                @endif
                                            </td>
                                            <td>
                                                <span class="pull-right">
                                                    <a href="{{route('payments.show', $payment->id)}}" class="btn btn-green"><i class="icon-search"></i>Detalhar</a>
                                                </span>
                                            </td>
                                        </tr>
                                        @endforeach
                                   </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
