<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <title>{{config('app.name')}} | @yield("tittle")</title>
    {{ Html::style('flat/html/css/bootstrap.min.css') }}
    {{ Html::style('flat/html/css/bootstrap-responsive.min.css') }}
    {{ Html::style('flat/html/css/plugins/jquery-ui/smoothness/jquery-ui.css') }}
    {{ Html::style('flat/html/css/plugins/jquery-ui/smoothness/jquery.ui.theme.css') }}
    {{ Html::style('flat/html/css/plugins/pageguide/pageguide.css') }}
    {{ Html::style('flat/html/css/plugins/fullcalendar/fullcalendar.css') }}
    {{ Html::style('flat/html/css/plugins/fullcalendar/fullcalendar.print.css') }}
    {{ Html::style('flat/html/css/plugins/datatable/TableTools.css') }}
    {{ Html::style('flat/html/css/plugins/chosen/chosen.css') }}
    {{ Html::style('flat/html/css/plugins/select2/select2.css') }}
    {{ Html::style('flat/html/css/plugins/icheck/all.css') }}
    {{ Html::style('flat/html/css/plugins/datepicker/datepicker.css') }}
    {{ Html::style('flat/html/css/plugins/daterangepicker/daterangepicker.css') }}
    {{ Html::style('flat/html/css/style.css') }}
    {{ Html::style('flat/html/css/themes.css') }}
    {{ Html::style('css/styles.min.css') }}
    <link rel="shortcut icon" href="img/favicon.ico" />
    <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-precomposed.png" />
</head>

<body data-mobile-sidebar="button" class="theme-darkblue" >
<div id="navigation">
    <div class="container-fluid">
        <a class="mobile-sidebar-toggle" style="z-index:99999!important;"><i class="icon-th-list"></i></a>
        <a href="{{route("home.index")}}" id="brand">{{config('app.name')}}</a>
        <a class="toggle-nav" rel="tooltip" data-placement="bottom" title="Navegação"><i class="icon-reorder"></i></a>
        <div class="user">
            <div class="dropdown">
                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">{{Auth::user()->name}}</a>
                <ul class="dropdown-menu pull-right">
                    <li>
                        <a href="{{route("profile.show")}}">Perfil</a>
                    </li>
                    <li>
                        <a href="{{route("auth.logoff")}}">Sair</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid" id="content">
    <div id="left">
        @if(Auth::user()->type->description == "Administrador")
        <div class="subnav">
            <div class="subnav-title">
                <a href="{{route("users.index")}}" class="toggle-gessubnav"><i class="icon-user"></i><span>Usuários</span></a>
            </div>
        </div>
        <div class="subnav">
            <div class="subnav-title">
                <a href="{{route("instituitions.index")}}" class="toggle-gessubnav"><i class="icon-th-large"></i><span>Instituições</span></a>
            </div>
        </div>
        <div class="subnav">
            <div class="subnav-title">
                <a href="{{route("administrativesunits.index")}}" class="toggle-gessubnav"><i class="icon-th"></i><span>Unid. Gestoras</span></a>
            </div>
        </div>
        @endif
        @if(Auth::user()->type->description == "Administrador" || Auth::user()->type->description == "Gestor")
        <div class="subnav">
            <div class="subnav-title">
                <a href="{{route("payments.index")}}" class="toggle-gessubnav"><i class="icon-th-list"></i><span>Pagamentos</span></a>
            </div>
        </div>
        @endif
        @if(Auth::user()->type->description == "Administrador" || Auth::user()->type->description == "Gestor" || Auth::user()->type->description == "Funcionario")
        <div class="subnav">
            <div class="subnav-title">
                <a href="{{route("typesexpenses.index")}}" class="toggle-gessubnav"><i class="icon-tags"></i><span>Tipos de Despesas</span></a>
            </div>
        </div>
        <div class="subnav">
            <div class="subnav-title">
                <a href="{{route("revenues.index")}}" class="toggle-gessubnav"><i class="icon-signin"></i><span>Receitas</span></a>
            </div>
        </div>
        <div class="subnav">
            <div class="subnav-title">
                <a href="{{route("expenses.index")}}" class="toggle-gessubnav"><i class="icon-signout"></i><span>Despesas</span></a>
            </div>
        </div>
        <div class="subnav">
            <div class="subnav-title">
                <a href="{{route("graphics.index")}}" class="toggle-gessubnav"><i class="icon-bar-chart"></i><span>Gráficos</span></a>
            </div>
        </div>
        @endif
    </div>
    @yield("content")
</div>
<footer id="footer">
    <p>
        <span>Tudo Municipal</span>
        <span class="font-grey-4">|</span>
        <a href="mailto:contato@tudomunicipal.com.br">Contato</a>
    </p>
    <a href="http://localhost:8000/payments/list#" class="gototop">
        <i class="fa fa-arrow-up"></i>
    </a>
</footer>
</body>
{{ Html::script('flat/html/js/jquery.min.js') }}
{{ Html::script('flat/html/js/plugins/nicescroll/jquery.nicescroll.min.js') }}
{{ Html::script('flat/html/js/plugins/jquery-ui/jquery.ui.core.min.js') }}
{{ Html::script('flat/html/js/plugins/jquery-ui/jquery.ui.widget.min.js') }}
{{ Html::script('flat/html/js/plugins/jquery-ui/jquery.ui.mouse.min.js') }}
{{ Html::script('flat/html/js/plugins/jquery-ui/jquery.ui.draggable.min.js') }}
{{ Html::script('flat/html/js/plugins/jquery-ui/jquery.ui.resizable.min.js') }}
{{ Html::script('flat/html/js/plugins/jquery-ui/jquery.ui.sortable.min.js') }}
{{ Html::script('flat/html/js/plugins/touch-punch/jquery.touch-punch.min.js') }}
{{ Html::script('flat/html/js/plugins/slimscroll/jquery.slimscroll.min.js') }}
{{ Html::script('flat/html/js/bootstrap.min.js') }}
{{ Html::script('flat/html/js/plugins/bootbox/jquery.bootbox.js') }}
{{ Html::script('flat/html/js/plugins/daterangepicker/moment.min.js') }}
{{ Html::script('flat/html/js/plugins/slimscroll/jquery.slimscroll.min.js') }}
{{ Html::script('flat/html/js/plugins/datatable/jquery.dataTables.min.js') }}
{{ Html::script('flat/html/js/plugins/datatable/TableTools.min.js') }}
{{ Html::script('flat/html/js/plugins/datatable/ColReorder.min.js') }}
{{ Html::script('flat/html/js/plugins/datatable/ColVis.min.js') }}
{{ Html::script('flat/html/js/plugins/datatable/dataTables.scroller.min.js') }}
{{ Html::script('flat/html/js/plugins/vmap/jquery.vmap.min.js') }}
{{ Html::script('flat/html/js/plugins/vmap/jquery.vmap.world.js') }}
{{ Html::script('flat/html/js/plugins/vmap/jquery.vmap.sampledata.js') }}
{{ Html::script('flat/html/js/plugins/bootbox/jquery.bootbox.js') }}
{{ Html::script('flat/html/js/plugins/flot/jquery.flot.min.js') }}
{{ Html::script('flat/html/js/plugins/flot/jquery.flot.bar.order.min.js') }}
{{ Html::script('flat/html/js/plugins/flot/jquery.flot.pie.min.js') }}
{{ Html::script('flat/html/js/plugins/flot/jquery.flot.resize.min.js') }}
{{ Html::script('flat/html/js/plugins/flot/jquery.flot.resize.min.js') }}
{{ Html::script('flat/html/js/plugins/imagesLoaded/jquery.imagesloaded.min.js') }}
{{ Html::script('flat/html/js/plugins/pageguide/jquery.pageguide.js') }}
{{ Html::script('flat/html/js/plugins/fullcalendar/fullcalendar.min.js') }}
{{ Html::script('flat/html/js/plugins/chosen/chosen.jquery.min.js') }}
{{ Html::script('flat/html/js/plugins/select2/select2.min.js') }}
{{ Html::script('flat/html/js/plugins/validation/jquery.validate.min.js') }}
{{ Html::script('flat/html/js/plugins/validation/additional-methods.min.js') }}
{{ Html::script('flat/html/js/plugins/icheck/jquery.icheck.min.js') }}
{{ Html::script('flat/html/js/plugins/datepicker/bootstrap-datepicker.js') }}
{{ Html::script('flat/html/js/plugins/datepicker/locales/bootstrap-datepicker.pt-BR.js') }}
{{ Html::script('flat/html/js/plugins/daterangepicker/daterangepicker.js') }}
{{ Html::script('flat/html/js/eakroko.js') }}
{{ Html::script('flat/html/js/application.js') }}
{{ Html::script('bower_components/jquery-mask-plugin/dist/jquery.mask.min.js') }}
{{ Html::script('bower_components/chart.js/dist/Chart.min.js') }}
{{ Html::script('js/scripts.min.js') }}
<!--[if lte IE 9]>
{{ Html::script('flat/html/js/plugins/placeholder/jquery.placeholder.min.js') }}
<script>
    $(document).ready(function() {
        $('input, textarea').placeholder();
    });
</script>
<![endif]-->
</html>
