/**
 * Created by darlan on 07/12/16.
 */
var urlAjax = window.location.protocol + "//" + window.location.host + "/api/v1/";

var payments = [];
$(document).ready(function() {
    $(".value").mask('000.000.000.000.000,00', {reverse: true});
    $("select").chosen({
        disable_search_threshold: 9999999
    });
    $('.datepick').datepicker({
        format: "dd/mm/yyyy",
        language: 'pt-BR'
    }).on('changeDate', function(ev){
        $('.datepick').datepicker('hide');
    });
    $(".daterangepick").daterangepicker({
        autoUpdateInput: false,
        format: "DD/MM/YYYY",
        locale: {
            format: "DD/MM/YYYY",
            separator: " - ",
            applyLabel: "Aplicar",
            cancelLabel: "Cancelar",
            clearLabel: "Limpar",
            weekLabel: "W",
            fromLabel: "De",
            toLabel: "Até",
            customRangeLabel: "Selecionar Data",
            monthNames: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
            daysOfWeek: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"]
        },
        ranges: {
            Hoje: [moment(), moment()],
            "Mês Atual": [moment().startOf("month"), moment().endOf("month")]
        }
    });
    $(".daterangepick").on("apply.daterangepicker", function(ev, picker) {
        $(this).val(picker.startDate.format("DD/MM/YYYY") + " - " + picker.endDate.format("DD/MM/YYYY"));
    });
    $(".daterangepick").on("cancel.daterangepicker", function(ev, picker) {
        $(this).val("");
    });
    $("#instituition").change(function(){
        if($(this).val()) {
          $.get(urlAjax + "administrativesunits/instituitions/" + $(this).val(), function(data, status, statusCode){
              if(statusCode.status == 200) {
                  $('#unit').empty();
                  $('#unit').append('<option value="">-- Selecione uma Unidade Gestora --</option>');
                  $.each(data, function (idx, obj) {
                      $('#unit').append('<option value="' + obj.id + '">' + obj.description + '</option>');
                  });
                  $('#unit').trigger("liszt:updated");
                  $('#unit').chosen({
                      disable_search_threshold: 9999999
                  });
              } else {
                $('#unit').empty();
                $('#unit').append('<option value="">-- Selecione uma Unidade Gestora --</option>');
                $('#unit').trigger("liszt:updated");
                $('#unit').chosen({
                    disable_search_threshold: 9999999
                });
                $('#type').empty();
                $('#type').append('<option value="">-- Selecione um Tipo de Despesa --</option>');
                $('#type').trigger("liszt:updated");
                $('#type').chosen({
                    disable_search_threshold: 9999999
                });
              }
          });
        } else {
            $('#unit').empty();
            $('#unit').append('<option value="">-- Selecione uma Unidade Gestora --</option>');
            $('#unit').trigger("liszt:updated");
            $('#unit').chosen({
                disable_search_threshold: 9999999
            });
            $('#type').empty();
            $('#type').append('<option value="">-- Selecione um Tipo de Despesa --</option>');
            $('#type').trigger("liszt:updated");
            $('#type').chosen({
                disable_search_threshold: 9999999
            });
        }
    });
    $("#unit").change(function(){
        if($(this).val()) {
          $.get(urlAjax + "typesexpenses/administrativesunits/" + $(this).val(), function(data, status, statusCode){
              if(statusCode.status == 200) {
                  $('#type').empty();
                  $('#type').append('<option value="">-- Selecione um Tipo de Despesa --</option>');
                  $.each(data, function (idx, obj) {
                      $('#type').append('<option value="' + obj.id + '">' + obj.description + '</option>');
                  });
                  $('#type').trigger("liszt:updated");
                  $('#type').chosen({
                      disable_search_threshold: 9999999
                  });
              } else {
                $('#type').empty();
                $('#type').append('<option value="">-- Selecione um Tipo de Despesa --</option>');
                $('#type').trigger("liszt:updated");
                $('#type').chosen({
                    disable_search_threshold: 9999999
                });
              }
          });
        } else {
            $('#type').empty();
            $('#type').append('<option value="">-- Selecione um Tipo de Despesa --</option>');
            $('#type').trigger("liszt:updated");
            $('#type').chosen({
                disable_search_threshold: 9999999
            });
        }
    });
    $("#gerar_grafico").click(function(){
        $.ajax({
            url: urlAjax + "graphics/month",
            type: "POST",
            data: {instituition: $("#instituition").val(), unit: $("#unit").val(), type: $("#type").val(), month: $("#month").val(), year: $("#year").val()},
            complete: function(data){
                console.log(urlAjax);
                console.log(data);
                var json = JSON.parse(data.responseText);
                console.log();
                if(data.status == 200) {

                    $('#dataBt').addClass("btn-primary");
                    $('#ateDataBt').addClass("btn-secundary");

                  var totalRevenue = 0;
                  $.each(json.revenues, function (idx, obj) {
                      console.log(obj.value.replace("R$ ", "").replace(/\./g, "").replace(",", "."));
                      totalRevenue += parseFloat(obj.value.replace("R$ ", "").replace(/\./g, "").replace(",", "."));
                  });
                  console.log("Total Revenue: " + totalRevenue);
                  var totalExpense = 0;
                  $.each(json.expenses, function (idx, obj) {
                      console.log(obj.value.replace("R$ ", "").replace(/\./g, "").replace(",", "."));
                      totalExpense += parseFloat(obj.value.replace("R$ ", "").replace(/\./g, "").replace(",", "."));
                  });
                  console.log("Total Expense: " + totalExpense);
                  var gastos = totalExpense
                  var limite = totalRevenue - totalExpense;
                  $('#grafic').modal("show");

                  if(json.maximum_limit && json.prudential_limit && json.alert_limit) {
                    var limMax = totalRevenue * (json.maximum_limit/100);
                    var limPru = totalRevenue * (json.prudential_limit/100);
                    var limAle = totalRevenue * (json.alert_limit/100);
                    $('#limit-max').html(limMax.toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL"
                    }) + " - " + json.maximum_limit + "%");
                    $('#limit-prudential').html(limPru.toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL"
                    }) + " - " + json.prudential_limit + "%");
                    $('#limit-alert').html(limAle.toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL"
                    }) + " - " + json.alert_limit + "%");
                    $("#graphic-limit").show();
                  } else {
                    $("#graphic-limit").hide();
                  }
                  console.log(JSON.parse(data.responseText));
                  $('#alert-erro-graphic').hide();
                  $("#c-graphic").show();
                      var ctx = $("#c-graphic");
                      var data = {
                        labels: [
                            "Gastos Totais: " + totalExpense.toLocaleString("pt-BR", {
                                style: "currency",
                                currency: "BRL"
                            }),
                            "Receitas Totais: " + totalRevenue.toLocaleString("pt-BR", {
                                style: "currency",
                                currency: "BRL"
                            })
                        ],
                        datasets: [
                          {
                              data: [gastos, limite],
                              backgroundColor: [
                                  "#FF0000",
                                  "#CCCCCC",
                              ],
                              hoverBackgroundColor: [
                                  "#FF0000",
                                  "#CCCCCC"
                              ]
                          }]
                      };
                      new Chart(ctx, {
                          type: 'doughnut',
                          data: data,
                          options: {
                              responsive: false,
                              title: {
                                  display: true,
                                  text: 'Gastos ' + $("#month").val() + "/" + $("#year").val()
                              }
                          }
                      });
              } else {
                $('#grafic').modal("show");
                $("#c-graphic").hide();
                $("#graphic-limit").hide();
                $('#alert-erro-graphic').show();
                $('#erro-graphic').html(json.erro[0]);
              }
            }
        });
    });
    $("#dataBt").click(function(){
        $.ajax({
            url: urlAjax + "graphics/month",
            type: "POST",
            data: {instituition: $("#instituition").val(), unit: $("#unit").val(), type: $("#type").val(), month: $("#month").val(), year: $("#year").val()},
            complete: function(data){
                console.log(urlAjax);
                console.log(data);
                var json = JSON.parse(data.responseText);
                console.log();
                if(data.status == 200) {
                    $('#dataBt').removeClass("btn-secundary");
                    $('#dataBt').addClass("btn-primary");
                    $('#ateDataBt').removeClass("btn-primary");
                    $('#ateDataBt').addClass("btn-secundary");

                    var totalRevenue = 0;
                    $.each(json.revenues, function (idx, obj) {
                        totalRevenue += parseFloat(obj.value.replace("R$ ", "").replace(/\./g, "").replace(",", "."));
                    });
                    console.log("Total Revenue: " + totalRevenue);
                    var totalExpense = 0;
                    $.each(json.expenses, function (idx, obj) {
                        totalExpense += parseFloat(obj.value.replace("R$ ", "").replace(/\./g, "").replace(",", "."));
                    });
                    console.log("Total Expense: " + totalExpense);
                    var gastos = totalExpense
                    var limite = totalRevenue - totalExpense;

                    if(json.maximum_limit && json.prudential_limit && json.alert_limit) {
                        var limMax = totalRevenue * (json.maximum_limit/100);
                        var limPru = totalRevenue * (json.prudential_limit/100);
                        var limAle = totalRevenue * (json.alert_limit/100);
                        $('#limit-max').html(limMax.toLocaleString("pt-BR", {
                                style: "currency",
                                currency: "BRL"
                            }) + " - " + json.maximum_limit + "%");
                        $('#limit-prudential').html(limPru.toLocaleString("pt-BR", {
                                style: "currency",
                                currency: "BRL"
                            }) + " - " + json.prudential_limit + "%");
                        $('#limit-alert').html(limAle.toLocaleString("pt-BR", {
                                style: "currency",
                                currency: "BRL"
                            }) + " - " + json.alert_limit + "%");
                        $("#graphic-limit").show();
                    } else {
                        $("#graphic-limit").hide();
                    }
                    console.log(JSON.parse(data.responseText));
                    var ctx = $("#c-graphic");
                    var data = {
                        labels: [
                            "Gastos Totais: " + totalExpense.toLocaleString("pt-BR", {
                                style: "currency",
                                currency: "BRL"
                            }),
                            "Receitas Totais: " + totalRevenue.toLocaleString("pt-BR", {
                                style: "currency",
                                currency: "BRL"
                            })
                        ],
                        datasets: [
                            {
                                data: [gastos, limite],
                                backgroundColor: [
                                    "#FF0000",
                                    "#CCCCCC",
                                ],
                                hoverBackgroundColor: [
                                    "#FF0000",
                                    "#CCCCCC"
                                ]
                            }]
                    };
                    new Chart(ctx, {
                        type: 'doughnut',
                        data: data,
                        options: {
                            responsive: false,
                            title: {
                                display: true,
                                text: 'Gastos ' + $("#month").val() + "/" + $("#year").val()
                            }
                        }
                    });
                }
            }
        });
    });
    $("#ateDataBt").click(function(){
        $.ajax({
            url: urlAjax + "graphics/year",
            type: "POST",
            data: {instituition: $("#instituition").val(), unit: $("#unit").val(), type: $("#type").val(), month: $("#month").val(), year: $("#year").val()},
            complete: function(data){
                console.log(urlAjax);
                console.log(data);
                var json = JSON.parse(data.responseText);
                console.log();
                if(data.status == 200) {
                    $('#dataBt').removeClass("btn-primary");
                    $('#dataBt').addClass("btn-secundary");
                    $('#ateDataBt').removeClass("btn-secundary");
                    $('#ateDataBt').addClass("btn-primary");

                    var totalRevenue = 0;
                    $.each(json.revenues, function (idx, obj) {
                        totalRevenue += parseFloat(obj.value.replace("R$ ", "").replace(/\./g, "").replace(",", "."));
                    });
                    console.log("Total Revenue: " + totalRevenue);
                    var totalExpense = 0;
                    $.each(json.expenses, function (idx, obj) {
                        totalExpense += parseFloat(obj.value.replace("R$ ", "").replace(/\./g, "").replace(",", "."));
                    });
                    console.log("Total Expense: " + totalExpense);
                    var gastos = totalExpense
                    var limite = totalRevenue - totalExpense;

                    if(json.maximum_limit && json.prudential_limit && json.alert_limit) {
                        var limMax = totalRevenue * (json.maximum_limit/100);
                        var limPru = totalRevenue * (json.prudential_limit/100);
                        var limAle = totalRevenue * (json.alert_limit/100);
                        $('#limit-max').html(limMax.toLocaleString("pt-BR", {
                                style: "currency",
                                currency: "BRL"
                            }) + " - " + json.maximum_limit + "%");
                        $('#limit-prudential').html(limPru.toLocaleString("pt-BR", {
                                style: "currency",
                                currency: "BRL"
                            }) + " - " + json.prudential_limit + "%");
                        $('#limit-alert').html(limAle.toLocaleString("pt-BR", {
                                style: "currency",
                                currency: "BRL"
                            }) + " - " + json.alert_limit + "%");
                        $("#graphic-limit").show();
                    } else {
                        $("#graphic-limit").hide();
                    }
                    console.log(JSON.parse(data.responseText));
                    var ctx = $("#c-graphic");
                    var data = {
                        labels: [
                            "Gastos Totais: " + totalExpense.toLocaleString("pt-BR", {
                                style: "currency",
                                currency: "BRL"
                            }),
                            "Receitas Totais: " + totalRevenue.toLocaleString("pt-BR", {
                                style: "currency",
                                currency: "BRL"
                            })
                        ],
                        datasets: [
                            {
                                data: [gastos, limite],
                                backgroundColor: [
                                    "#FF0000",
                                    "#CCCCCC",
                                ],
                                hoverBackgroundColor: [
                                    "#FF0000",
                                    "#CCCCCC"
                                ]
                            }]
                    };
                    new Chart(ctx, {
                        type: 'doughnut',
                        data: data,
                        options: {
                            responsive: false,
                            title: {
                                display: true,
                                text: 'Gastos ' + $("#month").val() + "/" + $("#year").val()
                            }
                        }
                    });
                }
            }
        });
    });
    $(".dataTable").dataTable().fnDestroy();
    $(".dataTable").dataTable({
        sPaginationType: "full_numbers",
        oLanguage: {
            sEmptyTable: "Sem dados disponíveis na tabela",
            sSearch: "<span>Buscar:</span> ",
            sInfo: "Mostrar <span>_START_</span> para <span>_END_</span> de <span>_TOTAL_</span> registros",
            sLengthMenu: "_MENU_ <span>registros por pagina</span>",
            sInfoEmpty: "Mostrar <span>_TOTAL_</span> para <span>_TOTAL_</span> de <span>_TOTAL_</span> registros",
            sInfoFiltered: "(Filtrado de um total de <span>_MAX_</span> total registros)",
            oPaginate: {
                sNext: "Próximo",
                sPrevious: "Anterior",
                sFirst: "Primeiro",
                sLast: "Último"
            }
        },
        ordering: false,
        sDom: "lfrtip",
        responsive: true,
        iDisplayLength: 100,
        bSort: false
    });
    $(".dataTable").css("width", '100%');
    $('.dataTables_filter input').attr("placeholder", "Buscar Por...");
    $(".dataTables_length select").wrap("<div class='input-mini'></div>").chosen({
        disable_search_threshold: 9999999
    });
    $(".todos").change(function() {
        if($(this).is(":checked")) {
            $(".payment").each(function() {
                $(".payment").attr("checked", "checked");
                var index = $(this).attr("payment-id") + " - " + $(this).attr("payment-code");
                payments[index] = {
                    value: parseFloat($(this).attr("payment-value").replace("R$ ", "").replace(".", "").replace(",", ".")),
                    valueMoney: $(this).attr("payment-value"),
                    id: $(this).attr("payment-id"),
                    code: $(this).attr("payment-code")
                };
            });
            console.log(payments);
        } else {
            paymentsId = [];
            payments = [];
            $(".payment").removeAttr("checked");
            console.log(payments);
        }
    });
    $(".payment").change(function() {
        if($(this).is(":checked")) {
            var index = $(this).attr("payment-id") + " - " + $(this).attr("payment-code");
            payments[index] = {
                value: parseFloat($(this).attr("payment-value").replace("R$ ", "").replace(".", "").replace(",", ".")),
                valueMoney: $(this).attr("payment-value"),
                id: $(this).attr("payment-id"),
                code: $(this).attr("payment-code")
            };
            console.log(payments);
        } else {
            var index = $(this).attr("payment-id") + " - " + $(this).attr("payment-code");
            delete payments[index];
            console.log(payments);
        };
    });
    $("#approve").click(function() {
        var total = 0;
        $("#modal_table_approve tbody").html("");
        var html = "";
        for (pay in payments) {
            html += "<tr><td>" + payments[pay].code + "</td><td>" + payments[pay].valueMoney + "<input type='hidden' name='payment_id[]' value='" + payments[pay].id + "'></td></tr>";
            total += payments[pay].value;
        }
        if(total == 0) {
            $("#modal_aviso").modal("show");
            return;
        }
        $("#modal_table_approve tbody").html(html);
        $("#total_approve").html("Total: " + total.toLocaleString("pt-BR", {
            style: "currency",
            currency: "BRL"
        }));
        $("#modal_approve").modal("show");
    });
    $("#deprive").click(function() {
        var total = 0;
        $("#modal_table_deprive tbody").html("");
        var html = "";
        for (pay in payments) {
            html += "<tr><td>" + payments[pay].code + "</td><td>" + payments[pay].valueMoney + "<input type='hidden' name='payment_id[]' value='" + payments[pay].id + "'></td></tr>";
            total += payments[pay].value;
        }
        if(total == 0) {
            $("#modal_aviso").modal("show");
            return;
        }
        $("#modal_table_deprive tbody").html(html);
        $("#total_deprive").html("Total: " + total.toLocaleString("pt-BR", {
            style: "currency",
            currency: "BRL"
        }));
        $("#modal_deprive").modal("show");
    });
});
